/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Guillermo
 * @date 16/4/2015
 * 
 * Configuración de los paquetes de servicio
 */
@Configuration
@Import(value = {DataBaseConfig.class})
@ComponentScan("com.cruxconsultores.pegasus.service")
public class RootAppConfig {

	/**
	 * 
	 */
	public RootAppConfig() {
		// TODO Auto-generated constructor stub
	}

}
