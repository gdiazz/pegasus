/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Guillermo
 * @date 16/4/2015
 * 
 * Configuración de la base de datos emmbebida de la aplicación
 */
@Configuration
@ComponentScan("com.cruxconsultores.pegasus.repository")
@EnableTransactionManagement
public class DataBaseConfig {

	/**
	 * 
	 */
	private static final String DRIVER = "org.h2.Driver";
	private static String USER = "sa";
	private static String PASSWORD = "";
	private static String URL = "jdbc:h2:file:~/pegasus.demo;";
	
	@Bean
	public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUsername(USER);
        dataSource.setPassword(PASSWORD);
        dataSource.setUrl(URL);
        dataSource.setDriverClassName(DRIVER);
        return dataSource;
	}
	
	@Bean
	public Flyway flyway() throws SQLException{
		Flyway flyway = new Flyway();
		flyway.setLocations("classpath:db/migration");
		flyway.setDataSource(this.dataSource());
		flyway.clean();
		flyway.migrate();
		return flyway;
	}

}
