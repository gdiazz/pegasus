/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
@Controller
@RequestMapping("/")
public class HomeController {

	/**
	 * 
	 */
	public HomeController() {
		// TODO Auto-generated constructor stub
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(@RequestParam Map<String,String> params, Model model){
		
		System.out.println(params.get("test"));
		List<String> list = new ArrayList<String>();
		list.add("Me");
		list.add("Llamo");
		list.add("Guillermo");
		model.addAttribute("list", list);
		

		return "home/index";
	}

}
