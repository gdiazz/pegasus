/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.web.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cruxconsultores.pegasus.service.IUserService;
import com.cruxconsultores.pegasus.service.dto.PageDto;
import com.cruxconsultores.pegasus.service.dto.Pagination;
import com.cruxconsultores.pegasus.service.dto.Sorting;
import com.cruxconsultores.pegasus.service.dto.UserDto;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 */
@RestController
@RequestMapping(RestApi.CONTEXT)
public class UserController {

	@Autowired
	IUserService userService;

	/**
	 * 
	 */
	public UserController() {
		// TODO Auto-generated constructor stub
		System.out.println(RestApi.USERS_PATH);
	}

	@RequestMapping(value = { RestApi.USERS_PATH }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PageDto<UserDto>> paginateUsers(

			@RequestParam(required = false) Integer count,
			@RequestParam(required = false) Integer limit,
			@RequestParam(required = false) Integer offset,
			@RequestParam(required = false)String orderField,
			@RequestParam(defaultValue = Sorting.DEFAULT_ORDER_DIRECTION) String orderDirection

	) {
		
		Pagination pagination = new Pagination();
		pagination.setCount(count);
		pagination.setLimit(limit);
		pagination.setOffset(offset);

		Sorting sorting = new Sorting(UserDto.class);
		sorting.setOrderField(orderField);
		sorting.setOrderDirection(orderDirection);
		

		PageDto<UserDto> page = null;

		try {
			page = userService.paginate(pagination, sorting);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<PageDto<UserDto>>(page, HttpStatus.OK);

	}

}
