/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.web.controller.api;

import java.util.Date;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 *       De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
@RestController
@RequestMapping(RestApi.CONTEXT)
public class TestController {

	@RequestMapping(value = "test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Test> home(@RequestParam Map<String,String> params) {
		Test test = new Test();
		test.setDate(new Date());
		test.setMessage("Hello This is a rest service :) " + ( (params.get("val") != null)? params.get("val") : ""  ));
		return new ResponseEntity<Test>(test, HttpStatus.OK);
	}
	
	class Test{
		private String message;
		
		private Date date;

		/**
		 * @return the date
		 */
		public Date getDate() {
			return date;
		}

		/**
		 * @param date the date to set
		 */
		public void setDate(Date date) {
			this.date = date;
		}

		/**
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}

		/**
		 * @param message the message to set
		 */
		public void setMessage(String message) {
			this.message = message;
		}
		
		
		
	}


}
