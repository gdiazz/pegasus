/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.web.controller.api;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 *       Contiene todos los end points del API rest
 */
public class RestApi {

	public static final String CONTEXT = "/api";

	/**
	 * Users
	 */
	public static final String USERS_PATH = "/users";

	public static final String USERS_BY_ID_PATH = USERS_PATH + "/{id}";

	public static final String USERS_BY_USER_NAME_PATH = "/" + USERS_PATH
			+ "/{userName}";

	/**
	 * 
	 */
	public RestApi() {
		// TODO Auto-generated constructor stub
	}

}
