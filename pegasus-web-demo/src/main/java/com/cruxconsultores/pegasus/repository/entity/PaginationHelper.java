/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.google.common.base.CaseFormat;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 */
public class PaginationHelper<E> {

	
	private static int ORDER_FIELD = 0;
	
	private static int ORDER_DIRECTION = 1;
	
	private static final String ORDER_BY = " ORDER BY ";
	/**
	 * 
	 */
	public PaginationHelper() {

	}

	/**
	 * Devuelve una pagina
	 * 
	 * @return
	 */
	public Page<E> fetchPage(JdbcTemplate jd, String sqlCount, String sqlSelect,
			Object[] args, final Integer limit, Integer offset, final RowMapper<E> mapper) {

		// Determina la cantidad de filas total
		Integer rowCount = jd.queryForObject(sqlCount, Integer.class);
		Integer pagesCount = rowCount / limit;

		// Calcula el numero de páginas
		if (rowCount > limit * pagesCount) {
			pagesCount++;
		}

		// Crea la página

		final Page<E> page = new Page<E>();
		page.setLimit(limit);
		page.setOffset(offset);
		page.setPagesCount(pagesCount);
		page.setOrderField((String)args[ORDER_FIELD]);
		page.setOrderDirection((String)args[ORDER_DIRECTION]);

		// Determina la pagina de inicio
		final int startRow = (offset - 1) * limit;

		String orderFieldName = ((String)args[ORDER_FIELD]);
		if (orderFieldName.indexOf("_") < 0){
			orderFieldName = resolveFieldName(orderFieldName);
		}
			
		
		jd.query(sqlSelect + ORDER_BY + orderFieldName + " " + ((String)args[ORDER_DIRECTION]).toUpperCase() , new ResultSetExtractor<Object>() {

			@Override
			public Object extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				Integer currentRow = 0;
				
				final List<E> items = page.getItems();
				
				while (rs.next() &&  currentRow < startRow + limit ){
					
                    if (currentRow >= startRow) {
                    	items.add(mapper.mapRow(rs, currentRow));
                    }
                    currentRow++;
				}
				page.setLimit(items.size());
				return items;
			}

		} );

		return page;
	}
	
	/**
	 * Convierte camelCase a underscore
	 * @param propertieName
	 * @return
	 */
	public String resolveFieldName(String propertieName){
		 
		return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, propertieName).toUpperCase();
		
		
	}

}
