/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 */
public class Page<E> {

	/**
	 * Cantidad de elementos
	 */
	private Integer limit;
	
	/**
	 * Apartir del elemento n
	 */
	private Integer offset;
	
	/**
	 * cantidad de paginas disponibles
	 */
	private Integer pagesCount;

	
	/**
	 * 
	 */
	private List<E> items = new  ArrayList<E>();
	
	
	
	private String orderDirection;
	
	
	private String orderField;
	
	
	public Page(){
		
	}

	/**
	 * @return the limit
	 */
	public Integer getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	/**
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(Integer offset) {
		this.offset = offset;
	}


	/**
	 * @return the pagesCount
	 */
	public Integer getPagesCount() {
		return pagesCount;
	}

	/**
	 * @param pagesCount the pagesCount to set
	 */
	public void setPagesCount(Integer pagesCount) {
		this.pagesCount = pagesCount;
	}

	/**
	 * @return the orderDirection
	 */
	public String getOrderDirection() {
		return orderDirection;
	}

	/**
	 * @param orderDirection the orderDirection to set
	 */
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	/**
	 * @return the orderField
	 */
	public String getOrderField() {
		return orderField;
	}

	/**
	 * @param orderField the orderField to set
	 */
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	/**
	 * @return the items
	 */
	public List<E> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<E> items) {
		this.items = items;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Page [limit=" + limit + ", offset=" + offset + ", pagesCount="
				+ pagesCount + ", orderDirection=" + orderDirection
				+ ", orderField=" + orderField + ", items=" + items + "]";
	}
	

	
	
	
	
	
	

}
