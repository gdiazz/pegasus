/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.dao;

import java.util.List;

import com.cruxconsultores.pegasus.repository.entity.Page;
import com.cruxconsultores.pegasus.repository.entity.User;


/**
 * @author Guillermo
 * @date 16/4/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public interface GenericDao<E,T> {
	
	/**
	 * Inserta un elemento
	 * @return
	 * @throws EntityException
	 */
	public int save(E entity) throws Exception;
	
	/**
	 * Obtiene el elemento por id
	 * @return
	 * @throws EntityException
	 */
	public E find(Long id) throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<E> list() throws Exception;
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int delete(T id) throws Exception;
	
	/**
	 * 
	 * @param id
	 * @param newEntity
	 * @return
	 * @throws Exception
	 */
	public int update(T id, E newEntity ) throws Exception;
	
	/**
	 * 
	 * @param limit
	 * @param offset
	 * @return
	 */
	public Page<E> paginate(Integer limit, Integer offset, String orderField, String orderDirection) throws Exception;
}
