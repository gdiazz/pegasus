/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.entity.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.cruxconsultores.pegasus.repository.entity.User;

/**
 * @author Guillermo
 * @date 16/4/2015
 * 
 *       De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
@Component
public class UserMapper implements RowMapper<User> {

	public static final String ID = "ID";
	public static final String USER_NAME = "USER_NAME";
	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String MIDDLE_NAME = "MIDDLE_NAME";
	public static final String PATERNAL_NAME = "PATERNAL_NAME";
	public static final String MATERNAL_NAME = "MATERNAL_NAME";
	public static final String PASSWORD = "PASSWORD";
	public static final String EMAIL = "EMAIL";
	public static final String CERTIFICATE = "CERTIFICATE";
	public static final String CREATED = "CREATED";
	public static final String UPDATED = "UPDATED";
	public static final String ENABLED = "ENABLED";

	/**
	 * 
	 */
	public UserMapper() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */
	@Override
	public User mapRow(ResultSet rs, int row) throws SQLException {
		User user = new User();

		user.setCertificate(rs.getString(CERTIFICATE));
		user.setCreated(rs.getDate(CREATED));
		user.setEmail(rs.getString(EMAIL));
		user.setFirstName(rs.getString(FIRST_NAME));
		user.setId(rs.getLong(ID));
		user.setMiddleName(rs.getString(MIDDLE_NAME));
		user.setPassword(rs.getString(PASSWORD));
		user.setUpdated(rs.getDate(UPDATED));
		user.setUserName(rs.getString(USER_NAME));
		user.setPaternalName(rs.getString(PATERNAL_NAME));
		user.setMaternalName(rs.getString(MATERNAL_NAME));
		user.setEnabled((rs.getInt(ENABLED) == 1) ? true : false);

		return user;
	}

}
