/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.dao;

import com.cruxconsultores.pegasus.repository.entity.User;

/**
 * @author Guillermo
 * @date 16/4/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public interface IUserDao extends GenericDao<User, Long>{
	
	
	
}
