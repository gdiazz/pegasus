/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cruxconsultores.pegasus.repository.dao.IUserDao;
import com.cruxconsultores.pegasus.repository.entity.Page;
import com.cruxconsultores.pegasus.repository.entity.PaginationHelper;
import com.cruxconsultores.pegasus.repository.entity.User;
import com.cruxconsultores.pegasus.repository.entity.mapper.UserMapper;

/**
 * @author Guillermo
 * @date 16/4/2015
 * 
 */
@Repository
public class UserDaoImpl implements IUserDao {

	private JdbcTemplate jdbcTemplate;

	private final String SQL_INSERT_USER = "INSERT INTO USERS (ID, USER_NAME, FIRST_NAME, MIDDLE_NAME, PATERNAL_NAME, MATERNAL_NAME, PASSWORD, EMAIL, CERTIFICATE, CREATED, UPDATED, ENABLED) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

	private final String SQL_SELECT_USERS = "SELECT ID ,USER_NAME,FIRST_NAME,MIDDLE_NAME ,PATERNAL_NAME,MATERNAL_NAME ,PASSWORD,EMAIL,CERTIFICATE ,CREATED,UPDATED, ENABLED FROM USERS WHERE ENABLED = 1";

	private final String SQL_FIND_USER_BY_ID = "SELECT ID ,USER_NAME,FIRST_NAME,MIDDLE_NAME ,PATERNAL_NAME,MATERNAL_NAME ,PASSWORD,EMAIL,CERTIFICATE, CREATED, UPDATED, ENABLED FROM USERS WHERE ID = ? AND ENABLED = 1";

	private final String SQL_UPDATE_USER_BY_ID = "UPDATE USERS SET ID = ?, USER_NAME= ?, FIRST_NAME= ?, MIDDLE_NAME = ?, PATERNAL_NAME= ?, MATERNAL_NAME = ?, PASSWORD= ?, EMAIL= ?, CERTIFICATE = ?, CREATED= ?, UPDATED = ? WHERE ID = ?  AND ENABLED = 1";

	private final String SQL_DELETE_USER_BY_ID = "UPDATE USERS SET ENABLED = 0 WHERE ID = ? AND ENABLED = 1";

	private final String SQL_COUNT_USERS = "SELECT COUNT(ID) FROM USERS WHERE ENABLED = 1";

	/**
	 * 
	 */
	@Autowired
	public UserDaoImpl(DataSource datasource) {
		jdbcTemplate = new JdbcTemplate(datasource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.repository.dao.GenericDAO#save(java.lang.
	 * Object)
	 */
	@Override
	public int save(User entity) throws Exception {

		int result = jdbcTemplate.update(SQL_INSERT_USER, entity.getId(),
				entity.getUserName(), entity.getFirstName(),
				entity.getMiddleName(), entity.getPaternalName(),
				entity.getMaternalName(), entity.getPassword(),
				entity.getEmail(), entity.getCertificate(),
				entity.getCreated(), entity.getUpdated(), entity.isEnabled());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.repository.dao.GenericDAO#find(java.lang.
	 * Object)
	 */
	@Override
	public User find(Long id) throws Exception {

		User user = (User) jdbcTemplate.queryForObject(SQL_FIND_USER_BY_ID,
				new Object[] { id }, new UserMapper());

		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cruxconsultores.pegasus.repository.dao.GenericDAO#list()
	 */
	@Override
	public List<User> list() throws Exception {
		List<User> users = jdbcTemplate.query(SQL_SELECT_USERS,
				new UserMapper());
		return users;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.repository.dao.GenericDAO#delete(java.lang
	 * .Object)
	 */
	@Override
	public int delete(Long id) throws Exception {
		int result = jdbcTemplate.update(SQL_DELETE_USER_BY_ID, id);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.repository.dao.GenericDao#update(java.lang
	 * .Object, java.lang.Object)
	 */
	@Override
	public int update(Long id, User newEntity) throws Exception {
		int result = jdbcTemplate.update(SQL_UPDATE_USER_BY_ID,
				newEntity.getId(), newEntity.getUserName(),
				newEntity.getFirstName(), newEntity.getMiddleName(),
				newEntity.getPaternalName(), newEntity.getMaternalName(),
				newEntity.getPassword(), newEntity.getEmail(),
				newEntity.getCertificate(), newEntity.getCreated(),
				newEntity.getUpdated(), id);

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.repository.dao.GenericDao#paginate(java.lang
	 * .Integer, java.lang.Integer)
	 */
	@Override
	public Page<User> paginate(Integer limit, Integer offset,
			String orderField, String orderDirection) throws Exception {
		
		PaginationHelper<User> paginationHelper = new PaginationHelper<User>();
		return paginationHelper.fetchPage(jdbcTemplate, SQL_COUNT_USERS,
				SQL_SELECT_USERS, new Object[] { orderField, orderDirection },
				limit, offset, new UserMapper());

	}

}
