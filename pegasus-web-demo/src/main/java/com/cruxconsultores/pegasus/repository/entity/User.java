/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.entity;

import java.util.Date;

/**
 * @author Guillermo
 * @date 16/4/2015
 * 
 *       Entidad User
 */
public class User {

	private Long id;

	private String userName;

	private String firstName;

	private String middleName;

	private String paternalName;

	private String maternalName;

	private String password;

	private String email;

	private String certificate;

	private Date created;

	private Date updated;
	
	private Boolean enabled;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the paternalName
	 */
	public String getPaternalName() {
		return paternalName;
	}

	/**
	 * @param paternalName the paternalName to set
	 */
	public void setPaternalName(String paternalName) {
		this.paternalName = paternalName;
	}

	/**
	 * @return the maternalName
	 */
	public String getMaternalName() {
		return maternalName;
	}

	/**
	 * @param maternalName the maternalName to set
	 */
	public void setMaternalName(String maternalName) {
		this.maternalName = maternalName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the certificate
	 */
	public String getCertificate() {
		return certificate;
	}

	/**
	 * @param certificate the certificate to set
	 */
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/**
	 * @return the enabled
	 */
	public Boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", firstName="
				+ firstName + ", middleName=" + middleName + ", paternalName="
				+ paternalName + ", maternalName=" + maternalName
				+ ", password=" + password + ", email=" + email
				+ ", certificate=" + certificate + ", created=" + created
				+ ", updated=" + updated + ", enabled=" + enabled + "]";
	}


	

}
