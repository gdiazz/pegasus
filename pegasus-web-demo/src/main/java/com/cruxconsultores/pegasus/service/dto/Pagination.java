/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.dto;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 *       De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public class Pagination {

	
	private static final Integer COUNT_DEFAULT = 0;
	
	private static final Integer LIMIT_DAFAULT = 10;
	
	private static final Integer OFFSET_DEFAULT = 1;
	
    private Integer limit;

    private Integer offset;

    private Integer count;
	
    
    
	
	/**
	 * @param limit
	 * @param offset
	 * @param count
	 */
	public Pagination(Integer limit, Integer offset, Integer count) {
		super();
		this.limit = limit;
		this.offset = offset;
		this.count = count;
	}


	/**
	 * 
	 */
	public Pagination() {
		this.limit = LIMIT_DAFAULT;
		this.offset = OFFSET_DEFAULT;
		this.count = COUNT_DEFAULT;
	}


	/**
	 * @return the limit
	 */
	public Integer getLimit() {
		return limit;
	}


	/**
	 * @param limit the limit to set
	 */
	public void setLimit(Integer limit) {
		if (limit != null)
			this.limit = limit;
	}


	/**
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}


	/**
	 * @param offset the offset to set
	 */
	public void setOffset(Integer offset) {
		if (offset != null)
			this.offset = offset;
	}


	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}


	/**
	 * @param count the count to set
	 */
	public void setCount(Integer count) {
		if (count != null)
			this.count = count;
	}
	
	

}
