/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service;

import java.util.List;

import com.cruxconsultores.pegasus.service.dto.PageDto;
import com.cruxconsultores.pegasus.service.dto.Pagination;
import com.cruxconsultores.pegasus.service.dto.Sorting;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 */
public interface IGenericService<D, I> {

	/**
	 * Guarda un modelo
	 * @param model
	 * @return
	 */
	public int save(D model) throws Exception;
	
	/**
	 * Obtiene un modelo por id
	 * @param id
	 * @return
	 */
	public D find(I id) throws Exception;
	
	
	/**
	 * Devuelve una lista de modelos
	 * @return
	 * @throws Exception
	 */
	public List<D> list() throws Exception;
	
	/**
	 * Borra un elemento por id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int delete(I id) throws Exception;
	
	/**
	 * Pagina los modelos
	 * @return
	 * @throws Exception
	 */
	public PageDto<D> paginate(Pagination pagination, Sorting sorting) throws Exception;

}
