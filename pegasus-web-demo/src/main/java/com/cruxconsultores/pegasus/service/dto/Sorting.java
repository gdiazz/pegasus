/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.dto;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import com.cruxconsultores.pegasus.service.annotations.DefaultField;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public class Sorting {

	public static final String DEFAULT_ORDER_DIRECTION = "asc";
	
	private String orderField;
	
	private String orderDirection;
	
	
	/**
	 * @param orderField
	 * @param orderDirection
	 */
	public Sorting(String orderField, String orderDirection) {
		super();
		this.orderField = orderField;
		this.orderDirection = orderDirection;
	}

	/**
	 * 
	 */
	public Sorting(Class<?> cls) {
		orderDirection = DEFAULT_ORDER_DIRECTION;
		this.orderField = resolveOrderField(cls);
	}

	/**
	 * @return the orderField
	 */
	public String getOrderField() {
		return orderField;
	}

	/**
	 * @param orderField the orderField to set
	 */
	public void setOrderField(String orderField) {
		if (orderField != null)
			this.orderField = orderField;
	}

	/**
	 * @param orderField the orderField to set
	 */
	private String resolveOrderField(Class<?> cls) {
		Field [] fields = cls.getDeclaredFields();
		
		for (int i = 0; i < fields.length; i++) {
			Annotation anns [] = fields[i].getDeclaredAnnotations();
			for (int j = 0; j < anns.length; j++) {
				if (anns[j] instanceof DefaultField){
					return fields[i].getName();
				}
			}
		}
		return null;
	}
	/**
	 * @return the orderDirection
	 */
	public String getOrderDirection() {
		return orderDirection;
	}

	/**
	 * @param orderDirection the orderDirection to set
	 */
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	
	

}
