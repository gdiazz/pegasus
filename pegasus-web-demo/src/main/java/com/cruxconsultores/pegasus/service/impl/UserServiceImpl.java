/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cruxconsultores.pegasus.repository.dao.IUserDao;
import com.cruxconsultores.pegasus.repository.entity.Page;
import com.cruxconsultores.pegasus.repository.entity.User;
import com.cruxconsultores.pegasus.service.IUserService;
import com.cruxconsultores.pegasus.service.dto.PageDto;
import com.cruxconsultores.pegasus.service.dto.Pagination;
import com.cruxconsultores.pegasus.service.dto.Sorting;
import com.cruxconsultores.pegasus.service.dto.UserDto;
import com.cruxconsultores.pegasus.service.dto.mapper.UserMapperDto;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 */
@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	IUserDao userDao;

	@Autowired
	UserMapperDto userMapperDto;

	/**
	 * 
	 */
	public UserServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.service.IGenericService#save(java.lang.Object
	 * )
	 */
	@Override
	public int save(UserDto model) throws Exception {
		return userDao.save(userMapperDto.toEntity(model));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.service.IGenericService#find(java.lang.Object
	 * )
	 */
	@Override
	public UserDto find(Long id) throws Exception {
		return userMapperDto.toDto(userDao.find(id.longValue()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cruxconsultores.pegasus.service.IGenericService#list()
	 */
	@Override
	public List<UserDto> list() throws Exception {
		return userMapperDto.toDtoList(userDao.list());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cruxconsultores.pegasus.service.IGenericService#paginate(com.
	 * cruxconsultores.pegasus.service.dto.Pagination,
	 * com.cruxconsultores.pegasus.service.dto.Sorting)
	 */
	@Override
	public PageDto<UserDto> paginate(Pagination pagination, Sorting sorting)
			throws Exception {

		PageDto<UserDto> pageUserDto = new PageDto<UserDto>();

		Page<User> page = userDao.paginate(pagination.getLimit(),
				pagination.getOffset(), sorting.getOrderField(),
				sorting.getOrderDirection());

		pageUserDto.setPagination(new Pagination(page.getLimit(), page
				.getOffset(), page.getPagesCount()));
		
		pageUserDto.setSorting(sorting);
		
		pageUserDto.setModels(userMapperDto.toDtoList(page.getItems()));

		return pageUserDto;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cruxconsultores.pegasus.service.IGenericService#delete(java.lang.
	 * Object)
	 */
	@Override
	public int delete(Long id) throws Exception {
		return userDao.delete(id);
	}

}
