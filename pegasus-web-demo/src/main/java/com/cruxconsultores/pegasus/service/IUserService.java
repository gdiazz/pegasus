/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service;

import com.cruxconsultores.pegasus.service.dto.UserDto;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 */
public interface IUserService extends IGenericService<UserDto, Long>{
	
	
}
