/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cruxconsultores.pegasus.repository.entity.User;
import com.cruxconsultores.pegasus.service.dto.UserDto;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 */

@Component
public class UserMapperDto implements ServiceMapper<User, UserDto> {

	/**
	 * 
	 */
	public UserMapperDto() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.cruxconsultores.pegasus.service.dto.mapper.ServiceMapper#toEntity(java.lang.Object)
	 */
	@Override
	public User toEntity(UserDto dto) {
		
		User user = new User();
		user.setCertificate(dto.getCertificate());
		user.setCreated(dto.getCreated());
		user.setEmail(dto.getEmail());
		user.setEnabled(dto.getEnabled());
		user.setFirstName(dto.getFirstName());
		user.setId(dto.getId());
		user.setMaternalName(dto.getMaternalName());
		user.setMiddleName(dto.getMiddleName());
		user.setPassword(dto.getPassword());
		user.setPaternalName(dto.getPaternalName());
		user.setUpdated(dto.getUpdated());
		user.setUserName(dto.getUserName());
		
		return user;
	
	}

	/* (non-Javadoc)
	 * @see com.cruxconsultores.pegasus.service.dto.mapper.ServiceMapper#toDto(java.lang.Object)
	 */
	@Override
	public UserDto toDto(User entity) {
		UserDto user = new UserDto();
		user.setCertificate(entity.getCertificate());
		user.setCreated(entity.getCreated());
		user.setEmail(entity.getEmail());
		user.setEnabled(entity.isEnabled());
		user.setFirstName(entity.getFirstName());
		user.setId(entity.getId());
		user.setMaternalName(entity.getMaternalName());
		user.setMiddleName(entity.getMiddleName());
		user.setPassword(entity.getPassword());
		user.setPaternalName(entity.getPaternalName());
		user.setUpdated(entity.getUpdated());
		user.setUserName(entity.getUserName());
		return user;
	}



	/* (non-Javadoc)
	 * @see com.cruxconsultores.pegasus.service.dto.mapper.ServiceMapper#toDtoList(java.util.List)
	 */

	public List<UserDto> toDtoList(List<User> users){
		List<UserDto> usersDto = new ArrayList<UserDto>();
		for (User user : users) {
			usersDto.add(toDto(user));
		}
		return usersDto;
		
	}
	
	/* (non-Javadoc)
	 * @see com.cruxconsultores.pegasus.service.dto.mapper.ServiceMapper#toEntityList(java.util.List)
	 */
	public List<User> toEntityList(List<UserDto> usersDto){
		List<User> users = new ArrayList<User>();
		for (UserDto userDto : usersDto) {
			users.add(toEntity(userDto));
		}
		return users;
		
	}



}
