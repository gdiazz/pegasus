/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.dto;

import java.util.List;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 * Pagina a mostrar
 */
public class PageDto<D> {

	/**
	 * Información sobre la paginación
	 */
	private Pagination pagination;
	
	/**
	 * Configuración de ordenamiento
	 */
	private Sorting sorting;
	
	
	/**
	 * Los elementos paginados
	 */
	private List<D> models;
	
	
	/**
	 * 
	 */
	public PageDto() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the pagination
	 */
	public Pagination getPagination() {
		return pagination;
	}



	/**
	 * @param pagination the pagination to set
	 */
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}



	/**
	 * @return the sorting
	 */
	public Sorting getSorting() {
		return sorting;
	}



	/**
	 * @param sorting the sorting to set
	 */
	public void setSorting(Sorting sorting) {
		this.sorting = sorting;
	}



	/**
	 * @return the models
	 */
	public List<D> getModels() {
		return models;
	}



	/**
	 * @param models the models to set
	 */
	public void setModels(List<D> models) {
		this.models = models;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Page [pagination=" + pagination + ", sorting=" + sorting
				+ ", models=" + models + "]";
	}
	
	
	

}
