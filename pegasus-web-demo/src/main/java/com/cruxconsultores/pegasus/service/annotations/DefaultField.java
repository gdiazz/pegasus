/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 *       Indica a alguna propiedad de una clase que es la por defecto
 */
@Target({ FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DefaultField {

}
