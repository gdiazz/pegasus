/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.dto.mapper;

import java.util.List;

import com.cruxconsultores.pegasus.repository.entity.Page;
import com.cruxconsultores.pegasus.service.dto.PageDto;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 */
public interface ServiceMapper<E, D> {

	/**
	 * 
	 * @param dto
	 * @return
	 */
	public E toEntity(D dto);
	
	/**
	 * 
	 * @param entity
	 * @return
	 */
	public D toDto (E entity);
	
	/**
	 * 
	 * @param dtoList
	 * @return
	 */
	public List<E> toEntityList(List<D> dtoList);
	
	/**
	 * 
	 * @param entityList
	 * @return
	 */
	public List<D> toDtoList(List<E> entityList);
	
}
