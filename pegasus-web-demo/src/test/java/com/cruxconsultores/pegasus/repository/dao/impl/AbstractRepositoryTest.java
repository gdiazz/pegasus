/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.dao.impl;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import com.cruxconsultores.pegasus.config.DataBaseConfig;

/**
 * @author Guillermo
 * @date 20/4/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfig.class }, loader = AnnotationConfigContextLoader.class)
@Transactional
public class AbstractRepositoryTest {

	/**
	 * 
	 */
	public AbstractRepositoryTest() {
		// TODO Auto-generated constructor stub
	}

}
