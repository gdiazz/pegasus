/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.entity;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public class PaginationHelperTest {

	/**
	 * Test method for {@link com.cruxconsultores.pegasus.repository.entity.PaginationHelper#resolveFieldName(java.lang.String)}.
	 */
	@Test
	public void testResolveFieldName() {
		String expected = "IS_A_TEST";
		String camelCaseField = "isATest";
		PaginationHelper<Object> pagination = new PaginationHelper<Object>();
		String sqlCaseField = pagination.resolveFieldName(camelCaseField);
		assertTrue(expected.equals(sqlCaseField));
	}

}
