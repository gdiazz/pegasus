/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.repository.dao.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cruxconsultores.pegasus.repository.entity.Page;
import com.cruxconsultores.pegasus.repository.entity.User;
import com.cruxconsultores.pegasus.repository.entity.mapper.UserMapper;

/**
 * @author Guillermo
 * @date 20/4/2015
 * 
 *       De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public class UserDaoImplTest extends AbstractRepositoryTest {

	/**
	 * 
	 */
	private static final long ID_USER = 1;
	private static final long ID_USER_TO_DELETE = 2;
	private static final long ID_USER_TO_UPDATE = 3;

	@Autowired
	UserDaoImpl userDao;

	private User userTest;

	@Before
	public void setUp() {
		userTest = new User();
		userTest.setCertificate("cert");
		userTest.setCreated(new Date());
		userTest.setEmail("user@usermail.com");
		userTest.setFirstName("Carlos");
		userTest.setId(1000L);
		userTest.setMaternalName("Casas");
		userTest.setMiddleName("Andrés");
		userTest.setPassword("12354");
		userTest.setPaternalName("Suarez");
		userTest.setUpdated(new Date());
		userTest.setUserName("pavilo");
		userTest.setEnabled(true);
	}

	@Test
	public void testUserDao() {
		assertNotNull(userDao);
	}

	/**
	 * Test method for
	 * {@link com.cruxconsultores.pegasus.repository.dao.impl.UserDaoImpl#save(com.cruxconsultores.pegasus.repository.entity.User)}
	 * .
	 */
	@Test
	public void testSave() {
		int result = 0;
		try {
			result = userDao.save(userTest);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(result > 0);

	}

	/**
	 * Test method for
	 * {@link com.cruxconsultores.pegasus.repository.dao.impl.UserDaoImpl#find(com.cruxconsultores.pegasus.repository.entity.User)}
	 * .
	 */
	@Test
	public void testFind() {


		User user = null;
		try {
			user = userDao.find(ID_USER);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(user.getId() == ID_USER);
	}

	/**
	 * Test method for
	 * {@link com.cruxconsultores.pegasus.repository.dao.impl.UserDaoImpl#list()}
	 * .
	 */
	@Test
	public void testList() {
		try {
			assertTrue(userDao.list().size() > 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test method for
	 * {@link com.cruxconsultores.pegasus.repository.dao.impl.UserDaoImpl#delete(java.lang.Long)}
	 * .
	 */
	@Test
	public void testDelete() {
		int result = 0;
		try {
			result = userDao.delete(ID_USER_TO_DELETE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(result > 0);
	}

	@Test
	public void testUpdate() {
		int result = 0;
		try {
			userTest.setId(123456L);
			userTest.setUserName("pajarito");
			result = userDao.update(ID_USER_TO_UPDATE, userTest);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		assertTrue(result > 0);

	}
	
	@Test
	public void testPaginate() throws Exception{
		Page<User> p = userDao.paginate(3, 5, UserMapper.USER_NAME, "ASC");
		List<User> users =  p.getItems();
		assertTrue(users.size() > 0);
	}

}
