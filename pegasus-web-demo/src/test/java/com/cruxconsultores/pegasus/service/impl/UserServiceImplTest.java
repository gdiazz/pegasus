/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cruxconsultores.pegasus.service.IUserService;
import com.cruxconsultores.pegasus.service.dto.PageDto;
import com.cruxconsultores.pegasus.service.dto.Pagination;
import com.cruxconsultores.pegasus.service.dto.Sorting;
import com.cruxconsultores.pegasus.service.dto.UserDto;

/**
 * @author Guillermo
 * @date 21/4/2015
 * 
 *       De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public class UserServiceImplTest extends AbstractServiceTest {

	@Autowired
	IUserService userService;

	private UserDto userDto;
	private static final Long ID_USER = 114400188L;

	@Before
	public void setUp() {
		userDto = new UserDto();
		userDto.setCertificate("certificate");
		userDto.setCreated(new Date());
		userDto.setEmail("test@service.com");
		userDto.setEnabled(true);
		userDto.setFirstName("Juanito");
		userDto.setId(ID_USER);
		userDto.setMaternalName("Perez");
		userDto.setMiddleName("Mora");
		userDto.setPassword("123");
		userDto.setPaternalName("Valverde");
		userDto.setUserName("guille");
		userDto.setUpdated(new Date());
	}

	@Test
	public void testUserService() {
		assertNotNull(userService);
	}

	@Test
	public void testSave() {
		int result = 0;
		try {
			result = userService.save(userDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(result > 0);

	}
	
	@Test
	public void testPagination(){
		PageDto<UserDto> page = null;
		
		try {
			page = userService.paginate(new Pagination(3,1, 0), new Sorting("userName", "asc"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(page.getModels().size() > 0);
		System.out.println(page);
	}
	
	
}
