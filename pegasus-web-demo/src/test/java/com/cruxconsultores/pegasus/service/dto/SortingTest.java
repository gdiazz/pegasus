/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.service.dto;

import org.junit.Test;

/**
 * @author Guillermo
 * @date 24/4/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public class SortingTest {

	@Test
	public void test() {
		Sorting sorting = new Sorting(UserDto.class);
		System.out.println(sorting.getOrderField());
		System.out.println(sorting.getOrderDirection());
	}

}
