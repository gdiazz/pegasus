/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus;

import static org.junit.Assert.assertTrue;

import java.security.KeyStoreException;
import java.security.cert.CertificateException;

import org.junit.Before;
import org.junit.Test;

import com.cruxconsultores.pegasus.exception.NoManagerTypeException;

/**
 * @author Guillermo
 * @date 7/4/2015
 * 
 */
public class PegasusAppletTest {

	PegasusApplet applet;
	private static final String PASS = "1234";
	private static final String FILE_P12 = "src/test/resources/cert/keystore.p12";

	@Before
	public void setUp() {
		applet = new PegasusApplet();
	}

	@Test
	public void testInitp12() throws NoManagerTypeException {
		applet.setUsingDevice(Constants.USING_P12);
		applet.setP12FilePath(FILE_P12);
		assertTrue(applet.initialize(PASS));
	}
	
	@Test 
	public void getCertificatesBas64() throws NoManagerTypeException, KeyStoreException, CertificateException{
		applet.setUsingDevice(Constants.USING_P12);
		applet.setP12FilePath(FILE_P12);
		applet.initialize(PASS);
		String certificate = applet.getCertificateBase64();
		System.out.println(certificate);
	}


}
