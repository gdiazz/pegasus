/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.exception;

/**
 * @author Guillermo
 * @date 7/4/2015
 * 
 */
public class NoManagerTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public NoManagerTypeException() {
		super("No se ha definido un medio para obtener los certificados");
	}

}
