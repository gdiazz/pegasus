/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.AccessController;
import java.security.PrivilegedAction;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import org.apache.commons.io.IOUtils;

/**
 * @author Guillermo
 * @date 8/4/2015
 *       Provee al applet de metodos que permiten realizar cambios en el sistema
 *       de archivos Es necesario porque los navegadores no ejecutan codigo que
 *       altere el sistema de archivo del
 */
public class SecurityUtils {

	/**
	 * 
	 */
	public SecurityUtils() {

	}
	
	/**
	 * 
	 * @return
	 */
	public static File loadFileDialog() {
		PrivilegedAction<File> action = new PrivilegedAction<File>() {
			@Override
			public File run() {
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				File loadedFile = chooser.getSelectedFile();
				
				return loadedFile;
			}
		};
		return AccessController.doPrivileged(action);
	}
	

	/**
	 * 
	 * @param inFilePath
	 * @param outFilePath
	 * @return
	 */
	public static boolean copyFile(final String inFilePath,
			final String outFilePath) {
		PrivilegedAction<Boolean> action = new PrivilegedAction<Boolean>() {
			@Override
			public Boolean run() {
				File tempFile = new File(inFilePath);
				try (FileOutputStream out = new FileOutputStream(tempFile)) {
					IOUtils.copy(new FileInputStream(new File(outFilePath)),
							out);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
				return true;
			}
		};
		return AccessController.doPrivileged(action);
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static boolean mkdir(final File file) {
		PrivilegedAction<Boolean> action = new PrivilegedAction<Boolean>() {

			@Override
			public Boolean run() {
				if (file.exists())
					return true;
				else
					return file.mkdir();
			}
		};

		return AccessController.doPrivileged(action);
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 */
	public static boolean saveFile(final String pfile, String str) {
		PrivilegedAction<Boolean> action = new PrivilegedAction<Boolean>() {
			File file = new File(pfile);
			@Override
			public Boolean run() {
				if (file.exists())
					return true;
				else
					return file.mkdir();
			}
		};

		return AccessController.doPrivileged(action);
	}
	
	/**
	 * 
	 * @param msg
	 */
	public static void showMessage(final String msg){
		PrivilegedAction<Object> action = new PrivilegedAction<Object>() {

			@Override
			public Object run() {
				JOptionPane.showMessageDialog(null, msg);
				return null;

			}
		};

		AccessController.doPrivileged(action);
	}
	
	/**
	 * 
	 * @param msg
	 */
	public static int showConfirm(final String msg){
		PrivilegedAction<Integer> action = new PrivilegedAction<Integer>() {

			@Override
			public Integer run() {
				return JOptionPane.showConfirmDialog(null, msg);

			}
		};

		return AccessController.doPrivileged(action);
	}

}
