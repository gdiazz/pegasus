/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus;

import java.io.File;
import java.io.IOException;
import java.security.AccessController;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivilegedAction;
import java.security.ProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;

import javax.smartcardio.CardException;
import javax.swing.JApplet;

import sun.security.pkcs11.wrapper.PKCS11Exception;

import com.cruxconsultores.pegasus.device.DeviceManager;
import com.cruxconsultores.pegasus.device.EnumKeyUsage;
import com.cruxconsultores.pegasus.device.P12Manager;
import com.cruxconsultores.pegasus.device.model.AthenaSmartCard;
import com.cruxconsultores.pegasus.exception.NoManagerTypeException;
import com.cruxconsultores.pegasus.utils.CertificateUtils;
import com.cruxconsultores.pegasus.utils.SecurityUtils;
import com.cruxconsultores.pegasusx.sign.xml.XadesMixedSignature;
//import netscape.javascript.*;

/**
 * @author Guillermo
 * @date 7/4/2015
 * 
 *       Permite la comunicación con pegasus en front end
 */
@SuppressWarnings("restriction")
public class PegasusApplet extends JApplet {

	//private JSObject jsObject;	
	private DeviceManager manager;
	private String usingDevice;
	private String p12FilePath;
	private String certificateBase64;
	private String failReason;
	private String signatureFailReason;
	
	private static final long TIME_STAMP_FOLDER = Calendar.getInstance().getTime().getTime();
	private static final String USER_HOME = System.getProperty("user.home");
	private static final String TEMP_DIR = USER_HOME + File.separator
			+ ".temp_pegasus"+File.separator + TIME_STAMP_FOLDER;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public PegasusApplet() {
		super();
		if (!new File(TEMP_DIR).exists()){
			new File(TEMP_DIR).mkdirs();
		}
		System.out.println("PegasusApplet Iniciado");
	}

	/**
	 * Inicializa el keystore. Adicionalmente obtiene los certificados y los
	 * guarda en formato base64
	 * 
	 * @param str
	 * @return
	 * @throws NoManagerTypeException
	 */
	public boolean initialize(final String str) throws NoManagerTypeException {
		if (usingDevice == null)
			throw new NoManagerTypeException();

		PrivilegedAction<Boolean> action = new PrivilegedAction<Boolean>() {

			@Override
			public Boolean run() {

				switch (getUsingDevice()) {
				
				case Constants.USING_SMARTCARD:
					
					manager = new AthenaSmartCard();
					try {

						manager.init(str);
						X509Certificate cert = manager.getCertificatesByUsage(
								EnumKeyUsage.KEYENCIPHERMENT).get(0);
						certificateBase64 = CertificateUtils
								.getCertificateBase64(cert);

						return true;
					} catch (KeyStoreException | NoSuchAlgorithmException
							| CertificateException | ProviderException
							| IOException e) {

						if (e instanceof KeyStoreException) {
							setFailReason(Constants.ERR_MSG_CARD_NOT_FOUND);
						} else if (e instanceof IOException) {
							String mensaje = "";
							try {
								mensaje = e.getCause().getCause().getMessage();
							} catch (Exception ex1) {
							}
							if (mensaje.equals("CKR_PIN_LOCKED")) {
								setFailReason(Constants.ERR_MSG_PIN_LOCKED);

							} else {
								setFailReason(Constants.ERR_MSG_WRONG_PING);
							}
						} else if (e instanceof ProviderException) {
							String mensaje = "";

							mensaje = e.getMessage();
							;
							if (mensaje.indexOf("does not exist") > 0) {
								setFailReason(Constants.ERR_MSG_DRIVER_NOT_FOUND);
							} else if (mensaje.indexOf("Initialization failed") > 0) {
								setFailReason(Constants.ERR_MSG_CARD_READER_NOT_FOUND);
							}
						}

						return false;

					}
					
					
				case Constants.USING_P12:
					DeviceManager manager = new P12Manager();
					manager.setFileDir(getP12FilePath());
					try {

						manager.init(str);
						X509Certificate cert = manager.getCertificatesByUsage(
								EnumKeyUsage.KEYENCIPHERMENT).get(0);
						certificateBase64 = CertificateUtils
								.getCertificateBase64(cert);
						return true;

					} catch (KeyStoreException | NoSuchAlgorithmException
							| CertificateException | ProviderException
							| IOException e) {
						setFailReason(e.getMessage());
						return false;
					}

				default:
					return false;
				}
			}
		};
		return AccessController.doPrivileged(action);

	}
	

	/**
	 * 
	 * @param xml
	 * @param usingDevice
	 * @param pass
	 * @throws Exception 
	 */
	public void signXml(String xml, String usingDevice ,String pass) throws Exception{
		DeviceManager manager = null;
		XadesMixedSignature signature = new XadesMixedSignature();
		
		if (usingDevice == null){
			setSignatureFailReason(Constants.ERR_MSG_SIGN_USING);
			throw new NoManagerTypeException();
			
		}
			
			
		if (usingDevice.equals(Constants.USING_SMARTCARD)){
			manager.init(pass);
			XadesMixedSignature xadesMixedSignature = new XadesMixedSignature();
			xadesMixedSignature.setManager(manager);
			xadesMixedSignature.setParentId(Constants.PARENT_ID);
			xadesMixedSignature.setNodeTosign(Constants.NODE_TOSIGN);
			xadesMixedSignature.setUrlOcsp(Constants.URL_OCSP);
			xadesMixedSignature.setValidationType(XadesMixedSignature.USING_OCSP);
			xadesMixedSignature.setUrlTsa(Constants.URL_TSA);
			xadesMixedSignature.setUsage(EnumKeyUsage.DIGITALSIGNATURE);
			
			xadesMixedSignature.execute();
			
			
		}else if(usingDevice.equals(Constants.USING_P12)){
			
		}
	}

	/**
	 * @return the usingDevice
	 */
	public String getUsingDevice() {
		return usingDevice;
	}

	/**
	 * @param usingDevice
	 *            the usingDevice to set
	 */
	public void setUsingDevice(String usingDevice) {
		this.usingDevice = usingDevice;
	}

	/**
	 * @return the p12FilePath
	 */
	public String getP12FilePath() {
		return p12FilePath;
	}

	/**
	 * @param p12FilePath
	 *            the p12FilePath to set
	 */
	public void setP12FilePath(String p12FilePath) {
		this.p12FilePath = p12FilePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.Applet#stop()
	 */
	@Override
	public void stop() {
		AthenaSmartCard athena = (AthenaSmartCard) manager;
		try {
			athena.closeCardSession();
		} catch (IllegalAccessException | CardException | IOException
				| PKCS11Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the certificateBase64
	 */
	public String getCertificateBase64() {
		return certificateBase64;
	}


	/**
	 * @return the failReason
	 */
	public String getFailReason() {
		return failReason;
	}

	/**
	 * @param failReason
	 *            the failReason to set
	 */
	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}
	
	/**
	 * Prueba que hay comunicación con el applet
	 * @return
	 */
	public int testApplet(){
		return 1;
	}
	
	
	/**
	 * 
	 */
	public void findFile(){
		setP12FilePath(SecurityUtils.loadFileDialog().getAbsolutePath());
	}

	/**
	 * @return the signatureFailReason
	 */
	public String getSignatureFailReason() {
		return signatureFailReason;
	}

	/**
	 * @param signatureFailReason the signatureFailReason to set
	 */
	public void setSignatureFailReason(String signatureFailReason) {
		this.signatureFailReason = signatureFailReason;
	}
	

	
	

}
