/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus;

/**
 * @author Guillermo
 * @date 7/4/2015
 */
public class Constants {
	
	/**
	 * 
	 */
	public static final String URL_TSA = "http://tsa.sinpe.fi.cr/tsaHttp/";
	
	/**
	 * 
	 */
	public static final String URL_OCSP = "http://ocsp.sinpe.fi.cr/ocsp";
	
	/**
	 * 
	 */
	public static final String NODE_TOSIGN = "principal";
	
	/**
	 * 
	 */
	public static final String PARENT_ID = "documentoDigital";
	
	
	/**
	 * 
	 */
	public final static String USING_SMARTCARD = "smartcard";
	
	
	/**
	 * Deduce que se usará un athena smartcard
	 */
	public final static String USING_ATHENA_SMARTCARD = "athena";
	
	/**
	 * Define que se usará archivo p12 o pfx
	 */
	public final static String USING_P12 = "p12";
	
	
	public final static String ERR_MSG_CARD_NOT_FOUND = "La tarjeta no está insertada";
	public final static String ERR_MSG_PIN_LOCKED = "Pin bloqueado";
	public final static String ERR_MSG_WRONG_PING = "Pin incorrecto";
	public final static String ERR_MSG_DRIVER_NOT_FOUND = "No se encuentra el controlador de la tarjeta";
	public final static String ERR_MSG_CARD_READER_NOT_FOUND = "La lectora está desconectada";
	public final static String ERR_MSG_SIGN_USING= "No se definió un medio para firmar";
	
	

	/**
	 * 
	 */
	public Constants() {
		// TODO Auto-generated constructor stub
	}

}
