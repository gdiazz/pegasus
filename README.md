![alt tag](http://www.cruxconsultores.com/images/lgo-pegasus.png)

# Getting Started #
 
Proyecto basado en Maven


## Windows ##

Para un entorno Windows, se debe configurar la variable de entorno JAVA_HOME en 32 bits apuntando a:
C:\Program Files (x86)\Java\jdk1.6.X, esto debido a que las librerías que accesan a las lectoras están compiladas únicamente para 32bits.


## Compilación ##
```
#!bash

#Solo compilará el jar principal
mvn install 

#Compila el jar principal y exporta las dependencias dentro de /lib
#Se firman todos los jar
mvn install -P sign

```

## Obtener certificados desde diferentes recursos ##

### SmartCard Reader ###

Tener acceso a una lectora es sencillo. La libreria contiene las clases para hacerlo.

```
#!java

DeviceManager instance = new AthenaSmartCard();

//Inicializa la lectora
instance.init("pin");

//Imprime todos los certificados
List<X509Certificate> certificates = instance.getCertificates();
for (X509Certificate certificate : certificates) {
    System.out.println(certificate.getSubjectDN());
}

AthenaSmartCard athena = (AthenaSmartCard) instance;

//Libera la lectora
athena.closeCardSession();

```

### Archivos p12 y pfx ###
El proceso es el mismo que el anterior, la diferencia es que es necesario indicar el directorio donde se encuentra el contenedor de la firma. En este caso no es necesario liberar el certificado.

```
#!java

DeviceManager manager = new P12Manager();
manager.setFileDir("src/test/resources/certs/crux_firma.pfx");
manager.init("pin");
```

## Firma Digital ##
Primero se debe instanciar un DeviceManager, este se debe pasar por parámetro.

```
#!java

XadesMixedSignature xadesMixedSignature = new XadesMixedSignature();
xadesMixedSignature.setManager(manager);
xadesMixedSignature.setParentId(PARENT_ID); //Nodo padre que contiene la firma
xadesMixedSignature.setNodeTosign(NODE_TOSIGN); //Nodo a firmar
xadesMixedSignature.setUrlOcsp(URL_OCSP); //Servicio OCSP
xadesMixedSignature.setUrlTsa(URL_TSA); //Servicio de sello de de tiempo
xadesMixedSignature.setFileToSign(XML_TOSIGN); //Xml  a Firmar
xadesMixedSignature.setOutPutFile(XML_TOSIGN_OUTPUT); //Xml firmado
xadesMixedSignature.setUsage(EnumKeyUsage.DIGITALSIGNATURE);
xadesMixedSignature.execute();

```