/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.cruxconsultores.pegasus.utils;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Guillermo
 * @date 26/3/2015
 * 
 * De forma general. ¿Qué hace esta clase? (Sustituya por su respuesta)
 */
public class ConfigPropertiesTest {

	private static final String OCSP = "http://ocsp.sinpe.fi.cr/ocsp";
	private static final String TSA = "http://tsa.sinpe.fi.cr/tsaHttp/";
	
	@Test
	public void testConfigPropertiesOCSP() {
		System.out.println("testConfigPropertiesOCSP");
		String actual = ConfigProperties.URL_OCSP;
		assertTrue(actual.equals(OCSP));
	}
	
	@Test
	public void testConfigPropertiesTSA() {
		System.out.println("testConfigPropertiesTSA");
		String actual = ConfigProperties.URL_TSA;
		assertTrue(actual.equals(TSA));
	}

	
	
//	@Test
//	public void testConfigPropertiesDIR() {
//		System.out.println("testConfigPropertiesTSA");
//		String actual = ConfigProperties.DIR_CA;
//		assertTrue(actual.equals(TSA));
//	}
}
