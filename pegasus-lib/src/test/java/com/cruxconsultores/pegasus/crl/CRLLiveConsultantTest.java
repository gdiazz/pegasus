package com.cruxconsultores.pegasus.crl;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.cruxconsultores.pegasus.utils.CertificateUtils;

import es.mityc.javasign.certificate.CertStatusException;
import es.mityc.javasign.certificate.ICertStatus;

public class CRLLiveConsultantTest {
	
	CRLLiveConsultant crlLiveConsultant;
	private static String CERT_FILE_PATH = "src/test/resources/certs/test_certificate.cer";
	X509Certificate certificate;
	
	@Before
	public void setUp(){
		try {
			certificate = CertificateUtils.loadCertificateFromPath(CERT_FILE_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		
		crlLiveConsultant = new CRLLiveConsultant();
	}

	@Test
	public void testGetCertChainStatus(){
		try {
			List<ICertStatus> listStatus = crlLiveConsultant.getCertChainStatus(certificate);
			assertTrue(listStatus.size() > 0);
		} catch (CertStatusException e) {
			e.printStackTrace();
		}
	}
}
