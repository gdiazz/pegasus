package com.cruxconsultores.pegasus.crl;

import static org.junit.Assert.*;

import java.security.cert.CRL;

import org.junit.Before;
import org.junit.Test;


public class ConsumirCRLTest {
	
	ConsumirCRL consumirCrl;
	private static final String CRL_URL = "http://fdi.sinpe.fi.cr/repositorio/CA%20SINPE%20-%20PERSONA%20FISICA.crl";
	
	@Before
	public void setUp(){
		consumirCrl = new ConsumirCRL(CRL_URL);
	}
	
	@Test
	public void testGetCrl(){
		System.out.println("Test: testGetCrl");
		CRL crl = consumirCrl.getCrl();
		assertNotNull(crl);

	}

}
