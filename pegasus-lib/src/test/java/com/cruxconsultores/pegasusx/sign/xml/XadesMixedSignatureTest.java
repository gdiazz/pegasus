package com.cruxconsultores.pegasusx.sign.xml;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.smartcardio.CardException;

import org.junit.Ignore;
import org.junit.Test;

import sun.security.pkcs11.wrapper.PKCS11Exception;

import com.cruxconsultores.pegasus.device.DeviceManager;
import com.cruxconsultores.pegasus.device.EnumKeyUsage;
import com.cruxconsultores.pegasus.device.P12Manager;
import com.cruxconsultores.pegasus.device.model.AthenaSmartCard;

public class XadesMixedSignatureTest {

	private static final String URL_TSA = "http://tsa.sinpe.fi.cr/tsaHttp/";
	private static final String URL_OCSP = "http://ocsp.sinpe.fi.cr/ocsp";
	private static final String XML_TOSIGN_OUTPUT = "src/test/resources/xml/signed/xmlToSign_signed.xml";
	private static final String XML_TOSIGN = "src/test/resources/xml/xmlToSign.xml";
	private static final String NODE_TOSIGN = "principal";
	private static final String PARENT_ID = "documentoDigital";
	private static final String PASSWORD_INIT = "C4ux2014Cert";

	@Test
	public void testSiganture() throws Exception {
		System.out.println("XadesMixedSignatureTest: ");
		DeviceManager manager = new P12Manager();
		manager.setFileDir("src/test/resources/certs/crux_firma.pfx");
		manager.init(PASSWORD_INIT);

		XadesMixedSignature xadesMixedSignature = new XadesMixedSignature();
		xadesMixedSignature.setManager(manager);
		xadesMixedSignature.setParentId(PARENT_ID);
		xadesMixedSignature.setNodeTosign(NODE_TOSIGN);
		xadesMixedSignature.setValidationType(XadesMixedSignature.USING_CRL);
		xadesMixedSignature.setUrlTsa(URL_TSA);
		xadesMixedSignature.setFileToSign(XML_TOSIGN);
		xadesMixedSignature.setOutPutFile(XML_TOSIGN_OUTPUT);
		xadesMixedSignature.setUsage(EnumKeyUsage.DIGITALSIGNATURE);
		xadesMixedSignature.execute();

	}
	@Ignore
	@Test
	public void testSignatureUsingAthena() throws KeyStoreException,
			NoSuchAlgorithmException, CertificateException, ProviderException,
			IOException, IllegalAccessException, CardException, PKCS11Exception {
		DeviceManager manager = new AthenaSmartCard();
		manager.init("1189");

		List<X509Certificate> certs = manager.getCertificates();
		for (X509Certificate x509Certificate : certs) {
			System.out.println(x509Certificate.getSubjectDN());
		}

		AthenaSmartCard athena = (AthenaSmartCard) manager;
		athena.closeCardSession();
	}

	@Ignore
	@Test
	public void testSignatureDialog() throws Exception {
		// manager.init(PASSWORD_INIT);
		XadesMixedSignature xadesMixedSignature = new XadesMixedSignature();
		// xadesMixedSignature.setManager(manager);
		xadesMixedSignature.setParentId(PARENT_ID);
		xadesMixedSignature.setNodeTosign(NODE_TOSIGN);
		xadesMixedSignature.setUrlOcsp(URL_OCSP);
		xadesMixedSignature.setValidationType(XadesMixedSignature.USING_OCSP);
		xadesMixedSignature.setUrlTsa(URL_TSA);
		xadesMixedSignature.setFileToSign(XML_TOSIGN);
		xadesMixedSignature.setOutPutFile(XML_TOSIGN_OUTPUT);
		xadesMixedSignature.setUsage(EnumKeyUsage.DIGITALSIGNATURE);
		xadesMixedSignature.execute();
	}

	@Ignore
	@Test
	public void testSignatureNoFileSystem() throws Exception {
		System.out.println("testSignatureNoFileSystem: ");
		DeviceManager manager = new P12Manager();
		manager.setFileDir("src/test/resources/certs/crux_firma.pfx");
		manager.init(PASSWORD_INIT);

		XadesMixedSignature xadesMixedSignature = new XadesMixedSignature();
		xadesMixedSignature.setManager(manager);
		xadesMixedSignature.setParentId(PARENT_ID);
		xadesMixedSignature.setNodeTosign(NODE_TOSIGN);
		xadesMixedSignature.setValidationType(XadesMixedSignature.USING_CRL);
		xadesMixedSignature.setUrlTsa(URL_TSA);
		xadesMixedSignature.setFileToSign(XML_STR);
		xadesMixedSignature.setOutPutFile(XML_TOSIGN_OUTPUT);
		xadesMixedSignature.setUsage(EnumKeyUsage.DIGITALSIGNATURE);
		xadesMixedSignature.execute();
	}

	public final static String XML_STR = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
									+ "<documento id=\"documentoDigital\">\n"
									+ "	<firma id=\"principal\">\n"
									+ "		\n"
									+ "	</firma>\n"
									+ "</documento>";

}
