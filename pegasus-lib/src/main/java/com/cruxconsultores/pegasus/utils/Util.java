/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cruxconsultores.pegasus.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;

/**
 * Clase encargada de metodos basicos y genericos
 * @author Guillermo
 */
public class Util {
    
    /**
     * Obtiene un certificado .cer u otro desde una direccion en disco duro
     * @param dirFile
     * @return Certificado
     * @throws java.io.FileNotFoundException
     * @throws java.security.cert.CertificateException
     */
	@Deprecated
    public static X509Certificate loadCert(String dirFile) throws FileNotFoundException, CertificateException{
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        FileInputStream isCertCA = new FileInputStream(new File(dirFile));
        X509Certificate rootCert = (X509Certificate) cf.generateCertificate(isCertCA);
        return rootCert;
    }
    
    /**
     * Obtiene un certificado .cer u otro desde una direccion en disco duro
     * @param certIS
     * @return Certificado
     * @throws java.security.cert.CertificateException
     */
    public static X509Certificate loadCert(InputStream certIS) throws CertificateException{
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate rootCert = (X509Certificate) cf.generateCertificate(certIS);
        return rootCert;
    }
    public static X509CRL loadCRL(InputStream fis) throws CertificateException, CRLException, IOException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509CRL crl = (X509CRL) cf.generateCRL(fis);
        //fis.close();
        return crl;
    }
    
}
