
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cruxconsultores.pegasus.utils;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;

/**
 * Esta clase se encarga con algunas utilidades en los certiifcados como obtener
 * el DN en otro formato.
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 27/02/2014, 03:56:50 PM
 * @version 1.0 Copyright © 2014 by Crux Consultores All Rights Reserved.
 */
public class CertificateUtils {

    private static Log logger = LogFactory.getLog(CertificateUtils.class);

    private static String certificateUsage = "";

    /**
     * Este metodo es exclusivo para dividir en 2 el DN de un certificado la
     * idea es extraer el CN del DN
     *
     * @param s DN del certificado
     * @return Un array con los DN y el RESTO del DN en cada campo del vector
     */
    public static String[] splitDnByCn(String s) {
        s = s.toUpperCase();
        s = s.replace("SURNAME=", "s=");
        s = s.replace("GIVENNAME=", "g=");
        s = s.replace("SERIALNUMBER=", "sn=");
        s = s.replace("EMAILADDRESS=", "email=");
        s = s.replace("CN=", "cn=");
        s = s.replace("\"", "");
        s = s.replace("OU=", "ou=");
        s = s.replace("O=", "o=");
        s = s.replace("C=", "c=");
        s = s.replace("ST=", "st=");
        s = s.replace(", ", ",");

        //Saca el CN del DN de toda la hilera
        //
        String[] dn = new String[2];
        int i = s.indexOf("cn");
        int j = i;

        for (int k = i; k < s.length(); k++) {
            System.out.println(s.charAt(k));
            j++;
            if (s.charAt(k) == ',') {
                k = s.length();
            }
        }
        if (i != -1) {
            String temp = s.substring(i, j);
            dn[0] = temp;
            dn[1] = s.replace(temp, "");
            int st = dn[1].indexOf("st=");

            if (st > -1) {
                int stend = dn[1].indexOf("=", st + 3);
                String sub = dn[1].substring(st, stend - 2);
                dn[1] = dn[1].replace(sub, sub.replaceAll(",", " "));
            }

            if (!dn[1].contains("CPF")) {
                System.out.println("No tiene");
                StringBuilder strb = new StringBuilder(dn[1]);
                int ou = strb.indexOf("ou=");
                strb.insert(ou + 3, "NUB-");
                dn[1] = strb.toString();

            }
        }
        System.out.println(dn[0]);
        System.out.println(dn[1]);
        return dn;
    }

    /**
     * Convierte una cadena de texto en base64 en un certificado X509
     *
     * @param cert Cadena de texto del certificado en base64
     * @return Un certificado X509
     * @throws CertificateException
     */
    public static X509Certificate getCertificate(String cert) throws CertificateException {
        cert = cert.replace("-----BEGIN CERTIFICATE-----", "").replace("-----END CERTIFICATE-----", "");

        byte[] decoded = new Base64().decode(cert.getBytes());
        X509Certificate certificate = null;
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        InputStream in = new ByteArrayInputStream(decoded);
        certificate = (X509Certificate) certFactory.generateCertificate(in);

        return certificate;
    }

    /**
     * Devuelve un certificado en base64
     *
     * @param cert Certficiado X509
     * @return
     * @throws CertificateException
     */
    public static String getCertificateBase64(X509Certificate cert) throws CertificateException {

        byte[] encoded = new Base64().encode(cert.getEncoded());

        return new String(encoded);
    }

    /**
     * Convierte array binario en base64 texto
     *
     * @param bytes Arreglo de bytes
     * @return
     */
    public static String bytesToBase64(byte[] bytes) {
        byte[] encBytes = new Base64().encode(bytes);
        String str = new String(encBytes);
        return str;
    }

    /**
     * Convierte array binario en base64 texto
     *
     * @param s Bytes en base64
     * @return Cadena de texto
     */
    public static String base64ToBytes(String s) {
        byte[] encBytes = null;
        try {
            encBytes = new Base64().decode(s.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Utils:", ex);
        }
        String str = new String(encBytes);
        return str;
    }

    /**
     * Guarda un archivo con las llaves encodificadas
     *
     * @param path Directorio donde se guardará las llaves generadas
     * @param keyPair Par de llaves generadas
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void saveKeyDatFile(String path, KeyPair keyPair) throws FileNotFoundException, IOException {
        File f = new File(path);
        File fparent = new File(f.getParent());
        System.out.println(f.getParent());
        System.out.println("getCanonicalPath " + f.getCanonicalPath());

        if (fparent.mkdir()) {
            FileOutputStream fos = new FileOutputStream(f);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            Writer writer = new BufferedWriter(osw);
            writer.append("-----BEGIN ENCRYPTED PUBLIC KEY-----"
                    + CertificateUtils.bytesToBase64(keyPair.getPublic().getEncoded())
                    + "-----END ENCRYPTED PUBLIC KEY-----");
            writer.append("\r\n\r\n");
            writer.append("-----BEGIN ENCRYPTED PRIVATE KEY-----"
                    + CertificateUtils.bytesToBase64(keyPair.getPrivate().getEncoded())
                    + "-----END ENCRYPTED PRIVATE KEY-----");
            writer.close();
        }

    }

    /**
     * Parsea una llave publica de base64
     *
     * @param strKey Cadena de base64 de la llave publica
     * @return Llave pública generada
     */
    public static PublicKey parsePublicKey(String strKey) {
        PublicKey key = null;
        RSAKeyParameters rsa = null;
        strKey = strKey.replaceAll("-----BEGIN ENCRYPTED PRIVATE KEY-----", "");
        strKey = strKey.replaceAll("-----END ENCRYPTED PRIVATE KEY-----", "");
        try {
            rsa = (RSAKeyParameters) PublicKeyFactory.createKey(new Base64().decode(strKey.getBytes("UTF-8")));
            RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
            KeyFactory kf = KeyFactory.getInstance("RSA");
            key = kf.generatePublic(rsaSpec);
        } catch (IOException ex) {
            logger.error("Utils:", ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error("Utils:", ex);
        } catch (InvalidKeySpecException ex) {
            logger.error("Utils:", ex);
        }

        return key;
    }

    /**
     * Parsea una llave privada de String a PrivateKey
     *
     * @param strKey Cadena de texto de llave privada en base64
     * @return Llave Privada
     */
    public static PrivateKey parsePrivateKey(String strKey) {
        PrivateKey key = null;
        RSAKeyParameters rsa = null;

        strKey = strKey.replaceAll("-----BEGIN ENCRYPTED PRIVATE KEY-----", "");
        strKey = strKey.replaceAll("-----END ENCRYPTED PRIVATE KEY-----", "");

        try {
            rsa = (RSAKeyParameters) PrivateKeyFactory.createKey(new Base64().decode(strKey.getBytes()));
            RSAPrivateKeySpec rsaSpec = new RSAPrivateKeySpec(rsa.getModulus(), rsa.getExponent());
            KeyFactory kf = KeyFactory.getInstance("RSA");
            key = kf.generatePrivate(rsaSpec);
        } catch (IOException ex) {
            System.out.println(ex);
            logger.error("Utils:", ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error("Utils:", ex);
            System.out.println(ex);
        } catch (InvalidKeySpecException ex) {
            logger.error("Utils:", ex);
            System.out.println(ex);
        }

        return key;
    }

    /**
     * Este método obtiene los puntos de distribución de CRL de un certificado
     *
     * @param cert Es el certificado a analizar
     * @return Devuelve una lista con todos los puntos de distrubucion que
     * contiene el certificado
     * @throws CertificateParsingException
     * @throws IOException
     */
    public static List<String>
            getCaDistributionPoints(X509Certificate cert) throws CertificateParsingException, IOException {
        byte[] crldpExt = cert
                .getExtensionValue(X509Extensions.CRLDistributionPoints.getId());
        if (crldpExt == null) {
            return new ArrayList<String>();
        }
        ASN1InputStream oAsnInStream = new ASN1InputStream(
                new ByteArrayInputStream(crldpExt));
        DERObject derObjCrlDP = oAsnInStream.readObject();
        DEROctetString dosCrlDP = (DEROctetString) derObjCrlDP;
        byte[] crldpExtOctets = dosCrlDP.getOctets();
        ASN1InputStream oAsnInStream2 = new ASN1InputStream(
                new ByteArrayInputStream(crldpExtOctets));
        DERObject derObj2 = oAsnInStream2.readObject();
        CRLDistPoint distPoint = CRLDistPoint.getInstance(derObj2);
        List<String> crlUrls = new ArrayList<String>();
        for (DistributionPoint dp : distPoint.getDistributionPoints()) {
            DistributionPointName dpn = dp.getDistributionPoint();

            if (dpn != null
                    && dpn.getType() == DistributionPointName.FULL_NAME) {
                GeneralName[] genNames = GeneralNames.getInstance(
                        dpn.getName()).getNames();
                for (GeneralName genName : genNames) {
                    if (genName.getTagNo() == GeneralName.uniformResourceIdentifier) {
                        String url = DERIA5String.getInstance(genName.getName()).getString().replaceAll("crl", "crt");
                        crlUrls.add(url);
                    }
                }
            }
        }
        return crlUrls;
    }

    public static X509Certificate getCaFakeFromCertificate(X509Certificate certificate) throws IOException, CertificateException {

        String certUrl = getCaDistributionPoints(certificate).get(0);
        String decodeUrl = java.net.URLDecoder.decode(certUrl);

        String ca = "CA SINPE - PERSONA FISICA";
        System.out.println(decodeUrl);
        String fileName = FilenameUtils.getBaseName(decodeUrl);
        if (fileName.contains("PERSONA FISICA(1)")) {
            certificateUsage = "(1)";
        }
        ca += certificateUsage;
        
        String fileExtension = FilenameUtils.getExtension(decodeUrl);
        X509Certificate cert = null;
        boolean existsCrl = false;
        File carpetaTemporal = null;

        if (fileName.contains(ca)) {

            carpetaTemporal = new File(ConfigProperties.DIR_CA + "/" + fileName + "." + fileExtension);
            System.out.println(carpetaTemporal.exists());
            URL url = new URL(certUrl);
            if (!carpetaTemporal.exists()) {
                url.openConnection();
                InputStream file = url.openStream();
                byte[] data = IOUtils.toByteArray(file);
                org.apache.commons.io.FileUtils.writeByteArrayToFile(carpetaTemporal, data);
            } else {
                existsCrl = true;
            }

        } else {
            carpetaTemporal = new File(ConfigProperties.DIR_CA + "/" + ca + "." + fileExtension);
        }
        //Obtiene el crl
        cert = Util.loadCert(carpetaTemporal.getAbsolutePath());
        return cert;
    }

    public static X509Certificate getCaFromCertificate(X509Certificate certificate) throws IOException, CertificateException {

        String certUrl = getCaDistributionPoints(certificate).get(0);
        String decodeUrl = java.net.URLDecoder.decode(certUrl);
        String ca = "CA SINPE - PERSONA FISICA";
        System.out.println(decodeUrl);
        String fileName = FilenameUtils.getBaseName(decodeUrl);
        String fileExtension = FilenameUtils.getExtension(decodeUrl);
        X509Certificate cert = null;
        boolean existsCrl = false;
        File carpetaTemporal = null;

        if (fileName.contains(ca)) {

            carpetaTemporal = new File(ConfigProperties.DIR_CA + "/" + fileName + "." + fileExtension);

            URL url = new URL(certUrl);
            if (!carpetaTemporal.exists()) {
                url.openConnection();
                InputStream file = url.openStream();
                byte[] data = IOUtils.toByteArray(file);
                org.apache.commons.io.FileUtils.writeByteArrayToFile(carpetaTemporal, data);
            } else {
                existsCrl = true;
            }

        } else {
            carpetaTemporal = new File(ConfigProperties.DIR_CA + "/" + ca + "." + fileExtension);
        }
        //Obtiene el crl
        cert = Util.loadCert(carpetaTemporal.getAbsolutePath());
        return cert;
    }
    
    /**
     * Obtiene un certificado .cer u otro desde una direccion en disco duro
     * @param dirFile
     * @return Certificado
     * @throws java.io.FileNotFoundException
     * @throws java.security.cert.CertificateException
     */
    public static X509Certificate loadCertificateFromPath(String dirFile) throws FileNotFoundException, CertificateException{
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        FileInputStream isCertCA = new FileInputStream(new File(dirFile));
        X509Certificate rootCert = (X509Certificate) cf.generateCertificate(isCertCA);
        return rootCert;
    }

}
