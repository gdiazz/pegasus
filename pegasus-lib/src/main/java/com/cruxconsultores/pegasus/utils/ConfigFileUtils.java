/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cruxconsultores.pegasus.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Esta clase se encarga de la lectura y escritura del archiv de configuracion
 * @author Guillermo
 */
public class ConfigFileUtils {

    
    /**
     * 
     * @param propertie
     * @return 
     */
    public static String readPropertie(String propertie){
        try {
            ConfigProperties.pastePropertiesToUser();
        } catch (IOException ex) {
            Logger.getLogger(ConfigFileUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
	Properties prop = new Properties();
        InputStream is = null;
        try {
             is = new FileInputStream(ConfigProperties.FILE_PROPERTIES);
            prop.load(is);
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally{
            try {
                is.close();
            } catch (IOException ex) {
                
            }
            
        }
        return prop.getProperty(propertie);
    }
    
/**
     * Actualiza una propiedad
     * @param propertie
     * @param value 
     */
    public static void updatePropertie(String propertie, String value){
	Properties prop = new Properties();
	OutputStream output = null;
         try {
            ConfigProperties.pastePropertiesToUser();
        } catch (IOException ex) {
            Logger.getLogger(ConfigFileUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
	try {
                String p = "/config/config.properties";
                InputStream is = p.getClass().getResourceAsStream(p);
		output = new FileOutputStream(p.getClass().getResource(p).getPath());
                prop.setProperty(propertie, value);
 
		prop.store(output, null);
 
	} catch (IOException io) {
            
		io.printStackTrace();
	} finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
 
	}
    }
    
    //Test
    public static void main (String args []){
        System.out.println(ConfigFileUtils.readPropertie("urlocsp"));
        ConfigFileUtils.updatePropertie("urlocsp", "http://ocsp.sinpe.fi.cr/ocsp");
        System.out.println(ConfigFileUtils.readPropertie("urlocsp"));
        
    }
    
}

