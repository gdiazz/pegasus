/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cruxconsultores.pegasus.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.security.AccessController;
import java.security.PrivilegedAction;

import javax.swing.JFileChooser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 14/02/2014, 02:26:22 PM
 * @version 1.0 Copyright © 2010 by Crux Consultores All Rights Reserved.
 */
public class FileUtils {

	private static Log logger = LogFactory.getLog(FileUtils.class);

	/**
	 * Parsea un documento xml a Dom
	 *
	 * @param path
	 *            Ruta del archivo xml a parsear
	 * @return
	 */
	public static Document parseXmlTODoc(String path) {
		File XmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			logger.error("Utils:", ex);
		}

		Document doc = null;
		try {
			doc = dBuilder.parse(XmlFile);
		} catch (SAXException ex) {
			logger.error("Utils:", ex);
		} catch (IOException ex) {
			logger.error("Utils:", ex);
		}

		doc.getDocumentElement().normalize();

		return doc;
	}

	public static String showJFileChooser() throws IOException {

		JFileChooser chooser = new JFileChooser();
		String dir = "";
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setDialogType(JFileChooser.APPROVE_OPTION);

		int intchooser = chooser.showSaveDialog(chooser);
		return chooser.getSelectedFile().getAbsolutePath();

	}

	public static void saveFile(String path, String str)
			throws FileNotFoundException, IOException {
		File f = new File(path);
		f.mkdir();
		FileOutputStream fos = new FileOutputStream(f);
		OutputStreamWriter osw = new OutputStreamWriter(fos);
		Writer writer = new BufferedWriter(osw);
		writer.write(str);
		writer.close();
	}

	public static String readLineFile(String dir) {
		String fileDir = "";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(dir));
			String readed;
			while ((readed = reader.readLine()) != null) {
				fileDir = readed;
			}
		} catch (FileNotFoundException ex) {
			logger.error("Utils:", ex);
		} catch (IOException ex) {
			logger.error("Utils:", ex);
		}
		return fileDir;
	}

	public static void saveDocumentToFile(final Document doc, final String file) {
		// write the content into xml file

		AccessController.doPrivileged(new PrivilegedAction() {
			@Override
			public Object run() {
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = null;
				try {
					transformer = transformerFactory.newTransformer();
				} catch (TransformerConfigurationException ex) {
					logger.error("Utils:", ex);
				}
				DOMSource source = new DOMSource(doc);

				StreamResult result = new StreamResult(new File(file));

				try {
					// Output to console for testing
					// StreamResult result = new StreamResult(System.out);

					transformer.transform(source, result);
				} catch (TransformerException ex) {
					logger.error("Utils:", ex);
				}
				return null;

			}
		});

	}

	protected String DocumentToString(String resource) {

		Document doc = parseXmlTODoc(resource);
		TransformerFactory tfactory = TransformerFactory.newInstance();
		Transformer serializer;
		StringWriter stringWriter = new StringWriter();
		try {
			serializer = tfactory.newTransformer();
			serializer.transform(new DOMSource(doc), new StreamResult(
					stringWriter));
		} catch (TransformerException e) {
			System.err.println("Error al imprimir el documento");
			e.printStackTrace();
			System.exit(-1);
		}
		return stringWriter.toString();
	}

	public static Document inputStreamToDocument(InputStream in) {
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document ret = null;

		try {
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		try {
			ret = builder.parse(new InputSource(in));
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	@SuppressWarnings("restriction")
	public static void writeXML(Document doc, OutputStream out) {
		 try {
	            Transformer tr = TransformerFactory.newInstance().newTransformer();
	            tr.setOutputProperty(OutputKeys.INDENT, "no");
	            tr.setOutputProperty(OutputKeys.METHOD, "xml");
	            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

	            // send DOM to file
	            tr.transform(new DOMSource(doc), 
	                                 new StreamResult(out));

	        } catch (TransformerException te) {
	            System.out.println(te.getMessage());
	        }

	}

}
