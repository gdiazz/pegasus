package com.cruxconsultores.pegasus.utils;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.cruxconsultores.pegasus.model.DatosFirmaEnum;


/**
 * Utileria para el proceso de firma
 *
 * @author Luis Gabriel Lara Arias
 */
public class Utileria {

    private JButton boton;

    public Utileria() {
    }

    /**
     * Se encarga se convertir un archivo a bytes
     *
     * @param direccion del documento
     * @return array de bytes
     * @throws FileNotFoundException
     * @throws IOException
     */
    public byte[] convertirDocumento(String direccion) throws FileNotFoundException, IOException {

        FileInputStream fis = new FileInputStream(direccion);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
                System.out.println("Lectura " + readNum + " bytes,");
            }
        } catch (IOException ex) {
        }

        byte[] bytes = bos.toByteArray();

        return bytes;

    }

    public byte[] getBytesFromFile(File file) throws IOException {

        InputStream is = new FileInputStream(file);
        long length = file.length();
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }

    /**
     * Escribe el archivo en el disco
     *
     * @param nombre
     * @param datos
     * @throws Exception
     */
    public void escribirArchivo(String nombre, byte[] datos) throws Exception {
        File archivo = new File(nombre);
        FileOutputStream fos = new FileOutputStream(archivo);
        fos.write(datos);
        fos.flush();
        fos.close();
    }

    public File buscarArchivoFirmar() {

        File file = null;
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("Docuemento PDF a Firmar", "PDF");
        fileChooser.setFileFilter(filtroImagen);
        boton = new JButton("Selección");
        int resultado = fileChooser.showOpenDialog(boton);
        fileChooser.showOpenDialog(fileChooser);

        fileChooser.setAcceptAllFileFilterUsed(false);

        file = fileChooser.getSelectedFile();
        return file;
    }

    public String guardarArchivoFirmar(String[] formatoArchivo) {

        JFileChooser jFGuardar = new JFileChooser();
        boton = new JButton("Selección");
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("Selección de archivos", formatoArchivo);
        jFGuardar.setFileFilter(filtroImagen);
        jFGuardar.setApproveButtonText("Prueba");
        jFGuardar.setAcceptAllFileFilterUsed(false);
        String ruta = "";

        try {
            if (jFGuardar.showSaveDialog(null) == jFGuardar.APPROVE_OPTION) {
                ruta = jFGuardar.getSelectedFile().getAbsolutePath();

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ruta;
    }

    public void mensajes(String tipoError, int error, String titulo) {

        JOptionPane.showMessageDialog(null, tipoError, titulo, error);

    }
    
    /**
     * Este método convierte un archivo xml a un Dom
     * @param s Ruta del archivo a parsear
     * @return Devuelve un Documento DOM
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
     public static Document parseXML(String s) throws ParserConfigurationException, SAXException, IOException{
	File fXmlFile = new File(s);
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
	return doc;
     }

         /**
     * Este metodo pretende obtener un dato unico de una hilera de atributos de certificados, por ejemplo:
     * CN=JORGE ARTURO VEGA NUÑEZ (FIRMA), OU=CIUDADANO, O=PERSONA FISICA; 
     * Se desea obtener solo "JORGE ARTURO VEGA NUÑEZ (FIRMA)"
     * 
     * @param subject Tipo String, es una hilera que contiene todo los atributos del certificado
     * @param enumDatos Este parametro indica que dato del certificado se desea obtener
     * @return String, devuelve el dato que se desea obtener
     */
    public static String getSubjectX500By(String subject, DatosFirmaEnum enumDatos){
        if (subject != null){
           subject = subject.concat(",");
            if (enumDatos == DatosFirmaEnum.COUNTRY_NAME_C){
                return getWordBetween(subject, "C=", ",");
                
            }else if (enumDatos == DatosFirmaEnum.NOMBRE_COMUN_CN){
                return getWordBetween(subject, "CN=", ",");
                
            }else if (enumDatos == DatosFirmaEnum.NUMERO_SERIE){
                return getWordBetween(subject + "AGREGADO", "SERIALNUMBER=", ",");
                
            }else if (enumDatos == DatosFirmaEnum.ORGANIZACION_O){
                return getWordBetween(subject, "O=", ",");
                
            }else if (enumDatos == DatosFirmaEnum.NOMBRE_FIRMAMNTE){
                return getWordBetween(subject, "GIVENNAME=", ",");
            }else if (enumDatos == DatosFirmaEnum.APELLIDOS_FIRMANTE){
                return getWordBetween(subject, "SURNAME=", ",");
            }
           
        }
           return null; 
    }
    
   /**
     * Este metodo se encagarga de obtener un un texto entre 2 campos consecutivos
     * por ejemplo: Pedro fue a la servcio, porque tenia ganas de orinar.
     * getWordBetween("," , "."), devolveria el texto [" porque tenia ganas de orinar"]
     * @param subject String, Es la hilera a 
     * @param begin String, Es el incio del analisis
     * @param end String, Es el final del análisis.
     * @return devuelve la palabra seleccionada
     */
    private static String getWordBetween(String subject, String begin, String end){
        int i = subject.indexOf(begin);
        if (i >= 0){
            int j = subject.substring(i).indexOf(end) + i;
            String palabra = subject.substring(i, j);
            return palabra.substring(palabra.indexOf("=") + 1);
        }

        return "";
    }
}
