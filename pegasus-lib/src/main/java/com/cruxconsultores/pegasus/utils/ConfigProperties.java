/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cruxconsultores.pegasus.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

/**
 *
 * @author Guillermo
 */
public class ConfigProperties {
    
    public static void pastePropertiesToUser() throws IOException{
        File f = new File(FILE_PROPERTIES);
        OutputStream out = null;
        InputStream  is = null;
        
        if (!f.exists()){
            try {
                is =  ConfigProperties.class.getResourceAsStream(SOURCE_PROPERTIES);
                out = new FileOutputStream(f);
                byte buffer [] = new byte[1024];
                int length = 0;
                while((length = is.read(buffer)) > 0){
                    out.write(buffer, 0 , length);
                }
            } catch (FileNotFoundException ex) {

            } catch (IOException ex) {

            }
            finally{
                out.close();
                is.close();
            }
            
        }
    }
    
    /**
     * Donde se localiza en el paquete el archivo de propiedades por defecto
     */
    public static String SOURCE_PROPERTIES = "/config/config.properties";
    
    /**
     * Donde se obtiene el archivo de propiedades
     */
    public static String FILE_PROPERTIES = System.getProperty("user.home")+"/pegasus.properties";
    
    /**
     * Url del servidor OCSP
     */
    public static String URL_OCSP = ConfigFileUtils.readPropertie("urlocsp");
            //"http://ocsp.sinpe.fi.cr/ocsp";
            //ConfigFileUtils.readPropertie("urlocsp");
    
    /**
     * Url del servidor de sellado de tiempo
     */
    public static String URL_TSA = ConfigFileUtils.readPropertie("urltsa");
            //"http://tsa.sinpe.fi.cr/tsaHttp/";
            //ConfigFileUtils.readPropertie("urltsa");
    
    /**
     * Url de descarga de las CA
     */
    public static String DIR_CA =  System.getProperty("java.io.tmpdir")+"/pegasus/ca";
    public static String DIR_CRL =  System.getProperty("java.io.tmpdir")+"/pegasus/crl";

    
    
}
