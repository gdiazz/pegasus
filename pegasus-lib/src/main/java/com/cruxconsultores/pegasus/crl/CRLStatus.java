/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.crl;

import es.mityc.javasign.certificate.AbstractCertStatus;
import es.mityc.javasign.certificate.IX509CRLCertStatus;
import es.mityc.javasign.certificate.RevokedInfo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Extensions;

/**
 * Esta clase obtiene mediante un certificado su validez Internamente se revisa
 * sus propiedades obteniendo los puntos de distribucion CRL con los cuales se
 * podrá validar su revocación
 *
 * @autor Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 13/02/2014, 10:09:19 AM
 * @version 1.0 Copyright © 2014 Crux Consultores All Rights Reserved.
 */
public final class CRLStatus extends AbstractCertStatus implements IX509CRLCertStatus {

    private X509CRL crl;
    private final X509Certificate cert;

    public CRLStatus(X509Certificate cert) {
        this.cert = cert;
        this.init();
    }


    /**
     * Revisa el certificado y llena las propiedades
     * de la clase Heredada
     */
    private void init() {
        try {
            ConsumirCRL concrl = new ConsumirCRL(getCrlDistributionPoints(cert).get(0));
            this.crl = concrl.getCrl();
        } catch (CertificateParsingException ex) {
            Logger.getLogger(CRLStatus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CRLStatus.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.setCertificate(cert);
        this.setRevokedInfo(getActualRevokedInfo(cert));

    }


    /**
     * Llena la información de revocación de un certificado
     * @param pcert Certificado X509
     * @return 
     */
    private RevokedInfo getActualRevokedInfo(X509Certificate pcert) {
        Object razon = null;
        Date fechaRevocacion = null;

        if (this.crl.isRevoked(pcert)) {
            this.setCertStatus(CERT_STATUS.revoked);
            fechaRevocacion = this.crl.getRevokedCertificate(pcert).getRevocationDate();
            razon = this.crl.getRevokedCertificate(certificate).toString();

            System.out.println(razon);
        } else {
            setCertStatus(CERT_STATUS.valid);
        }
        RevokedInfo info = new RevokedInfo(razon, fechaRevocacion);
        return info;

    }

    @Override
    public byte[] getEncoded() {
        try {
            return this.cert.getEncoded();
        } catch (CertificateEncodingException ex) {
            Logger.getLogger(CRLStatus.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public X509CRL getX509CRL() {
        return this.crl;
    }

    /**
     * Este método obtiene los puntos de distribución de CRL de un certificado
     *
     * @param cert Es el certificado a analizar
     * @return Devuelve una lista con todos los puntos de distrubucion que
     * contiene el certificado
     * @throws CertificateParsingException
     * @throws IOException
     */
    private static List<String>
            getCrlDistributionPoints(X509Certificate cert) throws CertificateParsingException, IOException {
        byte[] crldpExt = cert
                .getExtensionValue(X509Extensions.CRLDistributionPoints.getId());
        if (crldpExt == null) {
            return new ArrayList<String>();
        }
        ASN1InputStream oAsnInStream = new ASN1InputStream(
                new ByteArrayInputStream(crldpExt));
        DERObject derObjCrlDP = oAsnInStream.readObject();
        DEROctetString dosCrlDP = (DEROctetString) derObjCrlDP;
        byte[] crldpExtOctets = dosCrlDP.getOctets();
        ASN1InputStream oAsnInStream2 = new ASN1InputStream(
                new ByteArrayInputStream(crldpExtOctets));
        DERObject derObj2 = oAsnInStream2.readObject();
        CRLDistPoint distPoint = CRLDistPoint.getInstance(derObj2);
        List<String> crlUrls = new ArrayList<String>();
        for (DistributionPoint dp : distPoint.getDistributionPoints()) {
            DistributionPointName dpn = dp.getDistributionPoint();
            
            if (dpn != null
                    && dpn.getType() == DistributionPointName.FULL_NAME) {
                GeneralName[] genNames = GeneralNames.getInstance(
                        dpn.getName()).getNames();
                for (GeneralName genName : genNames) {
                    if (genName.getTagNo() == GeneralName.uniformResourceIdentifier) {
                        String url = DERIA5String.getInstance(genName.getName()).getString();
                        crlUrls.add(url);
                    }
                }
            }
        }
        return crlUrls;
    }

}
