/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.crl;

import es.mityc.javasign.certificate.CertStatusException;
import es.mityc.javasign.certificate.ICertStatus;
import es.mityc.javasign.certificate.ICertStatusRecoverer;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Esta clase es la implementación de la consulta del estado de revocación de
 * los certificados. La idea de esta implementación es lograr validar un
 * certificado mediante crl e incluirlo en las firmas XADES-XL así como su
 * validación de cadena de confianza.
 *
 * @autor Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 13/02/2014, 10:05:06 AM
 * @version 1.0 Copyright © 2014 Crux Consultores All Rights Reserved.
 */
public class CRLLiveConsultant implements ICertStatusRecoverer {

	public CRLLiveConsultant() {

	}

	/**
	 * Este método es la implementación de las interfaces de mityc donde se
	 * recupera la información del estado de revocacion. En este caso Se
	 * consulta todo por medio de crl
	 * 
	 * @param xc
	 *            Certificado X509
	 * @return Devuelve el estado de la cadena
	 * @throws CertStatusException
	 */
	@Override
	public ICertStatus getCertStatus(X509Certificate xc)
			throws CertStatusException {
		CRLStatus bloque = new CRLStatus(xc);
		return bloque;
	}

	/**
	 * No Implementado
	 * 
	 * @param list
	 * @return
	 * @throws CertStatusException
	 */
	@Override
	public List<ICertStatus> getCertStatus(List<X509Certificate> list)
			throws CertStatusException {
		return null;
	}

	/**
	 * Obtiene el estado de la cadena de confianza
	 * 
	 * @param xc
	 *            Certficado X509
	 * @return Lista con estados de cada certificado de la cadena
	 * @throws CertStatusException
	 */
	@Override
	public List<ICertStatus> getCertChainStatus(X509Certificate xc)
			throws CertStatusException {
		List<ICertStatus> lista = new ArrayList<ICertStatus>();
		CertificateFactory cf = null;
		try {
			cf = CertificateFactory.getInstance("X.509");
		} catch (CertificateException ex) {
			Logger.getLogger(CRLLiveConsultant.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		List<X509Certificate> mylist = new ArrayList<X509Certificate>();
		mylist.add(xc);
		CertPath certPath = null;
		try {
			certPath = cf.generateCertPath(mylist);
		} catch (CertificateException ex) {
			Logger.getLogger(CRLLiveConsultant.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		List<? extends Certificate> certificates = certPath.getCertificates();
		int certificatesSize = certificates.size();
		for (int i = 0; i < certificatesSize; i++) {
			X509Certificate certificateToValidate = (X509Certificate) certificates
					.get(i);
			// Si es el ultimo certificado estamos ante el certificado raiz en
			// el que el emisor es él mismo
			if (i == certificatesSize - 1) {
			}
			ICertStatus estado = new CRLStatus(certificateToValidate);
			lista.add(estado);
		}

		return lista;
	}

	/**
	 * No implementado
	 * 
	 * @param list
	 * @return
	 * @throws CertStatusException
	 */
	@Override
	public List<List<ICertStatus>> getCertChainStatus(List<X509Certificate> list)
			throws CertStatusException {
		throw new UnsupportedOperationException("Not supported yet."); // To
																		// change
																		// body
																		// of
																		// generated
																		// methods,
																		// choose
																		// Tools
																		// |
																		// Templates.
	}

}
