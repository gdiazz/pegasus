/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.crl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

/**
 * Esta clase se encarga de descargar los archivos CRL que pertenecen a un
 * Certificado. Los descarga en una carpeta temporal, para posteriormente
 * obtenerlos
 *
 * @autor Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 13/02/2014, 10:42:37 AM
 * @version 1.0 Copyright © 2014 Crux Consultores All Rights Reserved.
 */
public class ConsumirCRL {


    /**
     * Esta es al url a consultar el crl
     */
    private String crlUrl;
    
    /**
     * Este es el crl obtenido a partir de esa dirección
     */
    private X509CRL crl;
    
    /**
     * Directorio de descarga por defecto
     */
    private static final String DIR_TEMP = System.getProperty("java.io.tmpdir") + "/pegasus/crl";

    /**
     * Medianto una url el constructor, genera una descarga del archivo especificado
     * @param crlUrl 
     */
    public ConsumirCRL(String crlUrl) {
        this.crlUrl = crlUrl;
        try {
            this.downLoadCrl();
        } catch (IOException ex) {
            Logger.getLogger(ConsumirCRL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(ConsumirCRL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CRLException ex) {
            Logger.getLogger(ConsumirCRL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Obtiene el CRL descargado
     * @return 
     */
    public X509CRL getCrl() {
        return this.crl;
    }

    /**
     * Este método descarga un archivo de una url y lo guarda en la carpete
     * temporal definida por java tempdir/pegasus/crl y almacena todos los
     * archivos crl descargados
     *
     * @throws MalformedURLException
     * @throws IOException
     */
    private void downLoadCrl() throws MalformedURLException, IOException, FileNotFoundException, CertificateException, CRLException {
        String decodeUrl = java.net.URLDecoder.decode(crlUrl);
        String fileName = FilenameUtils.getBaseName(decodeUrl);
        String fileExtension = FilenameUtils.getExtension(decodeUrl);
        boolean existsCrl = false;
        
        File carpetaTemporal = new File(DIR_TEMP +"/"+ fileName + "." + fileExtension);
        URL url = new URL(crlUrl);
        if (!carpetaTemporal.exists()) {
            url.openConnection();
            InputStream file = url.openStream();
            byte[] data = IOUtils.toByteArray(file);
            FileUtils.writeByteArrayToFile(carpetaTemporal, data);
        }else{
            existsCrl = true;
        }
        //Obtiene el crl
        this.crl = getCrlFromFile(new FileInputStream(carpetaTemporal));
        Date now = new Date();
        if (existsCrl){
            if (now.after(this.crl.getNextUpdate())){
                carpetaTemporal.delete();
                url.openConnection();
                InputStream file = url.openStream();
                byte[] data = IOUtils.toByteArray(file);
                FileUtils.writeByteArrayToFile(carpetaTemporal, data);
                this.crl = getCrlFromFile(new FileInputStream(carpetaTemporal));
            }
        }
        
        
    }

    /**
     * Este metodo se encarga de cargar de un stream de datos a un crl
     *
     * @param fis Recibe un flujo de datos
     * @return Retorna un objeto CRL
     * @throws CertificateException
     * @throws CRLException
     */
    private X509CRL getCrlFromFile(InputStream fis) throws CertificateException, CRLException, IOException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509CRL crl = (X509CRL) cf.generateCRL(fis);
        fis.close();
        return crl;
    }
}
