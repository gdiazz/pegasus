/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cruxconsultores.pegasus.ocsp;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessController;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivilegedAction;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.ocsp.BasicOCSPResp;
import org.bouncycastle.ocsp.CertificateID;
import org.bouncycastle.ocsp.OCSPException;
import org.bouncycastle.ocsp.OCSPReq;
import org.bouncycastle.ocsp.OCSPReqGenerator;
import org.bouncycastle.ocsp.OCSPResp;
import org.bouncycastle.ocsp.SingleResp;

/**
 * Esta clase se encarga de la verificación OCSP, de los certificados
 *
 * @author memo
 */
public class OCSPVerifier {

    /**
     * Logs
     */
    static Logger log = Logger.getLogger(OCSPVerifier.class.getName());

    public static int GOOD = 0;
    public static int REVOKED = 2;
    public static int UNKNOW = 1;
    public static int EXPIRED = 4;
    public static int VALID = 5;
    
    private BasicOCSPResp basicResp;

    public void validateCertificate(X509Certificate cert) {
        
    }

    
    public BasicOCSPResp validate(X509Certificate ca, X509Certificate  cert) throws OCSPException, IOException {
        check(ca, cert);
        return basicResp;
    }

    public int check(X509Certificate ca, X509Certificate cert) throws OCSPException, MalformedURLException, IOException {

        AccessController.doPrivileged(new PrivilegedAction() {
            @Override
            public Object run() {
                // privileged code goes here, for example:
                Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
                return null; // nothing to return
            }
        });
        
        /*Se genera la petición con el certificado a verificar y su número de serie*/
        OCSPReqGenerator ocspReqGen = new OCSPReqGenerator();
        ocspReqGen.addRequest(new CertificateID(CertificateID.HASH_SHA1, ca, cert.getSerialNumber()));
        OCSPReq ocspReq = ocspReqGen.generate();

        
        /* Se establece la conexión HTTP con el ocsp del DNIe */
        URL url = new URL("http://ocsp.sinpe.fi.cr/ocsp");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        /* Se configuran las propiedades de la petición HTTP */
        con.setRequestProperty("Content-Type", "application/ocsp-request");
        con.setRequestProperty("Accept", "application/ocsp-response");
        con.setDoOutput(true);
        OutputStream out = con.getOutputStream();
        DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));
        /* Se obtiene la respuesta del servidos OCSP del DNIe */
        dataOut.write(ocspReq.getEncoded());

        dataOut.flush();
        dataOut.close();

        /* Se parsea la respuesta y se obtiene el estado del certificado retornado por el OCSP */
        InputStream in = (InputStream) con.getContent();
        OCSPResp ocspResponse = new OCSPResp(in);
        
        return getStatus(ocspResponse);
    }

    /**
     * 
     * @param response
     * @return 
     */
    public int getStatus(OCSPResp response) {
        try {
             basicResp = (BasicOCSPResp) response.getResponseObject();
            for (SingleResp singResp : basicResp.getResponses()) {
                if (singResp.getCertStatus() ==
                        org.bouncycastle.ocsp.CertificateStatus.GOOD){
                    return GOOD;
                    
                } else if (singResp.getCertStatus() instanceof 
                        org.bouncycastle.ocsp.UnknownStatus){
                    return UNKNOW;
                    
                } else if (singResp.getCertStatus() instanceof 
                        org.bouncycastle.ocsp.RevokedStatus){
                    return REVOKED;
                    
                } 
            }
        } catch (OCSPException ex) {
            java.util.logging.Logger.getLogger(OCSPVerifier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public int getStatus(BasicOCSPResp response) {
        for (SingleResp singResp : response.getResponses()) {
            if (singResp.getCertStatus() ==
                    org.bouncycastle.ocsp.CertificateStatus.GOOD){
                return GOOD;
                
            } else if (singResp.getCertStatus() instanceof
                    org.bouncycastle.ocsp.UnknownStatus){
                return UNKNOW;
                
            } else if (singResp.getCertStatus() instanceof
                    org.bouncycastle.ocsp.RevokedStatus){
                return REVOKED;
                 
            }
        }
        return 0;
    }

    /**
     * Validate keychain
     *
     * @param client is the client X509Certificate
     * @param trustedCerts is Array containing all trusted X509Certificate
     * @return true if validation until root certificate success, false
     * otherwise
     * @throws CertificateException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public boolean validateKeyChain(X509Certificate client,
            X509Certificate... trustedCerts) throws CertificateException,
            InvalidAlgorithmParameterException, NoSuchAlgorithmException,
            NoSuchProviderException {

        boolean found = false;
        int i = trustedCerts.length;
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        TrustAnchor anchor;
        Set anchors;
        CertPath path;
        List list;
        PKIXParameters params;
        CertPathValidator validator = CertPathValidator.getInstance("PKIX");

        while (!found && i > 0) {
            anchor = new TrustAnchor(trustedCerts[--i], null);
            anchors = Collections.singleton(anchor);

            list = Arrays.asList(new Certificate[]{client});
            path = cf.generateCertPath(list);

            params = new PKIXParameters(anchors);
            params.setRevocationEnabled(false);

            if (client.getIssuerDN().equals(trustedCerts[i].getSubjectDN())) {
                try {
                    validator.validate(path, params);
                    if (isSelfSigned(trustedCerts[i])) {
                        // found root ca
                        found = true;
                        System.out.println("validating root" + trustedCerts[i].getSubjectX500Principal().getName());
                    } else if (!client.equals(trustedCerts[i])) {
                        // find parent ca
                        System.out.println("validating via:" + trustedCerts[i].getSubjectX500Principal().getName());
                        found = validateKeyChain(trustedCerts[i], trustedCerts);
                    }
                } catch (CertPathValidatorException e) {
                    // validation fail, check next certifiacet in the trustedCerts array
                }
            }
        }

        return found;
    }

    /**
     *
     * @param cert is X509Certificate that will be tested
     * @return true if cert is self signed, false otherwise
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public boolean isSelfSigned(X509Certificate cert)
            throws CertificateException, NoSuchAlgorithmException,
            NoSuchProviderException {
        try {
            PublicKey key = cert.getPublicKey();

            cert.verify(key);
            return true;
        } catch (SignatureException ex) {
            return false;
        } catch (InvalidKeyException ex) {
            return false;
        }
    }
}
