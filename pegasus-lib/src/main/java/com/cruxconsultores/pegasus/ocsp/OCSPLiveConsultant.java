package com.cruxconsultores.pegasus.ocsp;

/**
 *
 * @author glara
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cruxconsultores.pegasus.utils.CertificateUtils;

import es.mityc.javasign.certificate.CertStatusException;
import es.mityc.javasign.certificate.ICertStatus;
import es.mityc.javasign.certificate.ICertStatusRecoverer;

/**
 * <p>
 * Recupera el estado de un certificado mediante una consulta OCSP a un OCSP
 * responder disponible por canal HTTP.</p>
 *
 * @author Ministerio de Industria, Energía y Turismo
 * @version 1.0
 */
@SuppressWarnings("deprecation")
public class OCSPLiveConsultant implements ICertStatusRecoverer {

    /**
     * Internacionalizador.
     */
    private InputStream file;
    /**
     * Ruta del servidor HTTP OCSP al que se realiza la consulta.
     */
    private String servidorOCSP;
    /**
     * Validador de confianza
     */


    /**
     * <p>
     * Constructor.</p>
     *
     * @param hostOCSPResponder url del servidor OCSP responder al que envían
     * las consultas
     */
    public OCSPLiveConsultant(String hostOCSPResponder) {
        servidorOCSP = hostOCSPResponder;

    }

    /**
     * <p>
     * No implementado.</p>
     *
     * @param certList no implementado
     * @return no implementado
     * @throws CertStatusException no implementado
     * @see
     * es.mityc.javasign.certificate.ICertStatusRecoverer#getCertStatus(java.util.List)
     */
    @Override
    public List<ICertStatus> getCertStatus(final List<X509Certificate> certList) throws CertStatusException {

        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public ICertStatus getCertStatus(final X509Certificate cert) throws CertStatusException {
        // Obtenemos la respuesta del servidor OCSP
        // String tiempoRespuesta = ConstantesXADES.CADENA_VACIA;
        OCSPCliente ocspCliente = null;
        OCSPStatus bloque = null;
        RespuestaOCSP respuesta = null;

        try {
            ocspCliente = new OCSPCliente(servidorOCSP);
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            X509Certificate issuerCertificate = CertificateUtils.getCaFakeFromCertificate(cert);

            respuesta = ocspCliente.validateCert(cert, issuerCertificate);
            
            //			tiempoRespuesta = UtilidadFechas.formatFechaXML(respuesta.getTiempoRespuesta());
        } catch (IOException ex) {
            Logger.getLogger(OCSPLiveConsultant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(OCSPLiveConsultant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OCSPClienteException ex) {

            throw new CertStatusException(ex.getMessage(), ex);
        } catch (OCSPProxyException ex) {

            throw new CertStatusException(ex.getMessage(), ex);
        } 
        bloque = new OCSPStatus(respuesta, cert);

        return bloque;
    }

    @Override
    public List<ICertStatus> getCertChainStatus(X509Certificate cert) throws CertStatusException {
        FileInputStream isCertCA = null;
        try {
            // Obtenemos la respuesta del servidor OCSP
            OCSPCliente ocspCliente = null;
            List<ICertStatus> result = new ArrayList<ICertStatus>();
            ocspCliente = new OCSPCliente(this.servidorOCSP);

            // Construimos la cadena de certificacion del certificado
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            X509Certificate issuerCertificateValidate = CertificateUtils.getCaFakeFromCertificate(cert);

            X509Certificate certificateToValidate = cert;

            RespuestaOCSP respuesta = ocspCliente.validateCert(certificateToValidate, issuerCertificateValidate);
            OCSPStatus bloque = new OCSPStatus(respuesta, certificateToValidate);
            
            System.out.println(bloque.getStatus());
            result.add(bloque);
            return result;

        } catch (OCSPClienteException ex) {
            Logger.getLogger(OCSPLiveConsultant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OCSPProxyException ex) {
            Logger.getLogger(OCSPLiveConsultant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OCSPLiveConsultant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(OCSPLiveConsultant.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return null;
    }

    /**
     * <p>
     * Operación no soportada.</p>
     *
     * @param certs
     * @return no utilizado
     * @throws CertStatusException no utilizado
     * @see
     * es.mityc.javasign.certificate.ICertStatusRecoverer#getCertChainStatus(java.security.cert.X509Certificate)
     */
    @Override
    public List<List<ICertStatus>> getCertChainStatus(List<X509Certificate> certs) throws CertStatusException {
        throw new UnsupportedOperationException("Not Supported Operation");
    }
}
