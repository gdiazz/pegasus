/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.model;

import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0
 */
public class InformacionParaFirmar {
    
    private String ubicacion;
    private String razon;
    private Image image; 
    private boolean ultimaPagina;
    private Rectangle rectangle;

    public InformacionParaFirmar( String razon, Image image, boolean ultimaPagina , String ubicacion) {

        this.ubicacion = ubicacion;
        this.razon = razon;
        this.image = image;
        this.ultimaPagina = ultimaPagina;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public boolean isUltimaPagina() {
        return ultimaPagina;
    }

    public void setUltimaPagina(boolean ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }

 
    public InformacionParaFirmar() {
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    
}
