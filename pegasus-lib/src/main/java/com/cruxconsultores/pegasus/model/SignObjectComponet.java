/*
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.model;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.X509Certificate;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0

 */
public class SignObjectComponet {
    
    private String alias;
    private PrivateKey privateKey;
    private Provider provider;
    private KeyStore myKeyStore;
    private X509Certificate cert;

    public KeyStore getMyKeyStore() {
        return myKeyStore;
    }

    public void setMyKeyStore(KeyStore myKeyStore) {
        this.myKeyStore = myKeyStore;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public X509Certificate getCert() {
        return cert;
    }

    public void setCert(X509Certificate cert) {
        this.cert = cert;
    }
    
    
    
}
