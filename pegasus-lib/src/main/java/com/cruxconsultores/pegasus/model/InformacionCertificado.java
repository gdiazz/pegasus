/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.model;

import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.Date;
import org.apache.commons.lang.StringUtils;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public class InformacionCertificado {

    private int version;
    private int validacion;
    private String cnCliente;
    private String ouCliente;
    private String organizacionCliente;
    private String paisCliente;
    private String nombreCliente;
    private String apellidoCliente;
    private String serialNumberCliente;
    private BigInteger identificadorCertificado;
    private Date validosDesde;
    private Date validoHasta;
    private String autoridad;

    public InformacionCertificado(){
        this.version = 0;
        this.validacion = 0;
        this.cnCliente = "";
        this.ouCliente = "";
        this.organizacionCliente = "";
        this.paisCliente = "";
        this.nombreCliente = "";
        this.apellidoCliente = "";
        this.serialNumberCliente = "";
        this.identificadorCertificado = new BigInteger("0");
        this.validosDesde = new Date();
        this.validoHasta = new Date();
        this.autoridad = "";
    }

    public InformacionCertificado(X509Certificate certificadoCliente) {

        int indice =certificadoCliente.getIssuerDN().getName().indexOf("CN=");
        this.setAutoridad(StringUtils.substringBetween(certificadoCliente.getIssuerDN().getName(),"CN=", ","));
        this.setVersion(certificadoCliente.getVersion());
        this.setSerialNumberCliente(certificadoCliente.getSubjectDN().getName());
        this.setApellidoCliente(StringUtils.substringBetween(certificadoCliente.getSubjectDN().getName(), "SURNAME=", ","));
        this.setCnCliente(StringUtils.substringBetween(certificadoCliente.getSubjectDN().getName(), "CN=", "("));
        this.setOrganizacionCliente(StringUtils.substringBetween(certificadoCliente.getSubjectDN().getName(), "O=", ","));
        this.setOuCliente(StringUtils.substringBetween(certificadoCliente.getSubjectDN().getName(), "OU=", ","));
        this.setPaisCliente(StringUtils.substringBetween(certificadoCliente.getSubjectDN().getName(), "C=", ","));
        this.setNombreCliente(StringUtils.substringBetween(certificadoCliente.getSubjectDN().getName(), "GIVENNAME=", ","));
        this.setIdentificadorCertificado(certificadoCliente.getSerialNumber());
        this.setValidosDesde(certificadoCliente.getNotBefore());
        this.setValidoHasta(certificadoCliente.getNotAfter());
    }

    public String getAutoridad() {
        return autoridad;
    }

    public void setAutoridad(String autoridad) {
        this.autoridad = autoridad;
    }

    public int getValidacion() {
        return validacion;
    }

    public void setValidacion(int validacion) {
        this.validacion = validacion;
    }

    
    public BigInteger getIdentificadorCertificado() {
        return identificadorCertificado;
    }

    public Date getValidosDesde() {
        return validosDesde;
    }

    public Date getValidoHasta() {
        return validoHasta;
    }

    public int getVersion() {
        return version;
    }

    public String getCnCliente() {
        return cnCliente;
    }

    public String getOuCliente() {
        return ouCliente;
    }

    public String getOrganizacionCliente() {
        return organizacionCliente;
    }

    public String getPaisCliente() {
        return paisCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public String getSerialNumberCliente() {
        return serialNumberCliente;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setCnCliente(String cnCliente) {
        this.cnCliente = cnCliente;
    }

    public void setOuCliente(String ouCliente) {
        this.ouCliente = ouCliente;
    }

    public void setOrganizacionCliente(String organizacionCliente) {
        this.organizacionCliente = organizacionCliente;
    }

    public void setPaisCliente(String paisCliente) {
        this.paisCliente = paisCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    public void setSerialNumberCliente(String serialNumberCliente) {

        int indice;
        String cedulaReal = "";

        int indiceNacionalidad = serialNumberCliente.indexOf("OU=");
        int indiceNuevo = indiceNacionalidad + 13;
        String nacionalidad = serialNumberCliente.substring(indiceNacionalidad + 3, indiceNuevo);

        if (nacionalidad.equals("EXTRANJERO")) {

            indice = serialNumberCliente.indexOf("SERIALNUMBER=NUP-");

        } else {

            indice = serialNumberCliente.indexOf("SERIALNUMBER=CPF-");
        }
        indice += 17; 
        String cedula = serialNumberCliente.substring(indice);
        
        if (cedula.length()>13){
        
        cedula = cedula.substring(0,cedula.indexOf(","));
        
        }

        for (int i = 0; i < cedula.length(); i++) {

            char digito = cedula.charAt(i);

            String cadena = (new StringBuffer().append(digito)).toString();

            if (!cadena.equals("-")) {

                cedulaReal += cadena;
            }
        }

        this.serialNumberCliente = cedulaReal;
    }

    public void setIdentificadorCertificado(BigInteger identificadorCertificado) {
        this.identificadorCertificado = identificadorCertificado;
    }

    public void setValidosDesde(Date validosDesde) {
        this.validosDesde = validosDesde;
    }

    public void setValidoHasta(Date validoHasta) {
        this.validoHasta = validoHasta;
    }
}