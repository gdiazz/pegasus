/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.model;

import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public class FirmaValida extends InformacionCertificado{

    private String nombreDocumento;
    private Calendar fechaFirma;
    private boolean modificado;
    private boolean estampadoTiempo;
    private String algoritmoFirma;
    private String razon;
    private String localicacion;
    private Integer estadoOCPS;
    private Date fechaEstampado;
    private boolean isValid;
    private X509Certificate cert;

    public FirmaValida(X509Certificate certificadoCliente) {
        super(certificadoCliente);
    }
 
    public FirmaValida(String nombreDocumento, Calendar fechaFirma, boolean modificado, boolean estampadoTiempo, String algoritmoFirma, String razon, String localicacion, int estadoOCPS, Date fechaEstampado, X509Certificate certificadoCliente) {
        super(certificadoCliente);
        this.nombreDocumento = nombreDocumento;
        this.fechaFirma = fechaFirma;
        this.modificado = modificado;
        this.estampadoTiempo = estampadoTiempo;
        this.algoritmoFirma = algoritmoFirma;
        this.razon = razon;
        this.localicacion = localicacion;
        this.estadoOCPS = estadoOCPS;
        this.fechaEstampado = fechaEstampado;
        
    }

    public String getNombreDocumento() {
        
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public Calendar getFechaFirma() {
        return fechaFirma;
    }

    public void setFechaFirma(Calendar fechaFirma) {
        this.fechaFirma = fechaFirma;
    }

    public boolean isModificado() {
        return modificado;
    }

    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    public boolean isEstampadoTiempo() {
        return estampadoTiempo;
    }

    public void setEstampadoTiempo(boolean estampadoTiempo) {
        this.estampadoTiempo = estampadoTiempo;
    }

    public String getAlgoritmoFirma() {
        return algoritmoFirma;
    }

    public void setAlgoritmoFirma(String algoritmoFirma) {
        this.algoritmoFirma = algoritmoFirma;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public String getLocalicacion() {
        return localicacion;
    }

    public void setLocalicacion(String localicacion) {
        this.localicacion = localicacion;
    }

    public Integer getEstadoOCPS() {
        return estadoOCPS;
    }

    public void setEstadoOCPS(Integer estadoOCPS) {
        this.estadoOCPS = estadoOCPS;
    }

    public Date getFechaEstampado() {
        return fechaEstampado;
    }

    public void setFechaEstampado(Date fechaEstampado) {
        this.fechaEstampado = fechaEstampado;
    }

    public boolean isIsValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }

    public X509Certificate getCert() {
        return cert;
    }

    public void setCert(X509Certificate cert) {
        this.cert = cert;
    }

    @Override
    public String toString() {
        return "FirmaValida{" + "nombreDocumento=" + nombreDocumento + ", fechaFirma=" + fechaFirma + ", modificado=" + modificado + ", estampadoTiempo=" + estampadoTiempo + ", algoritmoFirma=" + algoritmoFirma + ", razon=" + razon + ", localicacion=" + localicacion + ", estadoOCPS=" + estadoOCPS + ", fechaEstampado=" + fechaEstampado + ", isValid=" + isValid + ", cert=" + cert + '}';
    }
    
    


}
