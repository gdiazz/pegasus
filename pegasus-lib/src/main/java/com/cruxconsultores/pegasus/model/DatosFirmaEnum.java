/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.model;

/**
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since  10/01/2014, 02:31:14 PM
 * @version 1.0
 */
public enum DatosFirmaEnum {
    NOMBRE_COMUN_CN,
    UNIDAD_ORGANIZACIONAL_OU,
    COUNTRY_NAME_C,
    ORGANIZACION_O,
    NUMERO_SERIE,
    NOMBRE_FIRMAMNTE,
    APELLIDOS_FIRMANTE
}
