/*
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.model;

import java.security.cert.X509Certificate;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0
 */
public class PegasusCertificate {
    private String alias;
    private X509Certificate cert;

    public PegasusCertificate(String alias, X509Certificate cert) {
        this.alias = alias;
        this.cert = cert;
    }

    public PegasusCertificate() {
    }

    
    
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public X509Certificate getCert() {
        return cert;
    }

    public void setCert(X509Certificate cert) {
        this.cert = cert;
    }
    
    
}
