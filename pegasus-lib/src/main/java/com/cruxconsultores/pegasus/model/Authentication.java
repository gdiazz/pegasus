/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.model;

import com.cruxconsultores.pegasus.utils.CertificateUtils;

/**
 * Pojo con los datos que permitirán la autenticación
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 26/02/2014, 02:18:41 PM
 * @version 1.0
 */
public final class Authentication {
    
    private String userDn;
    private String userCn;
    private String userDnWithoutCn;
    
    private String   validityStart;  //Fecha inicio de vigencia del certificado
    private String   validityExpire; //fecha en la que expira el certificado

    public Authentication(String userDn) {
        this.userDn = userDn;
        String [] dn = CertificateUtils.splitDnByCn(this.userDn);
        this.setUserCn(dn[0]);
        this.setUserDnWithoutCn(dn[1]);
        
    }

    public Authentication() {
    }

    public String getUserCn() {
        return userCn;
    }

    public void setUserCn(String userCn) {
        this.userCn = userCn;
    }

    public String getUserDnWithoutCn() {
        return userDnWithoutCn;
    }

    public void setUserDnWithoutCn(String userDnWithoutCn) {
        this.userDnWithoutCn = userDnWithoutCn;
    }
      
    public void setUserDn(String userDn) {
        this.userDn = userDn;
        String [] dn = CertificateUtils.splitDnByCn(this.userDn);
        this.setUserCn(dn[0]);
        this.setUserDnWithoutCn(dn[1]);
    }

    public String getValidityStart() {
        return validityStart;
    }

    public void setValidityStart(String validityStart) {
        this.validityStart = validityStart;
    }

    public String getValidityExpire() {
        return validityExpire;
    }

    public void setValidityExpire(String validityExpire) {
        this.validityExpire = validityExpire;
    }

    @Override
    public String toString() {
        return "Authentication{" + "userDn=" + userDn + ", userCn=" + userCn + ", userDnWithoutCn=" + userDnWithoutCn + ", validityStart=" + validityStart + ", validityExpire=" + validityExpire + '}';
    }
}
