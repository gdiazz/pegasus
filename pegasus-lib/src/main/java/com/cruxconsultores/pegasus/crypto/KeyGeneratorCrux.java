/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.crypto;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.KeyGenerator;


/**
 * Esta clase se encarga de generar llaves para encripción, según
 * el algoritmo de encripción y el certificado
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public class KeyGeneratorCrux {

    /**
     * Guarda el algoritmo con el que se generará la llave simétrica
     */
    private static String ALGORITHM_AES = "AES";
    
    /**
     * Guarda el algoritmo con el que se generará la llave asimétrica
     */
    private static String ALGORITHM_RSA = "RSA";
    /**
     * Constructor por defecto, es necesario setear el certificado
     * si es instanciado el generador con este construcor
     */
    public KeyGeneratorCrux() {
    }

    
    /**
     * Este metodo genera una llave para encripción simétrica.
     * @return Devuelve una cadena de texto con la llave
     * @throws java.security.NoSuchAlgorithmException Error si el algoritmo no
     * es correcto
     */
    public String genKey() throws NoSuchAlgorithmException{
        
        Key secretKey = KeyGenerator.getInstance(ALGORITHM_AES).generateKey();
        try {
            return SecuPegasus.getHexString(secretKey.getEncoded());
        } catch (Exception ex) {
            Logger.getLogger(KeyGeneratorCrux.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Genera un par de llaves de encripción
     * @return Retorna un KeyPair (llave publica y privada)
     * @throws NoSuchAlgorithmException 
     */
    public KeyPair genKeyPair() throws NoSuchAlgorithmException{
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM_RSA);
        keyGen.initialize(1024);
        KeyPair keyPair = keyGen.generateKeyPair();
        return keyPair;
    }
    
    
}
