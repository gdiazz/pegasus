/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cruxconsultores.pegasus.crypto.text;


import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;

import com.cruxconsultores.pegasus.model.SignObjectComponet;
import com.cruxconsultores.pegasus.utils.CertificateUtils;

/**
 * Esta clase se encarga de las firmas de texto
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 11/03/2014, 11:20:20 AM
 * @version 1.0
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */
public class StringSigner {

    /**
     * Aunque la semantica difiere del contexto, este objeto contiene
     * todas los elemenos necesarios para firmar un texto
     */
    private SignObjectComponet componente;
    
    private Provider provider;
    
    /**
     * Agrga el objeto con todo lo necesario para firmar
     * @param comp Objeto con las propiedades necesarias para realizar cualquier firma
     */
    public StringSigner(SignObjectComponet comp){
        this.componente = comp;
    }

    public StringSigner() {
    }

    /**
     * Firma una cadena de texto
     * @param text Cadena de texto a firmar
     * @return Cadena de texto firmada
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws SignatureException
     * @throws InvalidKeyException 
     */
    public String signText(String text) throws NoSuchAlgorithmException, NoSuchProviderException, SignatureException, InvalidKeyException {
        if (text != null) {
            if (!text.isEmpty()) {
                Signature signer = null;
                if (this.componente.getProvider() == null){
                    signer = Signature.getInstance("SHA1withRSA");
                }else{
                    signer = Signature.getInstance("SHA1withRSA", this.componente.getProvider());
                }
               
                signer.initSign(this.componente.getPrivateKey());
                signer.update(text.getBytes());
                return  new String(new Base64().encode(signer.sign()));
            }
        }
        return null;
    }
    /**
     * Verifica una cadena de texto firmada
     * @param original Cadena de texto sin firmar
     * @param text Cadena de texto Firmada
     * @param cert Certificado en base64 que firmó
     * @return Verdadero si es valida, de lo contrario falso
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.InvalidKeyException
     * @throws java.security.SignatureException
     * @throws java.security.cert.CertificateException
     */
    public boolean verifySignedText(String original, String text, String cert) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, CertificateException{
        boolean isValid = false;
        X509Certificate certificate;
        if (text != null) {
            if (!text.isEmpty()) {
                Signature signature = Signature.getInstance("SHA1withRSA");
                certificate = CertificateUtils.getCertificate(cert);
                signature.initVerify(certificate.getPublicKey());
                signature.update(original.getBytes());
                isValid = signature.verify(new Base64().decode(text.getBytes()));
            }

        }
        return isValid;

    }
    
    /**
     * Firma una cadena de texto con una llave privada
     * @param text Texto a firmar
     * @param privateKey LLave privada en base64
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException 
     */
    public String signText(String text, String privateKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException{
        if (text != null) {
            if (!text.isEmpty()) {
                Signature signer = Signature.getInstance("SHA1withRSA");
                signer.initSign(CertificateUtils.parsePrivateKey(privateKey));
                signer.update(text.getBytes());
                return  new String(new Base64().encode(signer.sign()));
            }
        }
        return null;   
    }
    
    
   
    
}
