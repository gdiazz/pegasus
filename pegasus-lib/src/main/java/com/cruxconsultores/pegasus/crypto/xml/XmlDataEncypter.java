/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.crypto.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.cruxconsultores.pegasus.crypto.CryptoString;

/**
 * Esta clase se encargará de encriptar los datos de un xml
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 19/03/2014, 10:50:57 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public class XmlDataEncypter {

    /**
     * El xml en una cadena de texto
     */
    private String xmlTextMode;

    /**
     * Xml después de encriptar
     */
    private Document docXml;

    /**
     * Certificado para encripcion
     */
    private X509Certificate cert;

    /**
     * 
     */
    public XmlDataEncypter() {
    }

    /**
     * Lee un xml a partir de una cadena de texto
     * @param str Xml en formato de texto
     * @return Un Dom
     * @throws DocumentException
     */
    public Document parse(String str) throws DocumentException {
        Document document = DocumentHelper.parseText(str);
        return document;
    }

    /**
     * Parsea un archivo xml
     *
     * @param file Archivo xml
     * @return Un Dom
     * @throws DocumentException
     */
    public Document parse(File file) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(file);
        return document;
    }

    /**
     * Escribe un Documento xml a archivo
     *
     * @param document Documento en memoria
     * @param fileName nombre del archivo
     * @throws IOException
     */
    public void write(Document document, String fileName) throws IOException {
//        OutputFormat format = OutputFormat.createPrettyPrint();
//        format.setEncoding("UTF-8");
        XMLWriter xmlWriter = null;
        try {
            xmlWriter = new XMLWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName), "UTF8")
                    );
            xmlWriter.write(document);
        } finally {
            if (xmlWriter != null) {
                xmlWriter.flush();
                xmlWriter.close();
            }
        }

    }

    /**
     * Encripta un solo nodo
     *
     * @param element Elemento del xml a firmar
     * @param pukey Llave publica que firma
     * @return Una cadena de texto encriptada
     */
    private String encriptNode(Element element, PublicKey pukey) {
        CryptoString cs = new CryptoString();
        String result = "";
        try {
            result = cs.encryptString(element.getText(), pukey);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(XmlDataEncypter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(XmlDataEncypter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(XmlDataEncypter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            Logger.getLogger(XmlDataEncypter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            Logger.getLogger(XmlDataEncypter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(XmlDataEncypter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Encripta un solo nodo
     *
     * @param element Elemento a decifrar
     * @param prikey Llave privada que decifra
     * @return Cadena de texto del elemento decifrada
     */
    private String decriptNode(Element element, PrivateKey prikey) {
        CryptoString cs = new CryptoString();
        String result = "";
        try {
            if (Base64.isArrayByteBase64(element.getText().getBytes())) {
                result = cs.decryptString(element.getText(), prikey);
                System.out.println(element.getText());
                System.out.println(result);
            }

        } catch (Exception e) {
            result = element.getText();
            //System.out.println(e);
        }
        return result;
    }

    /**
     * Recorre todo el dom recursivamente encriptandolo todos sus hijos a raiz
     * del padre
     *
     * @param element Elemento de inicio dónde iniciará la encripción
     * @param pukey Llave publica de encripción
     */
    public void encrypNodes(Element element, PublicKey pukey) {
        for (int i = 0, size = element.nodeCount(); i < size; i++) {
            Node node = element.node(i);
            if (node instanceof Element) {
                String str = node.getText().trim();
                if (str.length() > 0) {
                    String enc = encriptNode((Element) node, pukey);
                    node.setText(enc);
                }
                encrypNodes((Element) node, pukey);
            }
        }
    }

    /**
     * Recorre todo el dom recursivamente decifrando todos sus hijos 
     * desde un elemento decifrado
     *
     * @param element Elemento padre
     * @param pikey Llave privada de decifrado
     */
    public void decrypNodes(Element element, PrivateKey pikey) {
        for (int i = 0, size = element.nodeCount(); i < size; i++) {
            Node node = element.node(i);
            if (node instanceof Element) {
                String str = node.getText().trim();
                if (str.length() > 0) {
                    String enc = decriptNode((Element) node, pikey);
                    node.setText(enc);
                }
                decrypNodes((Element) node, pikey);
            }
        }
    }

}
