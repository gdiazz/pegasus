/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.crypto;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.AccessController;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PrivilegedAction;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;

/**
 * Clase encargada de encripción de cadenas de texto mediante certificados La
 * idea de esta clase es poder obtener cualquier dato en formato string y
 * encriptarlo por un certificado cualquiera. Del cual se obtendrá la llave
 * publica para dicha encripción.
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>, Glara
 * @since Creado en 25/02/2014, 09:18:56 AM
 * @version 1.0 Copyright © 2014 by Crux Consultores All Rights Reserved.
 */
public class SecuPegasus {

    private static final String CODIFICIATION = "UTF-8";

    /**
     * Clave pública de encripción
     */
    private PublicKey clavePublica;

    /**
     * Cadena de texto a encriptar
     */
    private String str;

    /**
     * Certificado de encripción
     */
    private X509Certificate cert;

    /**
     * Constructor, recibe un certificado en formato X509
     *
     * @param cert Certificado con llave pública
     */
    public SecuPegasus(X509Certificate cert) {
        this.cert = cert;
    }

    /**
     * Constructor
     *
     * @param str Cadena de texto a cifrar
     * @param cert Certificado con llave publica de cifrado1
     */
    public SecuPegasus(String str, X509Certificate cert) {
        this.str = str;
        this.cert = cert;
    }

    /**
     * Recibe un certificado en formato base64
     *
     * @param strB64cert Candena de texto con el certificado en base64
     */
    public SecuPegasus(String strB64cert) {
        //byte[] certificate = Base64.decodeBase64(strB64cert.getBytes());
        this.cert = loadCertInfoByte(strB64cert);
    }

    public SecuPegasus() {

    }

    /**
     * Encripta una cadena de texto solo si se ha pasado un certificado en el
     * constructor
     *
     * @param s Texto a encriptar
     * @return Texto encriptado
     */
    public String encriptar(String s) {
        this.str = s;
        return this.encriptar(cert);
    }

    /**
     * Encripta el texto pasado por el constructor, utilizando el certificado
     * pasado por el constructor.
     *
     * @return Cadena de texto encriptado
     */
    public String encriptar() {
        return this.encriptar(str);
    }

    /**
     * Encripta una cadena de texto con un certificado
     *
     * @param rootCert Certificado con el que se encriptará la cadena de texto
     * @return Cadena de texto encriptada
     */
    public String encriptar(X509Certificate rootCert) {
        try {
            AccessController.doPrivileged(new PrivilegedAction() {
                @Override
                public Object run() {
                    // privileged code goes here, for example:
                    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
                    return null; // nothing to return
                }
            });

            //Validar que el texto no exceda la capcidad de la llave
            int keySize = ((RSAPublicKey) rootCert.getPublicKey()).getModulus().bitLength();
            int maxTextSize = (keySize / 8) - 11;
            String result = "";
            if (this.str.length() > maxTextSize) {
                String toEncrypt[] = splitBySize(str, maxTextSize);

                for (String string : toEncrypt) {
                    result += encriptStr(string, rootCert) + "-";
                }
            } else {
                return encriptStr(str, rootCert);
            }
            return result;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SecuPegasus.class.getName()).log(Level.SEVERE, null, ex);

        } catch (NoSuchProviderException ex) {
            Logger.getLogger(SecuPegasus.class.getName()).log(Level.SEVERE, null, ex);

        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(SecuPegasus.class.getName()).log(Level.SEVERE, null, ex);

        } catch (InvalidKeyException ex) {
            Logger.getLogger(SecuPegasus.class.getName()).log(Level.SEVERE, null, ex);

        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(SecuPegasus.class.getName()).log(Level.SEVERE, null, ex);

        } catch (BadPaddingException ex) {
            Logger.getLogger(SecuPegasus.class.getName()).log(Level.SEVERE, null, ex);

        } catch (Exception ex) {
            Logger.getLogger(SecuPegasus.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.toString());

        }
        return null;

    }

    /**
     * Decifra una cadena de texto
     *
     * @param key Llave privada para decifrar el texto
     * @param encryptedData Cadena de texto encriptada
     * @return Una cadena de texto decifrada
     */
    public String decrypt(String encryptedData, PrivateKey key) {

        try {
            AccessController.doPrivileged(new PrivilegedAction() {
                @Override
                public Object run() {
                    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
                    return null; // nothing to return
                }
            });
        if (encryptedData.contains("-")){
            String splitedText [] = encryptedData.split("-");
            String result = "";
            for (String string : splitedText) {
                decryptString(string, key);
            }
        }else{
             return decryptString(encryptedData, key);
        }
        } catch (IOException ex) {

        } catch (InvalidCipherTextException ex) {
        }
        return null;

    }

    /**
     * Convierte de hexadecimal a String
     *
     * @param b Arreglo de bytes a convertir a texto
     * @return String Devuelve una cadena de texto en hexadecimal
     * @throws Exception
     */
    public static String getHexString(byte[] b) throws Exception {
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result
                    += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    /**
     * Se encarga de pasar el String a un array de bytes
     *
     * @param s Cadena de texto en hexadecimal
     * @return Arreglo de bytes
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * agrega un certificado x509
     *
     * @param cert Certificado x509
     */
    public void setCert(X509Certificate cert) {
        this.cert = cert;
    }

    /**
     * Obtene la Llave publica que se usa para la encripción
     *
     * @return Retorna la llave pública
     */
    public PublicKey getClavePublica() {
        return clavePublica;
    }

    /**
     * Prepara la cadena de texto que se desea encriptar
     *
     * @param strToEncript Cadena de texto a encriptar
     */
    public void setStrToEncript(String strToEncript) {
        this.str = strToEncript;
    }

    /**
     * Este metodo convierte un String en base64 a certificado
     *
     * @param base64cert Cadena de texto que contiene el certificado en base64
     * @return Un certificado
     */
    public X509Certificate loadCertInfoByte(String base64cert) {
        byte[] data = hexStringToByteArray(base64cert);
        X509Certificate rootCert = null;
//        String base64StringCer = null;
        try {
            AccessController.doPrivileged(new PrivilegedAction() {
                @Override
                public Object run() {
                    // privileged code goes here, for example:
                    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());  // Cargar el provider BC
                    return null; // nothing to return
                }
            });
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            InputStream isCertCA = new ByteArrayInputStream(data);
            rootCert = (X509Certificate) cf.generateCertificate(isCertCA);
            //System.out.println(rootCert);
            isCertCA.close();

        } catch (IOException e) {

        } catch (CertificateException ex) {
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rootCert;
    }

    public static void main(String args[]) {
//        SecuPegasus p = new SecuPegasus();
//        X509Certificate loadCertInfoByte = p.loadCertInfoByte("308204743082035ca00302010202143230303031303030303030313030303035383637300d06092a864886f70d01010505003082016f3118301606035504030c0f412e432e2064652070727565626173312f302d060355040a0c26536572766963696f2064652041646d696e69737472616369c3b36e205472696275746172696131383036060355040b0c2f41646d696e69737472616369c3b36e20646520536567757269646164206465206c6120496e666f726d616369c3b36e3129302706092a864886f70d010901161a617369736e657440707275656261732e7361742e676f622e6d783126302406035504090c1d41762e20486964616c676f2037372c20436f6c2e20477565727265726f310e300c06035504110c053036333030310b3009060355040613024d583119301706035504080c10446973747269746f204665646572616c3112301006035504070c09436f796f6163c3a16e31153013060355042d130c5341543937303730314e4e333132303006092a864886f70d0109020c23526573706f6e7361626c653a2048c3a963746f72204f726e656c617320417263696761301e170d3132303732373137303230305a170d3136303732373137303230305a3081db3129302706035504031320414343454d20534552564943494f5320454d50524553415249414c45532053433129302706035504291320414343454d20534552564943494f5320454d50524553415249414c455320534331293027060355040a1320414343454d20534552564943494f5320454d50524553415249414c455320534331253023060355042d131c414141303130313031414141202f2048454754373631303033345332311e301c06035504051315202f20484547543736313030334d4446524e4e30393111300f060355040b1308556e69646164203130819f300d06092a864886f70d010101050003818d0030818902818100b64d34123ce341395c695eff702d8a3c8decc1adbdf88bfb4bc7c8c676dcb2dbb05c7ac39abf6735352fc676fd74bbd4a45adc1dcb7e2e44055eec7d1add53112c7024be9daa3021da65ce13f24e3b26d6fdc2b38487f5c54bc2e04f8cb9390628e9c8af7d75dd96fc5653f81d33f7ba236960e851a8acff07e063c027fa82370203010001a31d301b300c0603551d130101ff04023000300b0603551d0f0404030206c0300d06092a864886f70d010105050003820101004f131e713a4c6dd8521e8e8a5548384151783a9d8806188c68eaedad705d260cc6a2d50570981d042323b536574a5ab54b80c6d63afca78373425cf1b1d4f1681f2748a278284320213ec4eb6c51523d798c8e3da85cfb7f6b8cb6d64b353862a6e9ec37f345d57b6f1123130f0405bdab3f5208a0eaac047d265cd0c0f132928b12411e3e6430b590fecd71523156ba8663aee0cf85cc68915db8f86095e7f4cca377cc5b2ff73ef5421cad8a19b24c43bccc5cebfe8cdeb34d0d8dce16104af95fec3cccd6586b7c4b5ca39ad3df5e016cd6deb9019da1713c1302e1eb2b00dab49d593df6b8f3664144e0005f5c1acfb5b05e9d20b28eec0c29b6ff2515c3");
//        String priKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCvmY4qDQfpMuqRsK/1MehQrFwCkt9nc6SpL7PdSjrwEMmCwE01XAaohes/uEOIuOl9t63UuJcKulbFCMYlYbj5Vs5/ZMn7sH0JFJT2wUOOCxlnktYzmvl+3dahDJkQGgKe9E8/5RZMUCdh74/0LKiKUjdEBC2nIyP18uQUDvK9/cGOAnmISfZEPl3dk/wS1rJkJl2L0s84fxf0cxi6DfLpAeWXaoudBmv+s9KVAj2CQTs9cfEtFSrtQRgQagT0NQfblvWyiZrGoPX90aoJEQdd9HffQ01vAauIpfre6VcImzS8X7x8dOVUcqpWg1TadhCikvhTGhY3e8i+FaIq90rjAgMBAAECggEAA0FmiFMLhYBDJwZOjM4rD2vSnN2aNLHJKLXQsNaSdybohXsJM2cMXI13CgxhfCRpd8Iy/9GYQ0OnHioGemTwZ/b7BxOYRwlDmCqAhH6awQRlqSzoubYyz+5O71drcwf4anf/DCE0I6AMyt0FHzLAwSe7DJDDQ+j73rBtGA4a4/jLaI2z99N9dF0oQdMBl1I0dGWoeZ4jzYoflyXfnWd66M115YIHq3XyPZ8HxCvpkCpU/M52Yj3tVjzRzqDV2vRKmI5Ny3kie5wYjsBcqydSs1Tc/jO2NjDaFbhI3eTVNkQAR8bMtGjsiWOM7b/UmiTSIvadC3QypjvRk1b8bG/9QQKBgQD6QZK2jHNJK1G8tNyhxWDq0bJExo9zrVwNIOsv/yL6AwHjFn2ygJqR//aBAlc/0omsg0L/y7t0VzAkZEyrUxypL3O+JWZ75Qe/GRATrJcVgGSYXyUg7VAxjZT91Qj+RdorFuMqB7PJ/lKDwSLmTYG9ufzrRR13ARJlQ6KOyvGSwwKBgQCzoVM1Eklyw6w8PONNK5SWxg0XcsAAzK4TnlY75eFsuUD3+avqSPW2RGyJ/3Tlpnfk383FMhaWvGFt/pLRj8lzxkkRrB/4KpbciR5q2TZXSOZm92+gn2N1pXHJYStVXmqJKqHDtzPzBuJZTIZ+kbYbwC86V0JXe9gLyz1lq12lYQKBgQCzpMCxpC7Z1mvtX5OF2Q9SYUdsEXFGo6qRjhDmOmscN25yWmHO8nyC09BbGZgSK4tCuFMvFkuxHyli5lEXJylK1dJrK6PeyjD+X/pndRxbhfgwXRRlB5XqjGzorbWv+eq7xck6EZ9hH9yR0eHAmuDIlitVpB6bn7xZzuxxPoOdSwKBgETcv61EyWt5n/mnzeuznU7/td0bXIfx73oN5TkpM3Yevs1RpZo3RhsNuB5fFE1FuAjB03yZaxd2OFOWmGpGGRCNH03rEoZsrVD2L/b+NlrX05v9l4mYEOT0+94bpiUgAO98rG7/OKceKH/bWmQDMDELSA+xi/lFvNkonOzw+QGBAoGASAwbNjK2mm6/lwYBV3rk2idHDUBEd8+ibLGDGn5Zcqc14A9LTXVi7ax48p5tqRjWIlw5TeTlZfbLA9C+S/AYHNv7bdorwiShPjNA0R141mohnSMDkW2F7FajAzq1kghsNj5+g+IDlPXTOgpZZ3q/vMhTcZXHGI8JnjEPux3AFL4=";
//        p.setCert(loadCertInfoByte);
//        System.out.println(splitBySize(priKey, 1023)[0].getBytes().length);
//        String toEncrypt = splitBySize(priKey, 118)[0];
//        System.out.println(toEncrypt.length());
//        String encriptar = p.encriptar(toEncrypt);
//        System.out.println("Encripted = " + encriptar);
//        RSAPublicKey key = (RSAPublicKey) loadCertInfoByte.getPublicKey();
//        System.out.println("llave publica " + key.getModulus().bitLength());

        SecuPegasus p = new SecuPegasus();
        X509Certificate loadCertInfoByte = p.loadCertInfoByte("308204743082035ca00302010202143230303031303030303030313030303035383637300d06092a864886f70d01010505003082016f3118301606035504030c0f412e432e2064652070727565626173312f302d060355040a0c26536572766963696f2064652041646d696e69737472616369c3b36e205472696275746172696131383036060355040b0c2f41646d696e69737472616369c3b36e20646520536567757269646164206465206c6120496e666f726d616369c3b36e3129302706092a864886f70d010901161a617369736e657440707275656261732e7361742e676f622e6d783126302406035504090c1d41762e20486964616c676f2037372c20436f6c2e20477565727265726f310e300c06035504110c053036333030310b3009060355040613024d583119301706035504080c10446973747269746f204665646572616c3112301006035504070c09436f796f6163c3a16e31153013060355042d130c5341543937303730314e4e333132303006092a864886f70d0109020c23526573706f6e7361626c653a2048c3a963746f72204f726e656c617320417263696761301e170d3132303732373137303230305a170d3136303732373137303230305a3081db3129302706035504031320414343454d20534552564943494f5320454d50524553415249414c45532053433129302706035504291320414343454d20534552564943494f5320454d50524553415249414c455320534331293027060355040a1320414343454d20534552564943494f5320454d50524553415249414c455320534331253023060355042d131c414141303130313031414141202f2048454754373631303033345332311e301c06035504051315202f20484547543736313030334d4446524e4e30393111300f060355040b1308556e69646164203130819f300d06092a864886f70d010101050003818d0030818902818100b64d34123ce341395c695eff702d8a3c8decc1adbdf88bfb4bc7c8c676dcb2dbb05c7ac39abf6735352fc676fd74bbd4a45adc1dcb7e2e44055eec7d1add53112c7024be9daa3021da65ce13f24e3b26d6fdc2b38487f5c54bc2e04f8cb9390628e9c8af7d75dd96fc5653f81d33f7ba236960e851a8acff07e063c027fa82370203010001a31d301b300c0603551d130101ff04023000300b0603551d0f0404030206c0300d06092a864886f70d010105050003820101004f131e713a4c6dd8521e8e8a5548384151783a9d8806188c68eaedad705d260cc6a2d50570981d042323b536574a5ab54b80c6d63afca78373425cf1b1d4f1681f2748a278284320213ec4eb6c51523d798c8e3da85cfb7f6b8cb6d64b353862a6e9ec37f345d57b6f1123130f0405bdab3f5208a0eaac047d265cd0c0f132928b12411e3e6430b590fecd71523156ba8663aee0cf85cc68915db8f86095e7f4cca377cc5b2ff73ef5421cad8a19b24c43bccc5cebfe8cdeb34d0d8dce16104af95fec3cccd6586b7c4b5ca39ad3df5e016cd6deb9019da1713c1302e1eb2b00dab49d593df6b8f3664144e0005f5c1acfb5b05e9d20b28eec0c29b6ff2515c3");
        String priKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCvmY4qDQfpMuqRsK/1MehQrFwCkt9nc6SpL7PdSjrwEMmCwE01XAaohes/uEOIuOl9t63UuJcKulbFCMYlYbj5Vs5/ZMn7sH0JFJT2wUOOCxlnktYzmvl+3dahDJkQGgKe9E8/5RZMUCdh74/0LKiKUjdEBC2nIyP18uQUDvK9/cGOAnmISfZEPl3dk/wS1rJkJl2L0s84fxf0cxi6DfLpAeWXaoudBmv+s9KVAj2CQTs9cfEtFSrtQRgQagT0NQfblvWyiZrGoPX90aoJEQdd9HffQ01vAauIpfre6VcImzS8X7x8dOVUcqpWg1TadhCikvhTGhY3e8i+FaIq90rjAgMBAAECggEAA0FmiFMLhYBDJwZOjM4rD2vSnN2aNLHJKLXQsNaSdybohXsJM2cMXI13CgxhfCRpd8Iy/9GYQ0OnHioGemTwZ/b7BxOYRwlDmCqAhH6awQRlqSzoubYyz+5O71drcwf4anf/DCE0I6AMyt0FHzLAwSe7DJDDQ+j73rBtGA4a4/jLaI2z99N9dF0oQdMBl1I0dGWoeZ4jzYoflyXfnWd66M115YIHq3XyPZ8HxCvpkCpU/M52Yj3tVjzRzqDV2vRKmI5Ny3kie5wYjsBcqydSs1Tc/jO2NjDaFbhI3eTVNkQAR8bMtGjsiWOM7b/UmiTSIvadC3QypjvRk1b8bG/9QQKBgQD6QZK2jHNJK1G8tNyhxWDq0bJExo9zrVwNIOsv/yL6AwHjFn2ygJqR//aBAlc/0omsg0L/y7t0VzAkZEyrUxypL3O+JWZ75Qe/GRATrJcVgGSYXyUg7VAxjZT91Qj+RdorFuMqB7PJ/lKDwSLmTYG9ufzrRR13ARJlQ6KOyvGSwwKBgQCzoVM1Eklyw6w8PONNK5SWxg0XcsAAzK4TnlY75eFsuUD3+avqSPW2RGyJ/3Tlpnfk383FMhaWvGFt/pLRj8lzxkkRrB/4KpbciR5q2TZXSOZm92+gn2N1pXHJYStVXmqJKqHDtzPzBuJZTIZ+kbYbwC86V0JXe9gLyz1lq12lYQKBgQCzpMCxpC7Z1mvtX5OF2Q9SYUdsEXFGo6qRjhDmOmscN25yWmHO8nyC09BbGZgSK4tCuFMvFkuxHyli5lEXJylK1dJrK6PeyjD+X/pndRxbhfgwXRRlB5XqjGzorbWv+eq7xck6EZ9hH9yR0eHAmuDIlitVpB6bn7xZzuxxPoOdSwKBgETcv61EyWt5n/mnzeuznU7/td0bXIfx73oN5TkpM3Yevs1RpZo3RhsNuB5fFE1FuAjB03yZaxd2OFOWmGpGGRCNH03rEoZsrVD2L/b+NlrX05v9l4mYEOT0+94bpiUgAO98rG7/OKceKH/bWmQDMDELSA+xi/lFvNkonOzw+QGBAoGASAwbNjK2mm6/lwYBV3rk2idHDUBEd8+ibLGDGn5Zcqc14A9LTXVi7ax48p5tqRjWIlw5TeTlZfbLA9C+S/AYHNv7bdorwiShPjNA0R141mohnSMDkW2F7FajAzq1kghsNj5+g+IDlPXTOgpZZ3q/vMhTcZXHGI8JnjEPux3AFL4=";
        p.setCert(loadCertInfoByte);
        System.out.println(p.encriptar(priKey));
    }

    private static String[] splitBySize(final String word, int size) {
        return word.split("(?<=\\G.{" + size + "})");
    }

    private String encriptStr(String string, X509Certificate rootCert) throws UnsupportedEncodingException, IOException, InvalidCipherTextException, Exception {
        byte[] bufferPlano = string.getBytes(CODIFICIATION);
        this.clavePublica = rootCert.getPublicKey();

        AsymmetricKeyParameter publicKey
                = (AsymmetricKeyParameter) PublicKeyFactory.createKey(this.clavePublica.getEncoded());
        AsymmetricBlockCipher e = new RSAEngine();
        e = new org.bouncycastle.crypto.encodings.PKCS1Encoding(e);
        e.init(true, publicKey);

        byte[] messageBytes = bufferPlano;

        byte[] hexEncodedCipher = e.processBlock(messageBytes, 0, messageBytes.length);
        return getHexString(hexEncodedCipher);

    }

    private String decryptString(String encryptedData, PrivateKey key) throws UnsupportedEncodingException, IOException, InvalidCipherTextException {
        byte[] bytes = encryptedData.getBytes(CODIFICIATION);

        AsymmetricKeyParameter privateKey
                = (AsymmetricKeyParameter) PrivateKeyFactory.createKey(key.getEncoded());
        AsymmetricBlockCipher e = new RSAEngine();
        e = new org.bouncycastle.crypto.encodings.PKCS1Encoding(e);
        e.init(false, privateKey);
        byte[] messageBytes = hexStringToByteArray(encryptedData);
        byte[] hexEncodedCipher = e.processBlock(messageBytes, 0, messageBytes.length);
        System.out.println(new String(hexEncodedCipher));

        return new String(hexEncodedCipher);
    }
}
