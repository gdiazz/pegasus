/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.codec.binary.Base64;

/**
 * Se encarga de todo los relacionado con la encripción de cadenas de texto
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public class CryptoString {

    /**
     * Algoritmo con el que se encriptará
     */
    private static final String ALGORITHM = "RSA/ECB/PKCS1Padding";

    /**
     * Encripta una cadena de texto
     * @param str Cadena de texto
     * @param key Llave pública
     * @return Devuelve la cadena de texto encriptada
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException 
     */
    public String encryptString(String str, PublicKey key) throws
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            IllegalBlockSizeException, 
            BadPaddingException, 
            InvalidKeyException, 
            UnsupportedEncodingException {
        
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return new String(
                new Base64().encode(cipher.doFinal(str.getBytes("UTF-8"))));
    }

    /**
     * Desencripta una cadena de texto
     * @param str  Cadena de texto a desencriptar
     * @param key  LLave privada de desencripción
     * @return Cadena de texto desencriptada
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException 
     */
    public String decryptString(String str, PrivateKey key) throws
            UnsupportedEncodingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            IllegalBlockSizeException,
            BadPaddingException {

        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return new String(cipher.doFinal(
                new Base64().decode(str.getBytes("UTF-8"))));
    }
}
