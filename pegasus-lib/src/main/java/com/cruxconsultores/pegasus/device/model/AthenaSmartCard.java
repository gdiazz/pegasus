/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.device.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.login.LoginException;
import javax.smartcardio.CardException;

import sun.security.pkcs11.SunPKCS11;
import sun.security.pkcs11.wrapper.PKCS11;
import sun.security.pkcs11.wrapper.PKCS11Exception;

import com.cruxconsultores.pegasus.device.DeviceManager;

/**
 * Clase que controla el acceso a la lectora de marca Athena
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 */
@SuppressWarnings("restriction")
public class AthenaSmartCard extends DeviceManager {

	/**
	 * 
	 */
	private static final String ATHENA_LINUX_CFG = "/devices/model/athena/linux.cfg";

	/**
	 * 
	 */
	private static final String ATHENA_MAC_CFG = "/devices/model/athena/mac.cfg";

	/**
	 * 
	 */
	private static final String ATHENA_WINDOWS_CFG = "/devices/model/athena/windows.cfg";

	private final InputStream WIN_CONFIG = this.getClass().getResourceAsStream(
			ATHENA_WINDOWS_CFG);

	private final InputStream MAC_CONFIG = this.getClass().getResourceAsStream(
			ATHENA_MAC_CFG);

	private final InputStream LIN_CONFIG = this.getClass().getResourceAsStream(
			ATHENA_LINUX_CFG);

	@SuppressWarnings("restriction")
	@Override
	public void init(String s) throws KeyStoreException, IOException,
			NoSuchAlgorithmException, CertificateException, ProviderException {

		String os = System.getProperty("os.name").toLowerCase();

		if (os.contains(new StringBuffer("win"))) {
			try {
				provider = new sun.security.pkcs11.SunPKCS11(WIN_CONFIG);
			} catch (Exception e) {
				throw new ProviderException(e);
			}

		} else if (os.contains(new StringBuffer("mac"))) {

			try {
				provider = new sun.security.pkcs11.SunPKCS11(MAC_CONFIG);
			} catch (Exception e) {
				throw new ProviderException(e);
			}

		} else if (os.contains(new StringBuffer("lin"))) {

			try {
				provider = new sun.security.pkcs11.SunPKCS11(LIN_CONFIG);

			} catch (Exception e) {
				throw new ProviderException(e);
			}

		}

		Security.addProvider(provider);

		getKeyStorage(s);

	}

	@Override
	public void clear() {
		try {
			((SunPKCS11) provider).logout();
			this.provider.clear();
			this.LIN_CONFIG.close();
			this.MAC_CONFIG.close();
			this.WIN_CONFIG.close();

		} catch (LoginException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	public void closeCardSession() throws CardException,
			IllegalAccessException, IOException, PKCS11Exception {
		String path = getLibraryPath();
		Map m = new HashMap();
		Field field = null;
		try {
			field = PKCS11.class.getDeclaredField("moduleMap");
		} catch (NoSuchFieldException ex) {
			Logger.getLogger(AthenaSmartCard.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (SecurityException ex) {
			Logger.getLogger(AthenaSmartCard.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		field.setAccessible(true);
		m = (Map) field.get(this.provider);
		PKCS11 tmpPkcs11 = (sun.security.pkcs11.wrapper.PKCS11) m.get(path);
		tmpPkcs11 = PKCS11.getInstance(path, "", null, true);
		try {
			tmpPkcs11.finalize();
		} catch (Throwable ex) {
			Logger.getLogger(AthenaSmartCard.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		m.remove(getLibraryPath());

	}

	public String getLibraryPath() {
		String os = System.getProperty("os.name").toLowerCase();
		InputStream stream = null;
		String path = "";
		if (os.contains(new StringBuffer("win"))) {

			stream = this.getClass().getResourceAsStream(ATHENA_WINDOWS_CFG);
		} else if (os.contains(new StringBuffer("mac"))) {

			stream = this.getClass().getResourceAsStream(ATHENA_MAC_CFG);

		} else if (os.contains(new StringBuffer("lin"))) {
			stream = this.getClass().getResourceAsStream(ATHENA_LINUX_CFG);
		}

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stream));
		String line = "";
		try {
			while ((line = reader.readLine()) != null) {
				if (line.contains("library")) {
					path = line.replace("library = ", "");
				}

			}
		} catch (IOException ex) {

		} finally {
			try {
				stream.close();
			} catch (IOException ex) {

			}
		}

		return path;
	}

}
