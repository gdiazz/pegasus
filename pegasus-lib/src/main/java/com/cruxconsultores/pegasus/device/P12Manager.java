/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.device;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.cert.CertificateException;

/**
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public class P12Manager extends DeviceManager{

    
    
    @Override
    public void init(String s) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, ProviderException {
        getKeyStorage(s);
    }


    @Override
    public KeyStore getKeyStorage(String passwd) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        if (isLoged) {
            if (!passwd.equals(sessionPassword)) {
                isLoged = false;
                throw new KeyStoreException();
            }
        } else {

            try {
                keyStore  = KeyStore.getInstance("pkcs12");
                System.out.println(getFileDir());
                keyStore.load(new FileInputStream(getFileDir()), passwd.toCharArray());
                
                isLoged = true;
                sessionPassword = passwd;
                return keyStore;
            } catch (KeyStoreException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new KeyStoreException();
            } catch (IOException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new IOException();
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new NoSuchAlgorithmException();
            } catch (CertificateException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new CertificateException();
            }
        }       
        return keyStore;
    }
    
    
    

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
}
