/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.device;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Interfaz que provee de características básicas a los gestores de llaves
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public interface KeyStoreManager {
    
    
    /**
     * Obtiene el proveedor de seguridad
     * @return provider
     */
    public Provider getProvider();
    
    
    /**
     * Obtiene la llave privada del KeyStorage
     * @param alias
     * @param passwd
     * @return 
     * @throws java.security.KeyStoreException 
     * @throws java.io.IOException 
     * @throws java.security.cert.CertificateException 
     * @throws java.security.UnrecoverableKeyException 
     * @throws java.security.NoSuchAlgorithmException 
     */

    public PrivateKey getPrivateKey(String alias) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException;
    
    /**
     * Obtiene el almacén de claves y certificados
     * @param passwd
     * @return 
     * @throws java.security.KeyStoreException 
     * @throws java.io.IOException 
     * @throws java.security.cert.CertificateException 
     * @throws java.security.NoSuchAlgorithmException 
     */
    public KeyStore getKeyStorage(String passwd) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException;
    
    
    
    public List<X509Certificate> getCertificates() throws KeyStoreException;
}
