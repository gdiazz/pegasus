/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.device;

/**
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public enum EnumKeyUsage {
        DIGITALSIGNATURE,
        NONREPUDIATION  ,
        KEYENCIPHERMENT ,
        DATAENCIPHERMENT,
        KEYAGREEMENT    ,
        KEYCERTSIGN     ,
        CRLSIGN         ,
        ENCIPHERONLY    ,
        DECIPHERONLY    
}
