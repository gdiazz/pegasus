/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.device;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.ProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.cruxconsultores.pegasus.model.PegasusCertificate;
import com.cruxconsultores.pegasus.model.SignObjectComponet;


/**
 * Clase abstracta con funciones comunes para todas los tipos de lectoras o 
 * dispositivos del cual se obtendrá el certificado sin
 * importar el sistema operativo. Es importante implementar su metodo abstracto
 * init, donde se obtendrá el proveedor ya que dependiendo del dispositivo variará
 * el archivo de configuración.
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 */
@SuppressWarnings("restriction")
public abstract class DeviceManager implements KeyStoreManager {

    /**
     * Almacen de claves 
     */
    protected KeyStore keyStore;

    /**
     * Llave privada obtenida del almacen 
     */
    protected PrivateKey privateKey;

    /**
     * Proveedor que contiene la configuración del dispositivo
     */
    protected Provider provider;

    /**
     * Variable que contiene la contraseña solo si se logrado accesar al
     * almacen de claves
     */
    protected String sessionPassword;

    /**
     * Indica si se ha logrado cargar el almacen de claves
     */
    protected boolean isLoged;

    /**
     * Lista de los certificados del almacén
     */
    protected List<X509Certificate> listCerts;
    
    /**
     * Lista de certificado con alias @see PegasusCertificate
     */
    protected List<PegasusCertificate> listPegCerts;
    
    /**
     * Lista de terminales conectadas (Esto es para cuando se usa un smartcard
     */
    protected List<CardTerminal> terminals;
    
    /**
     * Componente con las propiedaeds necesarias para firmar
     * @see SignObjectComponet
     */
    protected SignObjectComponet componente;
    
    /**
     * Tarjeta
     */
    protected Card card;
    
    /**
     * 
     */
    protected String fileDir;
    
    private static final int SIGN_CERT = 0;
    private static final int AUTH_CERT = 1;

    public DeviceManager() {
        componente = new SignObjectComponet();
    }

    /**
     * Depende de cada modelo de tarjeta y los drivers la ejecucion de este
     * metodo
     * @param s Contraseña del dispositivo
     * @throws java.security.KeyStoreException
     * @throws java.io.IOException
     * @throws java.security.cert.CertificateException
     * @throws java.security.NoSuchAlgorithmException
     */
    public abstract void init(String s) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, ProviderException;

    /**
     * Obtiene el proveedor si está inicializado
     * @return 
     */
    @Override
    public Provider getProvider() {
        if (provider != null) {
            return provider;
        }

        return null;
    }

    /**
     * Obtiene una llave privada por alias
     * @param alias Identificador del alias
     * @return Devuelve la llave privada correspondiente al alias
     * @throws KeyStoreException
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws UnrecoverableKeyException 
     */
    @Override
    public PrivateKey getPrivateKey(String alias) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        if (isLoged) {
            System.out.println("Obteniendo llave");
            privateKey = (PrivateKey) this.keyStore.getKey(alias, sessionPassword.toCharArray());
            System.out.println("llave obtenida");
        } else {
             System.out.println("No se pudo obtener el la llave privada");
            throw new KeyStoreException();
        }
        return privateKey;
    }

    /**
     * Obtiene el keystore
     * @param passwd Contraseña para abrir el almacen de claves del dispositivo
     * @returnz Devuelve el almacen de llaves inicializado
     * @throws KeyStoreException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException 
     */
    @Override
    public KeyStore getKeyStorage(String passwd) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        if (isLoged) {
            if (!passwd.equals(sessionPassword)) {
                isLoged = false;
                throw new KeyStoreException();

            }
        } else {

            try {
                this.keyStore = KeyStore.getInstance("PKCS11", provider);
                this.keyStore.load(null, passwd.toCharArray());
                isLoged = true;
                sessionPassword = passwd;
                return keyStore;
            } catch (KeyStoreException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new KeyStoreException();
            } catch (IOException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new IOException();
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new NoSuchAlgorithmException();
            } catch (CertificateException ex) {
                ex.printStackTrace();
                isLoged = false;
                throw new CertificateException();
            }
        }       
        
        return keyStore;

    }

    /**
     * Obtiene la listade certificados del dispositivo
     * @return Lista de certificados
     * @throws KeyStoreException 
     */
    @Override
    public List<X509Certificate> getCertificates() throws KeyStoreException {
        if (isLoged) {
            listCerts = new ArrayList<X509Certificate>();
            if (this.keyStore != null) {

                Enumeration<String> alias = this.keyStore.aliases();
                while (alias.hasMoreElements()) {
                    String string = alias.nextElement();
                    System.out.println(string);
                    listCerts.add((X509Certificate) keyStore.getCertificate(string));
                }

            } else {
                throw new KeyStoreException();
            }
        }
            Collections.sort(listCerts, new Comparator<X509Certificate>(){
                @Override
                public int compare(X509Certificate s1, X509Certificate s2) {
                    String sa = StringUtils.substringBetween(s1.getSubjectDN().getName(), "(", ")");
                    String sb = StringUtils.substringBetween(s2.getSubjectDN().getName(), "(", ")");
                    return sa.compareToIgnoreCase(sb);
                }
            });
        return listCerts;
    }
    

    /**
     * Obtiene un objeto con el certificado y el alias del dispostivo
     * @return Lista de certificados y alias
     * @throws KeyStoreException 
     */
    public List<PegasusCertificate> getPegCertificates() throws KeyStoreException {
        if (isLoged) {
            listPegCerts = new ArrayList<PegasusCertificate>();
            if (this.keyStore != null) {

                Enumeration<String> alias = this.keyStore.aliases();
                while (alias.hasMoreElements()) {
                    String string = alias.nextElement();
                    listPegCerts.add(new PegasusCertificate(string, (X509Certificate) keyStore.getCertificate(string)));
                }

            } else {
                throw new KeyStoreException();
            }
        }
        
        Collections.sort(listPegCerts, new Comparator<PegasusCertificate>(){
            @Override
            public int compare(PegasusCertificate s1, PegasusCertificate s2) {
                String sa = StringUtils.substringBetween(s1.getCert().getSubjectDN().getName(), "(", ")");
                String sb = StringUtils.substringBetween(s2.getCert().getSubjectDN().getName(), "(", ")");
                return sa.compareToIgnoreCase(sb);
            }
        });
        return listPegCerts;
    }

    /**
     * Identifica si se encuentra conectado el lector de tarjetas
     * @return La tarjeta 
     */
    public  List<CardTerminal> getListCards() throws CardException {

        Card card = null;

        CardTerminal terminal = null;
            TerminalFactory factory = TerminalFactory.getDefault();
            terminals = factory.terminals().list();

        for (CardTerminal terminal1 : terminals) {
            terminal = terminal1;
            if (terminal.isCardPresent()) {
                this.card = card;
                card = terminal.connect("*");
            }
        }
        
        return terminals;
    }
    
    /**
     * Remueve la tarjeta
     * @throws CardException 
     */
    public void removeCard() throws CardException {

        Card card = null;

        CardTerminal terminal = null;
        TerminalFactory factory = TerminalFactory.getDefault();
        terminals = factory.terminals().list();

        for (CardTerminal terminal1 : terminals) {
            terminal = terminal1;
            
            if (terminal.isCardPresent()) {
                card = terminal.connect("*");
                card.disconnect(true);
                this.card = card;
            }
        }
        
    
    }
    /**
     * Verifica si el uso de la llave publica del certificado coincide con el que se consulta
     * @param cert Certificado
     * @param usage Uso de llave publica
     * @return True si el uso coincide de lo contrario False.
     */
    private boolean getkeyUsageByUsage(X509Certificate cert, EnumKeyUsage usage){
        boolean arr[] = cert.getKeyUsage();
        
        if (usage == EnumKeyUsage.DIGITALSIGNATURE)
           return arr[0];
        
        else if (usage == EnumKeyUsage.NONREPUDIATION)
           return arr[1];

        else if (usage == EnumKeyUsage.KEYENCIPHERMENT)
           return arr[2];

        else if (usage == EnumKeyUsage.DATAENCIPHERMENT)
           return arr[3];

        else if (usage == EnumKeyUsage.KEYAGREEMENT)
           return arr[4];
        
        else if (usage == EnumKeyUsage.KEYCERTSIGN)
           return arr[5];

        else if (usage == EnumKeyUsage.CRLSIGN)
           return arr[6];
        
        else if (usage == EnumKeyUsage.ENCIPHERONLY)
           return arr[7];

        else if (usage == EnumKeyUsage.DECIPHERONLY)
           return arr[8];
        
        
        return false;
    }
    
    /**
     * Algunos sistema externos puede que requieran de usar de todos los compon
     * entes para firmar, esto es independiente de su implementación. Pero es 
     * muy probable que lo que vayan a necesitar sea un objeto que contenga
     * El gestor de llaves, la llave privada, el proveedor y tal vez el alias
     * 
     * Al mencionar sistemas externos podemos referirnos como a una librería ex-
     * terna que requiera por parametros estos objetos.
     * @param alias Alias del certificado
     * @return Devuelve un objeto con todo lo necesario para  firmar
     * @throws KeyStoreException
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws UnrecoverableKeyException 
     */
    public SignObjectComponet getSignObjectComponet(String alias) 
            throws KeyStoreException, IOException, CertificateException, 
            NoSuchAlgorithmException, UnrecoverableKeyException
    {
        componente = new SignObjectComponet();
        componente.setProvider(provider);
        componente.setAlias(alias);
        componente.setMyKeyStore(keyStore);
        componente.setPrivateKey(getPrivateKey(alias));
        
        return componente;      
    }
    
    
    /**
     * Algunos sistema externos puede que requieran de usar de todos los compon
     * entes para firmar, esto es independiente de su implementación. Pero es 
     * muy probable que lo que vayan a necesitar sea un objeto que contenga
     * El gestor de llaves, la llave privada, el proveedor y tal vez el alias
     * 
     * Al mencionar sistemas externos podemos referirnos como a una librería ex-
     * terna que requiera por parametros estos objetos.
     * @param usage Un objeto listo para su uso
     * @return Devuelve un objeto con todo lo necesario para  firmar
     * @throws KeyStoreException
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws UnrecoverableKeyException 
     */
    public SignObjectComponet getSignObjectComponet(EnumKeyUsage usage) 
            throws KeyStoreException, IOException, CertificateException, 
            NoSuchAlgorithmException, UnrecoverableKeyException
    {   
        SignObjectComponet obj = new SignObjectComponet();
        List<PegasusCertificate> pcerts =  getPegCertificates();
        
        for (PegasusCertificate pegasusCertificate : pcerts) {
            boolean b = getkeyUsageByUsage(pegasusCertificate.getCert(), usage);
            if (b){
                obj.setAlias(pegasusCertificate.getAlias());
                obj.setMyKeyStore(keyStore);
                obj.setPrivateKey(getPrivateKey(pegasusCertificate.getAlias()));
                obj.setProvider(provider);
                obj.setCert(pegasusCertificate.getCert());
            }
        }
        return obj;
    }
    
    
    /**
     * Obtiene el o los certificados por su uso.
     * Esto es importante ya que a veces es necesario saber si un certificado
     * se puede usar para firmar, encriptar, autenticar o validar, entre otros. 
     * Algunos certificados pueden hacer o no hacer algunos de esas, por esa razón
     * nos aseguramos que el certificado que queremos usar sirva segun la bandera.
     * 
     * Por ejemplo: El certificado que se requiere para encripcion y desencripcion
     * 
     * @param usage
     * @return 
     */
    public List<X509Certificate> getCertificatesByUsage(EnumKeyUsage usage){
        List<X509Certificate>  listCert = null;
        List<X509Certificate>  certs = null;
        try {
            listCert = getCertificates();
            certs = new ArrayList<X509Certificate>();
        } catch (KeyStoreException ex) {
            Logger.getLogger(DeviceManager.class.getName()).log(Level.ERROR, null, ex);
        }
        for (X509Certificate x509Certificate : listCert) {
            if (getkeyUsageByUsage(x509Certificate, usage)){
               certs.add(x509Certificate);
            }
        }
        return certs;
    }
    
    /**
     * Obtiene el o los certificados por su uso.
     * Esto es importante ya que a veces es necesario saber si un certificado
     * se puede usar para firmar, encriptar, autenticar o validar, entre otros. 
     * Algunos certificados pueden hacer o no hacer algunos de esas, por esa razón
     * nos aseguramos que el certificado que queremos usar sirva segun la bandera.
     * 
     * Por ejemplo: El certificado que se requiere para encripcion y desencripcion
     * 
     * @param usage Enumerador que especifica el tipo de uso
     * @return 
     */
    public List<PegasusCertificate> getPegCertificatesByUsage(EnumKeyUsage usage){
        List<PegasusCertificate>  listCert = null;
        List<PegasusCertificate>  certs = null;
        try {
            listCert = getPegCertificates();
            certs = new ArrayList<PegasusCertificate>();
        } catch (KeyStoreException ex) {
            Logger.getLogger(DeviceManager.class.getName()).log(Level.ERROR, null, ex);
        }
        for (PegasusCertificate x509Certificate : listCert) {
            if (getkeyUsageByUsage(x509Certificate.getCert(), usage)){
               certs.add(x509Certificate);
            }
        }
        return certs;
    }
    /**
     * Para implementar una forma de liberar el almacen de llaves
     */
    public abstract void clear();

    public String getFileDir() {
        return fileDir;
    }

    public void setFileDir(String fileDir) {
        this.fileDir = fileDir;
    }
    
    
    
    
}
