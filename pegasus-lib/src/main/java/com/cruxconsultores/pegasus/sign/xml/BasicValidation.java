/*
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.sign.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.bouncycastle.ocsp.BasicOCSPResp;
import org.bouncycastle.ocsp.OCSPException;
import org.bouncycastle.tsp.TimeStampToken;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.cruxconsultores.pegasus.model.DatosFirmaEnum;
import com.cruxconsultores.pegasus.model.FirmaValida;
import com.cruxconsultores.pegasus.ocsp.OCSPVerifier;
import com.cruxconsultores.pegasus.utils.Util;
import com.cruxconsultores.pegasus.utils.Utileria;
import com.cruxconsultores.pegasus.validation.TimeStampExtractor;

import es.mityc.firmaJava.libreria.xades.ResultadoValidacion;
import es.mityc.firmaJava.libreria.xades.ValidarFirmaXML;
import es.mityc.firmaJava.libreria.xades.errores.FirmaXMLError;
import es.mityc.javasign.ts.TimeStampValidator;


/**
 * Esta clase se encarga de la lectura y validacion de firmas en un XML
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0
 */
public class BasicValidation {

    private List<FirmaValida> listaFirmasValidas;
    private TimeStampExtractor extractorTS;
    private boolean isValido;

    public static String DIR_CA = System.getProperty("java.io.tmpdir") + "/pegasus/ca";
    public static String DIR_SIGN = System.getProperty("java.io.tmpdir") + "/pegasus/signtaure/";

    private List<FirmaValida> listaFirmas = null;

    /**
     * Constructor
     */
    public BasicValidation() {
        this.listaFirmasValidas = null;

    }

    public List<FirmaValida> getFirmasXades(final String xmlString) {

        AccessController.doPrivileged(new PrivilegedAction() {
            @Override
            public Void run() {

                List<ResultadoValidacion> results = null;
                org.w3c.dom.Document doc = null;
                String serverCrlDir = DIR_CA + "/";

                try {
                    doc = convertStringToDocument(xmlString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (doc == null) {
                    System.out.println("Error de validacion. No se pudo parsear la firma.");
                    isValido = false;
                }
                ValidarFirmaXML validor = new ValidarFirmaXML();
                try {
                    results = validor.validar(doc, "", null, new TimeStampValidator(), serverCrlDir);

                } catch (FirmaXMLError ex) {
                    return null;
                }
                ResultadoValidacion resultadoValidacion = null;
                Iterator<ResultadoValidacion> iterador = results.iterator();

                listaFirmas = new ArrayList<FirmaValida>();
                while (iterador.hasNext()) {

                    resultadoValidacion = iterador.next();
                    X509Certificate certificado = (X509Certificate) resultadoValidacion.getDatosFirma().getCadenaFirma().getCertificates().get(0);

                    FirmaValida firma = new FirmaValida(certificado);
                    firma.setCert(certificado);
                    if (resultadoValidacion.isValidate()) {
                        firma.setIsValid(true);
                        
                    }

                    Calendar cal = Calendar.getInstance();
                    Date d = resultadoValidacion.getDatosFirma().getFechaFirma();
                    //System.out.println(firma.getFechaEstampado());
                    if (d != null) {
                        cal.setTime(d);
                        firma.setFechaFirma(cal);
                    } else {
                        firma.setFechaFirma(null);
                    }
                    listaFirmas.add(firma);
                }

                return null;

            }

        });
        return listaFirmas;

    }

    /**
     * Este método obtiene las firmas validadas de un archivo xml Cada firma
     * contiene la información resultado de la validación.
     *
     * @param file Recibe un Archivo
     * @return Una lista List<> con las firmas del documento
     */
    public List<FirmaValida> getFirmasBasicas(String ruta) {
        File file = new File(ruta);
        FileInputStream fileInput = null;
        List<FirmaValida> listaFirmas = new ArrayList<FirmaValida>();
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BasicValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<ResultadoValidacion> results = null;
        Document doc = null;
        try {
            doc = parseaDoc(fileInput);
        } catch (IOException ex) {
            Logger.getLogger(BasicValidation.class.getName() + "No se econtró el " + fileInput.toString()).log(Level.SEVERE, null, ex);
        }

        if (doc == null) {
            System.out.println("Error de validación. No se pudo parsear la firma.");
            return null;
        }

        ValidarFirmaXML validor = new ValidarFirmaXML();
        try {
            results = validor.validar(doc, "", null, new TimeStampValidator(), "");

        } catch (Exception ex) {
            Logger.getLogger(BasicValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultadoValidacion resultadoValidacion = null;
        Iterator<ResultadoValidacion> iterador = results.iterator();
        listaFirmasValidas = new ArrayList<FirmaValida>();
        System.out.println("Cantidad de firmas en el documento: " + results.size());
        extractorTS = new TimeStampExtractor(ruta);
        List<TimeStampToken> listaTiempo = extractorTS.getTsList();
        int i = 0;
        while (iterador.hasNext()) {
            resultadoValidacion = iterador.next();
            X509Certificate certificado = (X509Certificate) resultadoValidacion.getDatosFirma().getCadenaFirma().getCertificates().get(0);
            FirmaValida firma = new FirmaValida(certificado);

            //Obtiene todos los atributos del certificado
            String subject = certificado.getSubjectX500Principal().toString();

            firma.setAlgoritmoFirma(certificado.getSigAlgName());
            firma.setApellidoCliente(Utileria.getSubjectX500By(subject, DatosFirmaEnum.APELLIDOS_FIRMANTE));
            firma.setCnCliente(Utileria.getSubjectX500By(subject, DatosFirmaEnum.NOMBRE_COMUN_CN));

            //Valida el estado de OCPS
            OCSPVerifier consumir = new OCSPVerifier();
            try {
                //setea el estado OCPS
                BasicOCSPResp resp = consumir.validate(Util.loadCert("/trust/CA SINPE - PERSONA FISICA.cer"), certificado);
                //firma.setEstadoOCPS(consumir.compruebaRespuestaOCSP(resp));

            } catch (OCSPException ex) {
                Logger.getLogger(BasicValidation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (CertificateException ex) {
                Logger.getLogger(BasicValidation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BasicValidation.class.getName()).log(Level.SEVERE, null, ex);
            }

            //Fecha de la firma
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(resultadoValidacion.getDatosFirma().getFechaFirma());
            firma.setFechaFirma(calendar);

            firma.setIdentificadorCertificado(certificado.getSerialNumber());
            firma.setLocalicacion(Utileria.getSubjectX500By(subject, DatosFirmaEnum.COUNTRY_NAME_C));
            String nombre = "";
            nombre = Utileria.getSubjectX500By(subject, DatosFirmaEnum.NOMBRE_FIRMAMNTE);
            if (nombre.isEmpty()) {
                nombre = Utileria.getSubjectX500By(subject, DatosFirmaEnum.NOMBRE_COMUN_CN);
            }
            firma.setNombreCliente(nombre);

            firma.setNombreDocumento(file.getName());

//            if (resultadoValidacion.getDatosFirma().getDatosSelloTiempo().size() > 0) {
//                firma.setEstampadoTiempo(true);
//                firma.setFechaEstampado(resultadoValidacion.getDatosFirma().getDatosSelloTiempo().get(0).getFecha());
//            } else {
//                firma.setEstampadoTiempo(false);
//            }
            //Agrega los sellos de tiempo de la firma
            if (listaTiempo != null) {
                firma.setFechaEstampado(listaTiempo.get(i).getTimeStampInfo().getGenTime());
                i++;
            }

            firma.setRazon(Utileria.getSubjectX500By(certificado.getIssuerX500Principal().toString(), DatosFirmaEnum.ORGANIZACION_O));
            firma.setOrganizacionCliente(Utileria.getSubjectX500By(certificado.getIssuerX500Principal().toString(), DatosFirmaEnum.ORGANIZACION_O));
            firma.setValidosDesde(certificado.getNotBefore());
            firma.setValidoHasta(certificado.getNotAfter());

            //Imprime la firma obtenida
            // System.out.println(firma.toString());
            listaFirmas.add(firma);
        }

        if (resultadoValidacion.isValidate()) {
            // El método getNivelValido devuelve el último nivel XAdES válido
            System.out.println("La firma es valida.\n" + resultadoValidacion.getNivelValido()
                    + "\nCertificado: " + ((X509Certificate) resultadoValidacion.getDatosFirma().getCadenaFirma().getCertificates().get(0)).getSubjectDN()
                    + "\nFirmado el: " + resultadoValidacion.getDatosFirma().getFechaFirma()
                    + "\nNodos firmados: " + resultadoValidacion.getFirmados());
        } else {
            // El método getLog devuelve el mensaje de error que invalidó la firma
            System.out.println("La firma NO es valida\n" + resultadoValidacion.getLog());
        }

        return listaFirmas;
    }

    /**
     * <p>
     * Parsea un documento XML y lo introduce en un DOM.
     * </p>
     *
     * @param uriFirma URI al fichero XML
     * @return Docuemnto parseado
     */
    public Document parseaDoc(InputStream fichero) throws IOException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            System.out.println("Error interno al parsear la firma");
            ex.printStackTrace();
            return null;
        }

        Document doc = null;
        try {
            doc = db.parse(fichero);
            return doc;
        } catch (SAXException ex) {
            doc = null;
        } catch (IOException ex) {
            System.out.println("Error interno al validar firma");
            ex.printStackTrace();
        } finally {
            dbf = null;
            db = null;
        }
        return null;
    }

    public static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static org.w3c.dom.Document convertStringToDocument(String xmlStr) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.parse(new ByteArrayInputStream(xmlStr.getBytes("UTF-8")));
    }

    public static void main(String args[]) throws FileNotFoundException, IOException {
        BasicValidation bv = new BasicValidation();
        System.setProperty("java.io.tmpdir", System.getProperty("user.home") + "/.temp_pegasus");

        List<FirmaValida> firmasXades = bv.getFirmasXades("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
"<documento id=\"documentoDigital\">\n" +
"   <solicitud id=\"principal\">\n" +
"      <codigo>1267</codigo>\n" +
"      <tipo>Orden de servicio</tipo>\n" +
"      <fechaRegistro>2014-10-10</fechaRegistro>\n" +
"      <autorizado>01-1440-0187</autorizado>\n" +
"      <observaciones>Esto es un descuento de facturas</observaciones>\n" +
"      <entidades>\n" +
"         <entidad tipo=\"contratista\">Guillermo Bernardo Diaz Solis</entidad>\n" +
"         <entidad tipo=\"descontadora\">Luis Mariano Herrera Sanchez</entidad>\n" +
"         <entidad tipo=\"pagadora\">Joshua Sanchez Mora</entidad>\n" +
"      </entidades>\n" +
"      <contratos>\n" +
"         <contrato>\n" +
"            <codigo>844</codigo>\n" +
"            <numero>0</numero>\n" +
"            <titulo>Es una orden de compra</titulo>\n" +
"            <fechaEmision>2014-09-25</fechaEmision>\n" +
"            <plazoDias>20</plazoDias>\n" +
"            <moneda>CRC</moneda>\n" +
"            <monto>490571.9</monto>\n" +
"            <adjudicatario>01-1440-0187</adjudicatario>\n" +
"            <entidadEmisora>06-0369-0544</entidadEmisora>\n" +
"            <numeroMerlink>0</numeroMerlink>\n" +
"            <consorcio>0</consorcio>\n" +
"            <observaciones>null</observaciones>\n" +
"         </contrato>\n" +
"      </contratos>\n" +
"      <facturas>\n" +
"         <factura>\n" +
"            <codigo>209</codigo>\n" +
"            <numero>035676</numero>\n" +
"            <descripcion/>\n" +
"            <moneda>CRC</moneda>\n" +
"            <montoSolicitado>367657.0</montoSolicitado>\n" +
"            <costosVarios>78678.0</costosVarios>\n" +
"            <descuento>345.0</descuento>\n" +
"            <iva>53523.0</iva>\n" +
"            <otrosImpuestos>5.2345768E7</otrosImpuestos>\n" +
"            <deducciones>45245.0</deducciones>\n" +
"            <montoRetencion>535.0</montoRetencion>\n" +
"            <montoNeto>367657.0</montoNeto>\n" +
"         </factura>\n" +
"         <factura>\n" +
"            <codigo>210</codigo>\n" +
"            <numero>645</numero>\n" +
"            <descripcion/>\n" +
"            <moneda>CRC</moneda>\n" +
"            <montoSolicitado>367657.0</montoSolicitado>\n" +
"            <costosVarios>78678.0</costosVarios>\n" +
"            <descuento>345.0</descuento>\n" +
"            <iva>53523.0</iva>\n" +
"            <otrosImpuestos>5.2345768E7</otrosImpuestos>\n" +
"            <deducciones>45245.0</deducciones>\n" +
"            <montoRetencion>535.0</montoRetencion>\n" +
"            <montoNeto>367657.0</montoNeto>\n" +
"         </factura>\n" +
"         <factura>\n" +
"            <codigo>211</codigo>\n" +
"            <numero>435</numero>\n" +
"            <descripcion/>\n" +
"            <moneda>USD</moneda>\n" +
"            <montoSolicitado>367657.0</montoSolicitado>\n" +
"            <costosVarios>78678.0</costosVarios>\n" +
"            <descuento>345.0</descuento>\n" +
"            <iva>53523.0</iva>\n" +
"            <otrosImpuestos>5.2345768E7</otrosImpuestos>\n" +
"            <deducciones>45245.0</deducciones>\n" +
"            <montoRetencion>535.0</montoRetencion>\n" +
"            <montoNeto>367657.0</montoNeto>\n" +
"         </factura>\n" +
"      </facturas>\n" +
"   </solicitud>\n" +
"<ds:Signature Id=\"Signature521234\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:etsi=\"http://uri.etsi.org/01903/v1.3.2#\">\n" +
"<ds:SignedInfo Id=\"Signature-SignedInfo427147\">\n" +
"<ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/>\n" +
"<ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/>\n" +
"<ds:Reference Id=\"SignedPropertiesID995299\" Type=\"http://uri.etsi.org/01903#SignedProperties\" URI=\"#Signature521234-SignedProperties676704\">\n" +
"<ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/>\n" +
"<ds:DigestValue>yeOnYs4vMKCUrvVQiky0hpZnH9g=</ds:DigestValue>\n" +
"</ds:Reference>\n" +
"<ds:Reference URI=\"#Certificate1929467\">\n" +
"<ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/>\n" +
"<ds:DigestValue>Q1kndCiCU4wS0HvqWiI4LQAx32A=</ds:DigestValue>\n" +
"</ds:Reference>\n" +
"<ds:Reference Id=\"Reference-ID-956050\" URI=\"#principal\">\n" +
"<ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/>\n" +
"<ds:DigestValue>cWmHbP1ZAV0+LJZARcvG0Ch3aTc=</ds:DigestValue>\n" +
"</ds:Reference>\n" +
"</ds:SignedInfo>\n" +
"<ds:SignatureValue Id=\"SignatureValue660081\">\n" +
"KhHLenDZCIAay8gnYXdyhi1ZQ0w4b2fThF+lVfCxZFoiZWoCzO+d82Lw0Uh/OG+T0Z1cNEtehwri\n" +
"V+ezrbhDriAFvycxUkPUNzpklIMUb2zN4/Ua4Y9QM6OAh6gXPrO29Cm2J4XuKEIo20tmmrv9dv8y\n" +
"iU4GPBObsRNi+lmHTdhZ0q3dioQBzAxZuUKiyUGYthfTcz36ku+O89pAKAFrOn6elSfAScGjP55y\n" +
"J8k6aB8LjQb5tlGbfgGqTI3g/kXpkkzB8kgAZv0A6tsvOUL9NJ9/yrrL82fxame06qDleQucJ2TQ\n" +
"zJsPwUQKRuKJ+l62lqTbS7ZW55gLXVpQrFEyvA==\n" +
"</ds:SignatureValue>\n" +
"<ds:KeyInfo Id=\"Certificate1929467\">\n" +
"<ds:X509Data>\n" +
"<ds:X509Certificate>\n" +
"MIIFhDCCBGygAwIBAgIKLCY9PAAAAAIFFjANBgkqhkiG9w0BAQUFADCBmjEVMBMGA1UEBRMMNC0w\n" +
"MDAtMDA0MDE3MQswCQYDVQQGEwJDUjEkMCIGA1UEChMbQkFOQ08gQ0VOVFJBTCBERSBDT1NUQSBS\n" +
"SUNBMSowKAYDVQQLEyFESVZJU0lPTiBERSBTRVJWSUNJT1MgRklOQU5DSUVST1MxIjAgBgNVBAMT\n" +
"GUNBIFNJTlBFIC0gUEVSU09OQSBGSVNJQ0EwHhcNMTQwMjEwMjAyMzMxWhcNMTYwMjEwMjAyMzMx\n" +
"WjCBtzEZMBcGA1UEBRMQQ1BGLTAxLTE0NDAtMDE4NzETMBEGA1UEBBMKRElBWiBTT0xJUzEbMBkG\n" +
"A1UEKhMSR1VJTExFUk1PIEJFUk5BUkRPMQswCQYDVQQGEwJDUjEXMBUGA1UEChMOUEVSU09OQSBG\n" +
"SVNJQ0ExEjAQBgNVBAsTCUNJVURBREFOTzEuMCwGA1UEAxMlR1VJTExFUk1PIEJFUk5BUkRPIERJ\n" +
"QVogU09MSVMgKEZJUk1BKTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL+U0xioaXF6\n" +
"p+6MH3hrCFg5tHocvph8AzTWPH4nYrbFOOfyJBg5VHnMSq7L1aUmWCtEbwBoufMuz77l32wAmyjm\n" +
"6Ne6+7xbBXW+/R2W9BnEXBlNW4oxHaOriIiM2M5CESAWZVVes65qYBCmp9Cciknjnd9DmDekOZ1H\n" +
"g/Wc/z0wz9liNmdS8xJatOT0SUVzWtHk5VVHqelM61auF7ZQMuuqidL0KTEywPHG2zccV30dTIOY\n" +
"VEaNwf/8gTPMOHLE24iEUQ2uVSviny/fPZVjOSrngYrzO8TULRiy5U+/7z6y7SvXHi7IZs8hTdbN\n" +
"ZdPcar6/HMXCXFyTUe7NX+19vY0CAwEAAaOCAaswggGnMB8GA1UdIwQYMBaAFNG4kICx3z99OCJO\n" +
"wXUG46ju8o2mMFkGA1UdHwRSMFAwTqBMoEqGSGh0dHA6Ly9mZGkuc2lucGUuZmkuY3IvcmVwb3Np\n" +
"dG9yaW8vQ0ElMjBTSU5QRSUyMC0lMjBQRVJTT05BJTIwRklTSUNBLmNybDCBkAYIKwYBBQUHAQEE\n" +
"gYMwgYAwKAYIKwYBBQUHMAGGHGh0dHA6Ly9vY3NwLnNpbnBlLmZpLmNyL29jc3AwVAYIKwYBBQUH\n" +
"MAKGSGh0dHA6Ly9mZGkuc2lucGUuZmkuY3IvcmVwb3NpdG9yaW8vQ0ElMjBTSU5QRSUyMC0lMjBQ\n" +
"RVJTT05BJTIwRklTSUNBLmNydDAOBgNVHQ8BAf8EBAMCBsAwPQYJKwYBBAGCNxUHBDAwLgYmKwYB\n" +
"BAGCNxUIhcTqW4LR4zWVkRuC+ZcYhqXLa4F/gb3yRYe7oj4CAWQCAQQwEwYDVR0lBAwwCgYIKwYB\n" +
"BQUHAwQwFQYDVR0gBA4wDDAKBghggTwBAQEBAjAbBgkrBgEEAYI3FQoEDjAMMAoGCCsGAQUFBwME\n" +
"MA0GCSqGSIb3DQEBBQUAA4IBAQCXuxsXCTe6bZe6c2qcksT24hazzUQGqlJJVZykGM/4URxroBWt\n" +
"9NZ4hkHK3d4G0IZKKlLgLt0GCYoxSTNHos5Xv9ohsPnGyAZoW5S2+1eBpqKf7bzzBghCGP28GLoV\n" +
"18ygwh78/1X0i2bAwvsme7DK2Y3kpa7pvY4W+9xFmVmi1lRV6oas6b3ea2ZXLwHL4eEt2vm0H/Wf\n" +
"IuoNy7ShvsGLD6potPYRCNa2lZP1bhdBmXB+0ESVvQ9SGJx8MnKCf9zQcqHyunfbCi4ebiALvBT6\n" +
"sBxUjmbJr1WJZIGK7IbhvMEo6pNh//VsoyAE12I+d/u5MxOIEZ63b6KhNXSdluTC\n" +
"</ds:X509Certificate>\n" +
"</ds:X509Data>\n" +
"<ds:KeyValue>\n" +
"<ds:RSAKeyValue>\n" +
"<ds:Modulus>\n" +
"v5TTGKhpcXqn7owfeGsIWDm0ehy+mHwDNNY8fiditsU45/IkGDlUecxKrsvVpSZYK0RvAGi58y7P\n" +
"vuXfbACbKObo17r7vFsFdb79HZb0GcRcGU1bijEdo6uIiIzYzkIRIBZlVV6zrmpgEKan0JyKSeOd\n" +
"30OYN6Q5nUeD9Zz/PTDP2WI2Z1LzElq05PRJRXNa0eTlVUep6UzrVq4XtlAy66qJ0vQpMTLA8cbb\n" +
"NxxXfR1Mg5hURo3B//yBM8w4csTbiIRRDa5VK+KfL989lWM5KueBivM7xNQtGLLlT7/vPrLtK9ce\n" +
"LshmzyFN1s1l09xqvr8cxcJcXJNR7s1f7X29jQ==\n" +
"</ds:Modulus>\n" +
"<ds:Exponent>AQAB</ds:Exponent>\n" +
"</ds:RSAKeyValue>\n" +
"</ds:KeyValue>\n" +
"</ds:KeyInfo>\n" +
"<ds:Object Id=\"Signature521234-Object709706\"><etsi:QualifyingProperties Target=\"#Signature521234\"><etsi:SignedProperties Id=\"Signature521234-SignedProperties676704\"><etsi:SignedSignatureProperties><etsi:SigningTime>2014-10-14T14:00:31-06:00</etsi:SigningTime><etsi:SigningCertificate><etsi:Cert><etsi:CertDigest><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>am0avpRU9+9ar327l44Ctgcsczc=</ds:DigestValue></etsi:CertDigest><etsi:IssuerSerial><ds:X509IssuerName>CN=CA SINPE - PERSONA FISICA,OU=DIVISION DE SERVICIOS FINANCIEROS,O=BANCO CENTRAL DE COSTA RICA,C=CR,2.5.4.5=#130c342d3030302d303034303137</ds:X509IssuerName><ds:X509SerialNumber>208489513922800268739862</ds:X509SerialNumber></etsi:IssuerSerial></etsi:Cert></etsi:SigningCertificate></etsi:SignedSignatureProperties><etsi:SignedDataObjectProperties><etsi:DataObjectFormat ObjectReference=\"#Reference-ID-956050\"><etsi:Description>Ejemplo</etsi:Description><etsi:MimeType>text/xml</etsi:MimeType></etsi:DataObjectFormat></etsi:SignedDataObjectProperties></etsi:SignedProperties><etsi:UnsignedProperties Id=\"Signature521234-UnsignedProperties911087\"><etsi:UnsignedSignatureProperties><etsi:SignatureTimeStamp Id=\"SelloTiempo950265\"><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><etsi:EncapsulatedTimeStamp Id=\"SelloTiempo-Token319734\">MIIJlgYJKoZIhvcNAQcCoIIJhzCCCYMCAQMxCzAJBgUrDgMCGgUAMGIGCyqGSIb3DQEJEAEEoFMEUTBPAgEBBghggTwBAQEBBTAhMAkGBSsOAwIaBQAEFKKaVFg6yGn2af7knm4d9SMDrtwLAgMvh2gYDzIwMTQxMDE0MjAwMDM4WjAEgAIB9AEB/6CCBsYwggbCMIIFqqADAgECAgphFFa3AAAAAAADMA0GCSqGSIb3DQEBBQUAMHwxGTAXBgNVBAUTEENQSi0yLTEwMC0wOTgzMTExCzAJBgNVBAYTAkNSMQ4wDAYDVQQKEwVNSUNJVDENMAsGA1UECxMERENGRDEzMDEGA1UEAxMqQ0EgUE9MSVRJQ0EgU0VMTEFETyBERSBUSUVNUE8gLSBDT1NUQSBSSUNBMB4XDTEwMTIwMzIxMDQ1NVoXDTE3MTIwMzIxMTQ1NVowcTEZMBcGA1UEBRMQQ1BKLTQtMDAwLTAwNDAxNzELMAkGA1UEBhMCQ1IxJDAiBgNVBAoTG0JBTkNPIENFTlRSQUwgREUgQ09TVEEgUklDQTENMAsGA1UECxMEMDAwMTESMBAGA1UEAxMJVFNBIFNJTlBFMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jUDG31oev3/XZtUyoXTsIYMgUaXIebTuRzFyJnGS/QqSJH/TEMw90rKlVgWWiMI08fZwo1YiSuZL0qt2Ox6xHqMC4x9sPQHubAxtjVIA8XBF1p8+rkbGSek32GVpeAt69iAchGjl/2d7JPJ6oEIoKWQmqSQDFXZudxbaNSmI8aY2R4LGg6GOQmnuca5xSgQCLITYcAf1TqXgjPFaI6dWJv8kOZkA5Dj/wN807RBb5StiT4JgXFurS7Wbz/JXF6yhVytOHI1G2MPHKDPLb38lp4tA0relnXGiBWVBJ1G8K6R6f10FxTs5oSyNfAkVYND9f2qQUG0EVdA1/WGxucg6QIDAQABo4IDTzCCA0swgawGA1UdIASBpDCBoTCBngYIYIE8AQEBAQUwgZEwagYIKwYBBQUHAgIwXh5cAEkAbQBwAGwAZQBtAGUAbgB0AGEAIABsAGEAIABBAHUAdABvAHIAaQBkAGEAZAAgAGQAZQAgAEUAcwB0AGEAbQBwAGEAZABvACAAZABlACAAVABpAGUAbQBwAG8wIwYIKwYBBQUHAgEWF2h0dHA6Ly90c2Euc2lucGUuZmkuY3IvMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMA4GA1UdDwEB/wQEAwIGwDBEBgkqhkiG9w0BCQ8ENzA1MA4GCCqGSIb3DQMCAgIAgDAOBggqhkiG9w0DBAICAIAwBwYFKw4DAgcwCgYIKoZIhvcNAwcwHQYDVR0OBBYEFDEncrYrCpqQkkim72Sh9CyFwErTMB8GA1UdIwQYMBaAFHzICwpWRMU4kwKVYeGxlDRp9BJxMIHqBgNVHR8EgeIwgd8wgdyggdmggdaGZmh0dHA6Ly93d3cuZmlybWFkaWdpdGFsLmdvLmNyL3JlcG9zaXRvcmlvL0NBJTIwUE9MSVRJQ0ElMjBTRUxMQURPJTIwREUlMjBUSUVNUE8lMjAtJTIwQ09TVEElMjBSSUNBLmNybIZsaHR0cDovL3d3dy5taWNpdC5nby5jci9maXJtYWRpZ2l0YWwvcmVwb3NpdG9yaW8vQ0ElMjBQT0xJVElDQSUyMFNFTExBRE8lMjBERSUyMFRJRU1QTyUyMC0lMjBDT1NUQSUyMFJJQ0EuY3JsMIH+BggrBgEFBQcBAQSB8TCB7jByBggrBgEFBQcwAoZmaHR0cDovL3d3dy5maXJtYWRpZ2l0YWwuZ28uY3IvcmVwb3NpdG9yaW8vQ0ElMjBQT0xJVElDQSUyMFNFTExBRE8lMjBERSUyMFRJRU1QTyUyMC0lMjBDT1NUQSUyMFJJQ0EuY3J0MHgGCCsGAQUFBzAChmxodHRwOi8vd3d3Lm1pY2l0LmdvLmNyL2Zpcm1hZGlnaXRhbC9yZXBvc2l0b3Jpby9DQSUyMFBPTElUSUNBJTIwU0VMTEFETyUyMERFJTIwVElFTVBPJTIwLSUyMENPU1RBJTIwUklDQS5jcnQwDQYJKoZIhvcNAQEFBQADggEBAL1VQmEkM4+qNsdxy8Pxs7+IpKalr7lv1XyfL2R5syoFRun3iPwUh9n0p8Hf95hCOvX2Q5oXmo8gFwFEeyJlBmdrfRJySXI3wVZtqVXzl1N8eoGu5ACUl5DzqR9D0qXa2ZQ3mC44WFRlsQeBksW6dYre63e6Q9GR5lUVEdKHcQ6iriOObcwugTRA6krGTS5vaojHef2sGW7tBgETU8dMKAyxLFsPUw6KoW4C2Oxirp6smr6qHilccZgZ8mkDCmqCIaWF1u8fDBMyjwZMkxkAC/jazI4Fjg1tFUDQwEzfN8VVCsq3eoJY+YkBsVLlOjEtpilAItHuluIEVM1IXI66/IIxggJBMIICPQIBATCBijB8MRkwFwYDVQQFExBDUEotMi0xMDAtMDk4MzExMQswCQYDVQQGEwJDUjEOMAwGA1UEChMFTUlDSVQxDTALBgNVBAsTBERDRkQxMzAxBgNVBAMTKkNBIFBPTElUSUNBIFNFTExBRE8gREUgVElFTVBPIC0gQ09TVEEgUklDQQIKYRRWtwAAAAAAAzAJBgUrDgMCGgUAoIGMMBoGCSqGSIb3DQEJAzENBgsqhkiG9w0BCRABBDAcBgkqhkiG9w0BCQUxDxcNMTQxMDE0MjAwMDM4WjAjBgkqhkiG9w0BCQQxFgQUgdzCJYkVUbPSR0gZlDKLXxbhzZMwKwYLKoZIhvcNAQkQAgwxHDAaMBgwFgQUUpI2iOELIULzcMUtuK4/KyTk6kMwDQYJKoZIhvcNAQEBBQAEggEAjM6wEHVChXcFnPkpEnDlZSh392Lj/MNHLbuLiRUsoIeJ7htsaiteNEqt8vyxAVXcWWc5oH01ovVwHoreJ24Pcy5XVJuuw9iAAzLthRxSDUIpRFwwsDHE49exC+eldcnVJTQFS5UqOzMBkgVt1FLH/tvi5bHH7dE8kmOcT6wAO+K2zd3mVgVxjpLuxHPZctlAyRVUhf4L07PCgKvb3niHltslLlD2jusz5HQ8lvJbTcSsGj6uu4klbQnmmySHhQUvH7xitvpBOpZIJWOTg57EmBU5wFrw01b4GOJccZAH7mIJ5SPIWVl6U746QqB+uJ2oELpCPZ6rPBGzga7JH3SGfw==</etsi:EncapsulatedTimeStamp></etsi:SignatureTimeStamp><etsi:CompleteCertificateRefs Id=\"CompleteCertificateRefs267738\"><etsi:CertRefs><etsi:Cert URI=\"#CertPath508849\"><etsi:CertDigest><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>UpI2iOELIULzcMUtuK4/KyTk6kM=</ds:DigestValue></etsi:CertDigest><etsi:IssuerSerial><ds:X509IssuerName>CN=CA POLITICA SELLADO DE TIEMPO - COSTA RICA,OU=DCFD,O=MICIT,C=CR,2.5.4.5=#131043504a2d322d3130302d303938333131</ds:X509IssuerName><ds:X509SerialNumber>458444732182837776613379</ds:X509SerialNumber></etsi:IssuerSerial></etsi:Cert></etsi:CertRefs></etsi:CompleteCertificateRefs><etsi:CompleteRevocationRefs Id=\"CompleteRevocationRefs347274\"><etsi:OCSPRefs><etsi:OCSPRef><etsi:OCSPIdentifier URI=\"#OCSP449999\"><etsi:ResponderID><etsi:ByKey>3u1z3uNmadACPzHDumcU1+cC73Y=</etsi:ByKey></etsi:ResponderID><etsi:ProducedAt>2014-10-14T14:00:40-06:00</etsi:ProducedAt></etsi:OCSPIdentifier><etsi:DigestAlgAndValue><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>ZRROpScPwPJNtvDqV8Z4xKEaqtE=</ds:DigestValue></etsi:DigestAlgAndValue></etsi:OCSPRef><etsi:OCSPRef><etsi:OCSPIdentifier URI=\"#OCSP278785\"><etsi:ResponderID><etsi:ByKey>3u1z3uNmadACPzHDumcU1+cC73Y=</etsi:ByKey></etsi:ResponderID><etsi:ProducedAt>2014-10-14T14:00:41-06:00</etsi:ProducedAt></etsi:OCSPIdentifier><etsi:DigestAlgAndValue><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>6nyWK9kwqTMqekJhgCPW4ar4KjA=</ds:DigestValue></etsi:DigestAlgAndValue></etsi:OCSPRef></etsi:OCSPRefs></etsi:CompleteRevocationRefs><etsi:SigAndRefsTimeStamp Id=\"SelloTiempo312063\"><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><etsi:EncapsulatedTimeStamp>MIIJlgYJKoZIhvcNAQcCoIIJhzCCCYMCAQMxCzAJBgUrDgMCGgUAMGIGCyqGSIb3DQEJEAEEoFMEUTBPAgEBBghggTwBAQEBBTAhMAkGBSsOAwIaBQAEFNGiT21PpPofdNuxqYroA2JCr6ssAgMvh2kYDzIwMTQxMDE0MjAwMDQyWjAEgAIB9AEB/6CCBsYwggbCMIIFqqADAgECAgphFFa3AAAAAAADMA0GCSqGSIb3DQEBBQUAMHwxGTAXBgNVBAUTEENQSi0yLTEwMC0wOTgzMTExCzAJBgNVBAYTAkNSMQ4wDAYDVQQKEwVNSUNJVDENMAsGA1UECxMERENGRDEzMDEGA1UEAxMqQ0EgUE9MSVRJQ0EgU0VMTEFETyBERSBUSUVNUE8gLSBDT1NUQSBSSUNBMB4XDTEwMTIwMzIxMDQ1NVoXDTE3MTIwMzIxMTQ1NVowcTEZMBcGA1UEBRMQQ1BKLTQtMDAwLTAwNDAxNzELMAkGA1UEBhMCQ1IxJDAiBgNVBAoTG0JBTkNPIENFTlRSQUwgREUgQ09TVEEgUklDQTENMAsGA1UECxMEMDAwMTESMBAGA1UEAxMJVFNBIFNJTlBFMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jUDG31oev3/XZtUyoXTsIYMgUaXIebTuRzFyJnGS/QqSJH/TEMw90rKlVgWWiMI08fZwo1YiSuZL0qt2Ox6xHqMC4x9sPQHubAxtjVIA8XBF1p8+rkbGSek32GVpeAt69iAchGjl/2d7JPJ6oEIoKWQmqSQDFXZudxbaNSmI8aY2R4LGg6GOQmnuca5xSgQCLITYcAf1TqXgjPFaI6dWJv8kOZkA5Dj/wN807RBb5StiT4JgXFurS7Wbz/JXF6yhVytOHI1G2MPHKDPLb38lp4tA0relnXGiBWVBJ1G8K6R6f10FxTs5oSyNfAkVYND9f2qQUG0EVdA1/WGxucg6QIDAQABo4IDTzCCA0swgawGA1UdIASBpDCBoTCBngYIYIE8AQEBAQUwgZEwagYIKwYBBQUHAgIwXh5cAEkAbQBwAGwAZQBtAGUAbgB0AGEAIABsAGEAIABBAHUAdABvAHIAaQBkAGEAZAAgAGQAZQAgAEUAcwB0AGEAbQBwAGEAZABvACAAZABlACAAVABpAGUAbQBwAG8wIwYIKwYBBQUHAgEWF2h0dHA6Ly90c2Euc2lucGUuZmkuY3IvMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMA4GA1UdDwEB/wQEAwIGwDBEBgkqhkiG9w0BCQ8ENzA1MA4GCCqGSIb3DQMCAgIAgDAOBggqhkiG9w0DBAICAIAwBwYFKw4DAgcwCgYIKoZIhvcNAwcwHQYDVR0OBBYEFDEncrYrCpqQkkim72Sh9CyFwErTMB8GA1UdIwQYMBaAFHzICwpWRMU4kwKVYeGxlDRp9BJxMIHqBgNVHR8EgeIwgd8wgdyggdmggdaGZmh0dHA6Ly93d3cuZmlybWFkaWdpdGFsLmdvLmNyL3JlcG9zaXRvcmlvL0NBJTIwUE9MSVRJQ0ElMjBTRUxMQURPJTIwREUlMjBUSUVNUE8lMjAtJTIwQ09TVEElMjBSSUNBLmNybIZsaHR0cDovL3d3dy5taWNpdC5nby5jci9maXJtYWRpZ2l0YWwvcmVwb3NpdG9yaW8vQ0ElMjBQT0xJVElDQSUyMFNFTExBRE8lMjBERSUyMFRJRU1QTyUyMC0lMjBDT1NUQSUyMFJJQ0EuY3JsMIH+BggrBgEFBQcBAQSB8TCB7jByBggrBgEFBQcwAoZmaHR0cDovL3d3dy5maXJtYWRpZ2l0YWwuZ28uY3IvcmVwb3NpdG9yaW8vQ0ElMjBQT0xJVElDQSUyMFNFTExBRE8lMjBERSUyMFRJRU1QTyUyMC0lMjBDT1NUQSUyMFJJQ0EuY3J0MHgGCCsGAQUFBzAChmxodHRwOi8vd3d3Lm1pY2l0LmdvLmNyL2Zpcm1hZGlnaXRhbC9yZXBvc2l0b3Jpby9DQSUyMFBPTElUSUNBJTIwU0VMTEFETyUyMERFJTIwVElFTVBPJTIwLSUyMENPU1RBJTIwUklDQS5jcnQwDQYJKoZIhvcNAQEFBQADggEBAL1VQmEkM4+qNsdxy8Pxs7+IpKalr7lv1XyfL2R5syoFRun3iPwUh9n0p8Hf95hCOvX2Q5oXmo8gFwFEeyJlBmdrfRJySXI3wVZtqVXzl1N8eoGu5ACUl5DzqR9D0qXa2ZQ3mC44WFRlsQeBksW6dYre63e6Q9GR5lUVEdKHcQ6iriOObcwugTRA6krGTS5vaojHef2sGW7tBgETU8dMKAyxLFsPUw6KoW4C2Oxirp6smr6qHilccZgZ8mkDCmqCIaWF1u8fDBMyjwZMkxkAC/jazI4Fjg1tFUDQwEzfN8VVCsq3eoJY+YkBsVLlOjEtpilAItHuluIEVM1IXI66/IIxggJBMIICPQIBATCBijB8MRkwFwYDVQQFExBDUEotMi0xMDAtMDk4MzExMQswCQYDVQQGEwJDUjEOMAwGA1UEChMFTUlDSVQxDTALBgNVBAsTBERDRkQxMzAxBgNVBAMTKkNBIFBPTElUSUNBIFNFTExBRE8gREUgVElFTVBPIC0gQ09TVEEgUklDQQIKYRRWtwAAAAAAAzAJBgUrDgMCGgUAoIGMMBoGCSqGSIb3DQEJAzENBgsqhkiG9w0BCRABBDAcBgkqhkiG9w0BCQUxDxcNMTQxMDE0MjAwMDQyWjAjBgkqhkiG9w0BCQQxFgQUbVU21r0efzOghx1hUq3uggfkS9EwKwYLKoZIhvcNAQkQAgwxHDAaMBgwFgQUUpI2iOELIULzcMUtuK4/KyTk6kMwDQYJKoZIhvcNAQEBBQAEggEAynC4Svie/ewbS3lbE9fyjoPl57FriIFBUNAbqZQbs6fhKXUr6P9f1i5PO6DWN3yFBvxGzlp6Ou3X/vhoj06AbuIsIN7Fu5c+2VUgXHhADW4e6RjFsY95LHBin5fIjXSufnWd4sfHTaCT9IsfuPRh008fNVbNApAjREluZjkWBIh7JF5zDdAL6rBKovJzm87hulHqXaNldSgyo9U35HMW42AQBtH7cbTRM1zqEcqH4ckBAAgmOxkuUUeVSNKoTXpxZkdBC5I6KA0LOls0q0Gr+PGfS5c2+7Bj0ZUm2ce1/I7W76o368o/oF8hP2Caj5ZajyOe9mz4HLvi7XLHQ6d1VA==</etsi:EncapsulatedTimeStamp></etsi:SigAndRefsTimeStamp><etsi:CertificateValues Id=\"CertificateValues997603\"><etsi:EncapsulatedX509Certificate Id=\"CertPath508849\">MIIGwjCCBaqgAwIBAgIKYRRWtwAAAAAAAzANBgkqhkiG9w0BAQUFADB8MRkwFwYDVQQFExBDUEotMi0xMDAtMDk4MzExMQswCQYDVQQGEwJDUjEOMAwGA1UEChMFTUlDSVQxDTALBgNVBAsTBERDRkQxMzAxBgNVBAMTKkNBIFBPTElUSUNBIFNFTExBRE8gREUgVElFTVBPIC0gQ09TVEEgUklDQTAeFw0xMDEyMDMyMTA0NTVaFw0xNzEyMDMyMTE0NTVaMHExGTAXBgNVBAUTEENQSi00LTAwMC0wMDQwMTcxCzAJBgNVBAYTAkNSMSQwIgYDVQQKExtCQU5DTyBDRU5UUkFMIERFIENPU1RBIFJJQ0ExDTALBgNVBAsTBDAwMDExEjAQBgNVBAMTCVRTQSBTSU5QRTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOI1Axt9aHr9/12bVMqF07CGDIFGlyHm07kcxciZxkv0KkiR/0xDMPdKypVYFlojCNPH2cKNWIkrmS9KrdjsesR6jAuMfbD0B7mwMbY1SAPFwRdafPq5GxknpN9hlaXgLevYgHIRo5f9neyTyeqBCKClkJqkkAxV2bncW2jUpiPGmNkeCxoOhjkJp7nGucUoEAiyE2HAH9U6l4IzxWiOnVib/JDmZAOQ4/8DfNO0QW+UrYk+CYFxbq0u1m8/yVxesoVcrThyNRtjDxygzy29/JaeLQNK3pZ1xogVlQSdRvCuken9dBcU7OaEsjXwJFWDQ/X9qkFBtBFXQNf1hsbnIOkCAwEAAaOCA08wggNLMIGsBgNVHSAEgaQwgaEwgZ4GCGCBPAEBAQEFMIGRMGoGCCsGAQUFBwICMF4eXABJAG0AcABsAGUAbQBlAG4AdABhACAAbABhACAAQQB1AHQAbwByAGkAZABhAGQAIABkAGUAIABFAHMAdABhAG0AcABhAGQAbwAgAGQAZQAgAFQAaQBlAG0AcABvMCMGCCsGAQUFBwIBFhdodHRwOi8vdHNhLnNpbnBlLmZpLmNyLzAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAOBgNVHQ8BAf8EBAMCBsAwRAYJKoZIhvcNAQkPBDcwNTAOBggqhkiG9w0DAgICAIAwDgYIKoZIhvcNAwQCAgCAMAcGBSsOAwIHMAoGCCqGSIb3DQMHMB0GA1UdDgQWBBQxJ3K2KwqakJJIpu9kofQshcBK0zAfBgNVHSMEGDAWgBR8yAsKVkTFOJMClWHhsZQ0afQScTCB6gYDVR0fBIHiMIHfMIHcoIHZoIHWhmZodHRwOi8vd3d3LmZpcm1hZGlnaXRhbC5nby5jci9yZXBvc2l0b3Jpby9DQSUyMFBPTElUSUNBJTIwU0VMTEFETyUyMERFJTIwVElFTVBPJTIwLSUyMENPU1RBJTIwUklDQS5jcmyGbGh0dHA6Ly93d3cubWljaXQuZ28uY3IvZmlybWFkaWdpdGFsL3JlcG9zaXRvcmlvL0NBJTIwUE9MSVRJQ0ElMjBTRUxMQURPJTIwREUlMjBUSUVNUE8lMjAtJTIwQ09TVEElMjBSSUNBLmNybDCB/gYIKwYBBQUHAQEEgfEwge4wcgYIKwYBBQUHMAKGZmh0dHA6Ly93d3cuZmlybWFkaWdpdGFsLmdvLmNyL3JlcG9zaXRvcmlvL0NBJTIwUE9MSVRJQ0ElMjBTRUxMQURPJTIwREUlMjBUSUVNUE8lMjAtJTIwQ09TVEElMjBSSUNBLmNydDB4BggrBgEFBQcwAoZsaHR0cDovL3d3dy5taWNpdC5nby5jci9maXJtYWRpZ2l0YWwvcmVwb3NpdG9yaW8vQ0ElMjBQT0xJVElDQSUyMFNFTExBRE8lMjBERSUyMFRJRU1QTyUyMC0lMjBDT1NUQSUyMFJJQ0EuY3J0MA0GCSqGSIb3DQEBBQUAA4IBAQC9VUJhJDOPqjbHccvD8bO/iKSmpa+5b9V8ny9kebMqBUbp94j8FIfZ9KfB3/eYQjr19kOaF5qPIBcBRHsiZQZna30ScklyN8FWbalV85dTfHqBruQAlJeQ86kfQ9Kl2tmUN5guOFhUZbEHgZLFunWK3ut3ukPRkeZVFRHSh3EOoq4jjm3MLoE0QOpKxk0ub2qIx3n9rBlu7QYBE1PHTCgMsSxbD1MOiqFuAtjsYq6erJq+qh4pXHGYGfJpAwpqgiGlhdbvHwwTMo8GTJMZAAv42syOBY4NbRVA0MBM3zfFVQrKt3qCWPmJAbFS5ToxLaYpQCLR7pbiBFTNSFyOuvyC</etsi:EncapsulatedX509Certificate></etsi:CertificateValues><etsi:RevocationValues Id=\"RevocationValues802320\"><etsi:OCSPValues><etsi:EncapsulatedOCSPValue Id=\"OCSP449999\">MIIGKAoBAKCCBiEwggYdBgkrBgEFBQcwAQEEggYOMIIGCjCBvKIWBBTe7XPe42Zp0AI/McO6ZxTX5wLvdhgPMjAxNDEwMTQyMDAwNDBaMIGQMIGNMEMwCQYFKw4DAhoFAAQUCeE53CzRjIb88eXVAXO/PcecUc4EFNG4kICx3z99OCJOwXUG46ju8o2mAgosJj08AAAAAgUWgAAYDzIwMTQxMDE0MTkxNjQwWqARGA8yMDE0MTAxNjA3MzY0MFqhIDAeMBwGCSsGAQQBgjcVBAQPFw0xNDEwMTUxOTI2NDBaMA0GCSqGSIb3DQEBBQUAA4IBAQAWVVJSWVy3b8JtLUstx7hr0W0abHQb9zJEqXP7mHr2At9Ymhi2zNFaQGItZdBFIgC2vIj5XbN2nZ5Ix1LhRTSpZV1lUmGkPHVZLlyZL0a0EtLQ+hVoEE5SBzcYU58Gq/2c0y1QSM/i7hdf/kew2m1WDDB4WlndVhpvDeRgkhiad0NjFsQrE75z+Rx3LYqajwpCFrb6qGRHOnRhhXkb+3Aa3r/Yq87e7Cnv0I1eYRnLynYrx44ONGifVuGjr0hpUbak47H5TJJjzt7FfbqozLBwd+KWawkZ54+cC+CJ5gJEQALxVPETXlJJXxGlTj6FDRsBXV27vgLS7g0N0tYgG1PAoIIEMzCCBC8wggQrMIIDE6ADAgECAgprfJq+AAEAAuBqMA0GCSqGSIb3DQEBBQUAMIGaMRUwEwYDVQQFEww0LTAwMC0wMDQwMTcxCzAJBgNVBAYTAkNSMSQwIgYDVQQKExtCQU5DTyBDRU5UUkFMIERFIENPU1RBIFJJQ0ExKjAoBgNVBAsTIURJVklTSU9OIERFIFNFUlZJQ0lPUyBGSU5BTkNJRVJPUzEiMCAGA1UEAxMZQ0EgU0lOUEUgLSBQRVJTT05BIEZJU0lDQTAeFw0xNDEwMTAxNzM4MDZaFw0xNDEwMjQxNzM4MDZaMBoxGDAWBgNVBAMTD1BPUlZFTklSLmZkaS5jcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJJR5F4Mobk0W91o2AxsgJfFGUcmvigEKH3+fJS/UvZDya8r5YVl2bPLdnAdBS94yZvTZyg6pprC/wuVQ/O/DqfaRxAuu+S2thopvdoftborGN+LSXPsmWlxB94HEi45uvmIB3dGt9MOLZOmJvsDUY3Gc09HSMmTWB/0LhgScL2ET3JcGoaivXwljb2EpLpSVY84ZdcqGy1leosPJ67oLhR0RCHiwDaKb0f8NiGSLWdhW3MU5Tf/KSFbuB6+HgbT6iEGAbQTYiOlHpDz1Cd2RoCZY625RdJF/fYf4PjR9hbN44FqTTq+hKleOPVRzW/x54dOfBP3D03bkd5XRhy/Hx8CAwEAAaOB8TCB7jA9BgkrBgEEAYI3FQcEMDAuBiYrBgEEAYI3FQiFxOpbgtHjNZWRG4L5lxiGpctrgX+D9vx3gbjxMwIBZAIBATATBgNVHSUEDDAKBggrBgEFBQcDCTAOBgNVHQ8BAf8EBAMCB4AwGwYJKwYBBAGCNxUKBA4wDDAKBggrBgEFBQcDCTAPBgkrBgEFBQcwAQUEAgUAMB8GA1UdIwQYMBaAFNG4kICx3z99OCJOwXUG46ju8o2mMB0GA1UdDgQWBBTe7XPe42Zp0AI/McO6ZxTX5wLvdjAaBgNVHREEEzARgg9QT1JWRU5JUi5mZGkuY3IwDQYJKoZIhvcNAQEFBQADggEBAIcyEUo6UAE/RtTo8iM6vcRVZUMPEuJu5NhQd3+fcRDEjDP3Cq2Z3mmIDPG8Gr1tl63th4KTZf4SlyoUIHvjVYkISLvf3ZXBb+4ahz/+tqVsyizgWSe3TU8Ht/z6TJk6t8LoeaM9r3HTrmDJjEoyqxeYf9fNj/iQ4rEFm0cDsyi1bZTItQxQbkMVENlFpAvm1PUVxKjul/tA5y7lDxv9yjVISGFb/5OYLEj5yN/Yh4CxDlofpkmKhMVzqVhGie10tHX/BPNsMx7BHzCysFCAUxslJWxACgoxeKXgP7PFvMdw+NWoDvh3cReVk70ofxSbrcxCp8owYJHyjde6WSMpU/A=</etsi:EncapsulatedOCSPValue><etsi:EncapsulatedOCSPValue Id=\"OCSP278785\">MIIGKAoBAKCCBiEwggYdBgkrBgEFBQcwAQEEggYOMIIGCjCBvKIWBBTe7XPe42Zp0AI/McO6ZxTX5wLvdhgPMjAxNDEwMTQyMDAwNDFaMIGQMIGNMEMwCQYFKw4DAhoFAAQUCeE53CzRjIb88eXVAXO/PcecUc4EFNG4kICx3z99OCJOwXUG46ju8o2mAgphFFa3AAAAAAADgAAYDzIwMTQxMDE0MTkxNjQwWqARGA8yMDE0MTAxNjA3MzY0MFqhIDAeMBwGCSsGAQQBgjcVBAQPFw0xNDEwMTUxOTI2NDBaMA0GCSqGSIb3DQEBBQUAA4IBAQA/e05Mv+t6G6R+345pXrjhln0u8iN+Emm8g/Ml+01MckNYjvbnp8u5y/q+/d8YToEa9zo8geIzvT0Q6e1mI/3dVdYUXz9CiJgSj6ps4sZRl+84Lbkx6ZM7Fai5bQp/qhmZRP1jVpBHoiIdQawRbm4EOgZiguZ/Dv6EUwGj6dX+ueDeJf9DOWX/1ZKwO61nvjJq8WpecE0mGgzzYWMA95uw6fr7JROjOaYPChABDHrePkxeMGXYkPgWlgxPrYRSqi+E3znmwjijAXN8dRyU6BJIg1hkPEXpuXg6GNI+f4hKz6+v/kZqZwBB/U52BSzLRYYqWKO0tPkeiqeZCONL0VoNoIIEMzCCBC8wggQrMIIDE6ADAgECAgprfJq+AAEAAuBqMA0GCSqGSIb3DQEBBQUAMIGaMRUwEwYDVQQFEww0LTAwMC0wMDQwMTcxCzAJBgNVBAYTAkNSMSQwIgYDVQQKExtCQU5DTyBDRU5UUkFMIERFIENPU1RBIFJJQ0ExKjAoBgNVBAsTIURJVklTSU9OIERFIFNFUlZJQ0lPUyBGSU5BTkNJRVJPUzEiMCAGA1UEAxMZQ0EgU0lOUEUgLSBQRVJTT05BIEZJU0lDQTAeFw0xNDEwMTAxNzM4MDZaFw0xNDEwMjQxNzM4MDZaMBoxGDAWBgNVBAMTD1BPUlZFTklSLmZkaS5jcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJJR5F4Mobk0W91o2AxsgJfFGUcmvigEKH3+fJS/UvZDya8r5YVl2bPLdnAdBS94yZvTZyg6pprC/wuVQ/O/DqfaRxAuu+S2thopvdoftborGN+LSXPsmWlxB94HEi45uvmIB3dGt9MOLZOmJvsDUY3Gc09HSMmTWB/0LhgScL2ET3JcGoaivXwljb2EpLpSVY84ZdcqGy1leosPJ67oLhR0RCHiwDaKb0f8NiGSLWdhW3MU5Tf/KSFbuB6+HgbT6iEGAbQTYiOlHpDz1Cd2RoCZY625RdJF/fYf4PjR9hbN44FqTTq+hKleOPVRzW/x54dOfBP3D03bkd5XRhy/Hx8CAwEAAaOB8TCB7jA9BgkrBgEEAYI3FQcEMDAuBiYrBgEEAYI3FQiFxOpbgtHjNZWRG4L5lxiGpctrgX+D9vx3gbjxMwIBZAIBATATBgNVHSUEDDAKBggrBgEFBQcDCTAOBgNVHQ8BAf8EBAMCB4AwGwYJKwYBBAGCNxUKBA4wDDAKBggrBgEFBQcDCTAPBgkrBgEFBQcwAQUEAgUAMB8GA1UdIwQYMBaAFNG4kICx3z99OCJOwXUG46ju8o2mMB0GA1UdDgQWBBTe7XPe42Zp0AI/McO6ZxTX5wLvdjAaBgNVHREEEzARgg9QT1JWRU5JUi5mZGkuY3IwDQYJKoZIhvcNAQEFBQADggEBAIcyEUo6UAE/RtTo8iM6vcRVZUMPEuJu5NhQd3+fcRDEjDP3Cq2Z3mmIDPG8Gr1tl63th4KTZf4SlyoUIHvjVYkISLvf3ZXBb+4ahz/+tqVsyizgWSe3TU8Ht/z6TJk6t8LoeaM9r3HTrmDJjEoyqxeYf9fNj/iQ4rEFm0cDsyi1bZTItQxQbkMVENlFpAvm1PUVxKjul/tA5y7lDxv9yjVISGFb/5OYLEj5yN/Yh4CxDlofpkmKhMVzqVhGie10tHX/BPNsMx7BHzCysFCAUxslJWxACgoxeKXgP7PFvMdw+NWoDvh3cReVk70ofxSbrcxCp8owYJHyjde6WSMpU/A=</etsi:EncapsulatedOCSPValue></etsi:OCSPValues></etsi:RevocationValues></etsi:UnsignedSignatureProperties></etsi:UnsignedProperties></etsi:QualifyingProperties></ds:Object></ds:Signature></documento><?xml-stylesheet type=\"text/xsl\" href=\"http://192.168.0.118/ContractSignerView/xslviewerservlet?contract=MSDD-001\" ?>");
        for (FirmaValida firmaValida : firmasXades) {
            System.out.println(firmaValida.isIsValid());

        }

    }

}
