/**
 * Copyright 2013 Ministerio de Industria, Energía y Turismo
 *
 * Este fichero es parte de "Componentes de Firma XAdES 1.1.7".
 *
 * Licencia con arreglo a la EUPL, Versión 1.1 o –en cuanto sean aprobadas por
 * la Comisión Europea– versiones posteriores de la EUPL (la Licencia); Solo
 * podrá usarse esta obra si se respeta la Licencia.
 *
 * Puede obtenerse una copia de la Licencia en:
 *
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Salvo cuando lo exija la legislación aplicable o se acuerde por escrito, el
 * programa distribuido con arreglo a la Licencia se distribuye «TAL CUAL», SIN
 * GARANTÍAS NI CONDICIONES DE NINGÚN TIPO, ni expresas ni implícitas. Véase la
 * Licencia en el idioma concreto que rige los permisos y limitaciones que
 * establece la Licencia.
 */
package com.cruxconsultores.pegasus.sign.xml;

import java.io.File;
import java.security.AccessController;
import java.security.PrivilegedAction;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cruxconsultores.pegasus.crl.CRLLiveConsultant;
import com.cruxconsultores.pegasus.ocsp.OCSPLiveConsultant;
import com.cruxconsultores.pegasus.utils.ConfigProperties;

import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.firmaJava.libreria.xades.DataToSign.XADES_X_TYPES;
import es.mityc.firmaJava.libreria.xades.XAdESSchemas;
import es.mityc.firmaJava.role.SimpleClaimedRole;
import es.mityc.javasign.EnumFormatoFirma;
import es.mityc.javasign.ts.HTTPTimeStampGenerator;
import es.mityc.javasign.ts.TSPAlgoritmos;
import es.mityc.javasign.xml.refs.InternObjectSignToSign;
import es.mityc.javasign.xml.refs.InternObjectToSign;
import es.mityc.javasign.xml.refs.ObjectToSign;

/**
 * <p>
 * Clase de ejemplo para la firma XAdES-BES enveloping de un documento. El
 * ejemplo firmará el recurso indicado en la constante
 * <code>RESOURCE_TO_SIGN</code> y el nombre del fichero resultante será el
 * indicado por la constante <code>SIGN_FILE_NAME</code>.
 * </p>
 * <p>
 * Para realizar la firma se utilizará el almacén PKCS#12 definido en la
 * constante <code>GenericXMLSignature.PKCS12_FILE</code>, al que se accederá
 * mediante la password definida en la constante
 * <code>GenericXMLSignature.PKCS12_PASSWORD</code>. El directorio donde quedará
 * el archivo XML resultante será el indicado en al constante
 * <code>GenericXMLSignature.OUTPUT_DIRECTORY</code>
 * </p>
 * </p>
 *
 */

public class EnvelopingSignature extends GenericXMLSignature {

	/**
	 * Documento xml a firmar
	 */
	private String archivoAFirmar;

	public final static int USING_CRL = 0;
	public final static int USING_OCSP = 1;
	private int type = -1;

	private DataToSign dataToSign;

	/**
	 * Agrega el archivo a firmar
	 * 
	 * @param archivoAFirmar
	 *            Direccion absoluta del archivo a firmar
	 */
	public void setArchivoAFirmar(String archivoAFirmar) {
		this.archivoAFirmar = archivoAFirmar;
	}

	@Override
	protected DataToSign createDataToSign() {
		return dataToSign;
	}

	@Override
	protected String getSignatureFileName() {
		File f = new File(this.archivoAFirmar);
		String nombre = "";
		if (f.exists()) {
			nombre = f.getAbsolutePath().substring(0,
					f.getAbsolutePath().length() - 4)
					+ "_firmado.xml";
		}
		return nombre;
	}

	public void setSigantureUsing(int type) {
		this.type = type;

	}

	/**
	 * Firma un documento xml por primera
	 *
	 */
	public void firmarDocumento() {

		AccessController.doPrivileged(new PrivilegedAction() {
			@Override
			public Object run() {
				dataToSign = new DataToSign();
				dataToSign.setXadesFormat(EnumFormatoFirma.XAdES_XL);
				dataToSign.setXAdESXType(XADES_X_TYPES.TYPE_1);

				// Valida si es ocs o CRL
				if (type == USING_OCSP) {
					System.out.println(ConfigProperties.URL_OCSP);
					OCSPLiveConsultant liveC = new OCSPLiveConsultant(
							ConfigProperties.URL_OCSP);
					dataToSign.setCertStatusManager(liveC);
				} else if (type == USING_CRL) {
					dataToSign.setCertStatusManager(new CRLLiveConsultant());
				}
				HTTPTimeStampGenerator tsg = new HTTPTimeStampGenerator(
						ConfigProperties.URL_TSA, TSPAlgoritmos.SHA1);
				dataToSign.setTimeStampGenerator(tsg);
				dataToSign.setEsquema(XAdESSchemas.XAdES_132);
				dataToSign.setXMLEncoding("UTF-8");
				dataToSign.addClaimedRol(new SimpleClaimedRole("Rol de firma"));
				dataToSign.setEnveloped(false);
				InternObjectSignToSign objectToSign = new InternObjectSignToSign();
				Document docToSign = getDocument(archivoAFirmar);
				// Valida que tenga atributo id, el nodo raiz del documento xml,
				// si no lo tiene
				// entonces procede a asignarle uno por defecto
				try {
					Element e = docToSign.getDocumentElement();
					if (e.hasAttribute("id")) {
						if (!(e.getAttribute("id").length() > 0)) {
							e.setAttribute("id", "default");
						}
					} else {
						// Esto es raro... Pero solo así sirvió.
						// En java 1.6.45
						e.setAttributeNS(null, "id", "default");
						Attr idAttr = e.getAttributeNode("id");
						e.setAttributeNode(idAttr);

					}
					objectToSign.setData(e);
					dataToSign.addObject(new ObjectToSign(objectToSign,
							"Documento firmado", null, "text/xml", null));
				} catch (Exception ex) {
					System.out.println(ex);
					ex.printStackTrace();
				}

				try {
					execute();
				} catch (Exception ex) {
					System.out.println(ex);
					ex.printStackTrace();
				}
				return null;
			}

		});

	}

	/**
	 * Este método permite cofirmar un documento
	 *
	 * @throws java.lang.Exception
	 */
	public void cofirmarDocumento() throws Exception {
		dataToSign = new DataToSign();
		dataToSign.setXadesFormat(EnumFormatoFirma.XAdES_XL);
		dataToSign.setXAdESXType(XADES_X_TYPES.TYPE_1);
		dataToSign.setEsquema(XAdESSchemas.XAdES_132);
		dataToSign.setXMLEncoding("UTF-8");
		dataToSign.setEnveloped(true);
		dataToSign.setCertStatusManager(new CRLLiveConsultant());
		dataToSign.setTimeStampGenerator(new HTTPTimeStampGenerator(
				ConfigProperties.URL_TSA, TSPAlgoritmos.SHA1));
		dataToSign.addClaimedRol(new SimpleClaimedRole("Rol de contrafirma"));
		Document docToSign = getDocument(this.archivoAFirmar);
		// Obtiene el objeto a firmar
		NodeList lista = docToSign.getDocumentElement().getElementsByTagName(
				"ds:Object");
		if (lista.getLength() > 0) {
			Node n = lista.item(0);
			String idNodoAfirmar = n.getChildNodes().item(0).getAttributes()
					.item(0).getNodeValue();
			dataToSign.addObject(new ObjectToSign(new InternObjectToSign(
					idNodoAfirmar), "Documento de ejemplo", null, "text/xml",
					null));
			dataToSign.setDocument(docToSign);
			this.execute();
		} else {
			firmarDocumento();
			throw new Exception(
					"No es un archivo firmado \n Se procede a firmar por primera vez ");

		}

	}
}
