/*
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.sign.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.tsp.TimeStampTokenInfo;
import org.bouncycastle.util.Store;

import com.cruxconsultores.pegasus.model.FirmaValida;
import com.cruxconsultores.pegasus.model.InformacionParaFirmar;
import com.cruxconsultores.pegasus.ocsp.OCSPVerifier;
import com.cruxconsultores.pegasus.trust.TrustLoader;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDeveloperExtension;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.TSAClient;
import com.itextpdf.text.pdf.TSAClientBouncyCastle;


/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 03/03/2014, 08:56:42 AM
 * @version 1.0 Copyright © 2014 
 * Crux Consultores All Rights Reserved.
 */
public class FirmaItext {

    private static TimeStampToken estampado;
    private static Store certificadoDeTiempo;
    private static TimeStampTokenInfo certificadoDeTiempoInfo;
    private static TimeStampTokenInfo certificate;
    private Certificate[] certificadoArray;
    private Certificate certificado;
    private Date fechaEstampado;
    private int numeroPaginaFirma;

    /**
     * Documento a firmar
     * @param documento El arvhivo pdf a firmar
     * @param ks El Storage de seguridad
     * @param key La llave firmante
     * @param alias El alias del cual se obtendrá la llave privada
     * @param InformacionParaFirmar Información que se utilizará para la firma
     */
    public void firma(File documento, KeyStore ks, PrivateKey key, String alias, InformacionParaFirmar InformacionParaFirmar) {

        try {
        	TrustLoader truster = new TrustLoader();
        	
            // key = (PrivateKey) ks.getKey(alias, keypass);
            Certificate[] chain = ks.getCertificateChain(alias);

            PdfReader reader = new PdfReader(documento.getAbsolutePath());
            String NombreFile = documento.getPath().substring(documento.getPath().lastIndexOf('/') + 1, documento.getPath().lastIndexOf('.'));
            FileOutputStream fout = new FileOutputStream(NombreFile + "_firmado.pdf");
            //cuarto parametro para mas de una firma
            PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0', null, reader.getAcroFields().getSignatureNames().size() > 0);
            PdfSignatureAppearance sap = stp.getSignatureAppearance();
            stp.setFullCompression();

            sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
            sap.setReason(InformacionParaFirmar.getRazon());
            sap.setAcro6Layers(true);
            // sap.setImage(InformacionParaFirmar.getImage());
            sap.setSignatureGraphic(InformacionParaFirmar.getImage());

            //incluir imagen demo
            Image imagenDemo = Image.getInstance(getClass().getResource("/resources/demo.png"));
            sap.setImage(imagenDemo);
            sap.setLocation(InformacionParaFirmar.getUbicacion());

            if (InformacionParaFirmar.isUltimaPagina()) {

                numeroPaginaFirma = reader.getNumberOfPages();

            } else {

                numeroPaginaFirma = 1;
            }
            // PAdES parte 3 seccion 4.7 - Habilitacion para LTV
            stp.getWriter().addDeveloperExtension(new PdfDeveloperExtension(new PdfName("ESIC"), PdfWriter.PDF_VERSION_1_7, 1));

            sap.setVisibleSignature(InformacionParaFirmar.getRectangle(), numeroPaginaFirma, null);


            PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, new PdfName("adbe.pkcs7.detached"));
            dic.setReason(sap.getReason());
            dic.setLocation(sap.getLocation());
            dic.setContact(sap.getContact());
            dic.setDate(new PdfDate(sap.getSignDate()));


            if (InformacionParaFirmar.getImage() != null) {
                sap.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION);
            } else {

                sap.setRenderingMode(PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION);
            }

            sap.setCryptoDictionary(dic);
            sap.setExternalDigest(new byte[128], null, "RSA");
            // Reservamos el espacio necesario en el PDF para insertar la firma
            int contentEstimated = 15000;
            final HashMap<PdfName, Integer> exc = new HashMap<PdfName, Integer>();
            exc.put(PdfName.CONTENTS, Integer.valueOf(contentEstimated * 2 + 2));
            sap.preClose(exc);

            PdfPKCS7 sgn = new PdfPKCS7(key, chain, null, "SHA1", null, false);
            InputStream data = sap.getRangeStream();
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");

            byte buf[] = new byte[8192];
            int n;
            while ((n = data.read(buf)) > 0) {
                messageDigest.update(buf, 0, n);
            }
            byte hash[] = messageDigest.digest();
            Calendar cal = Calendar.getInstance();

            byte[] ocsp = null;

            Security.addProvider(new BouncyCastleProvider());
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            
            

            X509Certificate rootCert = truster.getCaFromDistributionPoint((X509Certificate) chain[0]);
            String url = PdfPKCS7.getOCSPURL((X509Certificate) chain[0]);
            if (url != null && url.length() > 0) {
                ocsp = new OcspClientBouncyCastle((X509Certificate) chain[0], rootCert, url).getEncoded();

            }

            byte sh[] = sgn.getAuthenticatedAttributeBytes(hash, cal, ocsp);
            sgn.update(sh, 0, sh.length);


            TSAClient tsc = new TSAClientBouncyCastle("http://tsa.sinpe.fi.cr/tsaHttp/", null, null);
            byte[] encodedSig = sgn.getEncodedPKCS7(hash, cal, tsc, ocsp);

            if (contentEstimated + 2 < encodedSig.length) {
                throw new Exception("Not enough space");
            }

            byte[] paddedSig = new byte[contentEstimated];


            System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);

            final PdfDictionary dic2 = new PdfDictionary();

            dic2.put(PdfName.CONTENTS, new PdfString(paddedSig).setHexWriting(true));
            sap.close(dic2);
            Security.removeProvider("SunPKCS11-SmartCard");

        } catch (Exception e) {

            e.printStackTrace();
            //JOptionPane.showMessageDialog(null, "Certificado desconocido, OCPS Host desconocido");
        }
    }

    /**
     * Valida multifirma
     * @param fileToVerificar
     * @return 
     */
      public List<FirmaValida> obtenerFirmas(File fileToVerificar) {
        List<FirmaValida> listaFirmas = new ArrayList<FirmaValida>();
        try {

            KeyStore myKeyStore = PdfPKCS7.loadCacertsKeyStore();
            //   KeyStore myKeyStore = PdfPKCS7.loadCacertsKeyStore("PKCS11");
            PdfReader reader = new PdfReader(fileToVerificar.getAbsolutePath());
            AcroFields af = reader.getAcroFields();

            // Search of the whole signature
            ArrayList names = af.getSignatureNames();

            // For every signature :
            for (int k = 0; k < names.size(); ++k) {
                String name = (String) names.get(k);

                PdfPKCS7 pk = af.verifySignature(name);

                Calendar cal = pk.getSignDate();

                Certificate pkc[] = pk.getSignCertificateChain();
                estampado = pk.getTimeStampToken();

                if (estampado != null) {
                    certificate = estampado.getTimeStampInfo();
                    certificadoDeTiempoInfo = estampado.getTimeStampInfo();
                    fechaEstampado = certificadoDeTiempoInfo.getGenTime();

                }

                certificadoArray = pk.getCertificates();
                certificado = certificadoArray[0];
                OCSPVerifier consumirOCPS = new OCSPVerifier();
                Integer estadoOCSP = consumirOCPS.getStatus(pk.getOcsp());
                X509Certificate certificadoX509 = (X509Certificate) certificado;

                FirmaValida firmaValida;
                firmaValida = new FirmaValida(fileToVerificar.getName(), pk.getSignDate(), pk.verify(), pk.verifyTimestampImprint(),
                        pk.getHashAlgorithm(), pk.getReason(), pk.getLocation(), estadoOCSP, fechaEstampado, certificadoX509);
                listaFirmas.add(firmaValida);
            }
        } catch (NoSuchAlgorithmException ex) {
           ex.fillInStackTrace();
        } catch (SignatureException ex) {
             ex.fillInStackTrace();
        } catch (IOException ex) {
             ex.fillInStackTrace();
        }
        return listaFirmas;
    }
    
    
    /**
     * Valida una unica firma
     * @param archivoEntrada 
     */
    public void validarFirma(String archivoEntrada) {

        try {

            Random rnd = new Random();
            KeyStore myKeyStore = PdfPKCS7.loadCacertsKeyStore();
            PdfReader reader = new PdfReader(archivoEntrada);
            AcroFields af = reader.getAcroFields();
            ArrayList names = af.getSignatureNames();

            for (int k = 0; k < names.size(); ++k) {

                String name = (String) names.get(k);
                int random = rnd.nextInt();
                FileOutputStream out = new FileOutputStream("revision_" + random + "_" + af.getRevision(name) + ".pdf");
                byte bb[] = new byte[8192];
                InputStream ip = af.extractRevision(name);

                int n = 0;

                while ((n = ip.read(bb)) > 0) {
                    out.write(bb, 0, n);
                }

                out.close();
                ip.close();

                PdfPKCS7 pk = af.verifySignature(name);

                Calendar cal = pk.getSignDate();

                Certificate pkc[] = pk.getCertificates();

                Object fails[] = PdfPKCS7.verifyCertificates(pkc, myKeyStore, null, cal);

                if (fails == null) {

                    System.out.print(pk.getSignName());

                } else {
                    System.out.print("Firma no válida");
                }

                File f = new File("revision_" + random + "_" + af.getRevision(name) + ".pdf");
                f.delete();

            }

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    public FirmaValida verifyPdf(File rutaEntrada) {

        try {

            KeyStore myKeyStore = PdfPKCS7.loadCacertsKeyStore();
            //   KeyStore myKeyStore = PdfPKCS7.loadCacertsKeyStore("PKCS11");
            PdfReader reader = new PdfReader(rutaEntrada.getAbsolutePath());
            AcroFields af = reader.getAcroFields();

            // Search of the whole signature
            ArrayList names = af.getSignatureNames();

            // For every signature :
            for (int k = 0; k < names.size(); ++k) {
                String name = (String) names.get(k);
                // Affichage du nom

                // Debut de l'extraction de la "revision"
                String NombreFile = rutaEntrada.getPath().substring(rutaEntrada.getPath().lastIndexOf('/') + 1, rutaEntrada.getPath().lastIndexOf('.'));

                FileOutputStream out = new FileOutputStream(NombreFile + "_revision.pdf");
                byte bb[] = new byte[8192];
                InputStream ip = af.extractRevision(name);
                int n = 0;
                while ((n = ip.read(bb)) > 0) {
                    out.write(bb, 0, n);
                }
                out.close();
                ip.close();
                // Fin extraction revision

                PdfPKCS7 pk = af.verifySignature(name);

                Calendar cal = pk.getSignDate();

                Certificate pkc[] = pk.getSignCertificateChain();
                estampado = pk.getTimeStampToken();

                if (estampado != null) {

                    certificate = estampado.getTimeStampInfo();
                    certificadoDeTiempoInfo = estampado.getTimeStampInfo();
                    fechaEstampado = certificadoDeTiempoInfo.getGenTime();

                }

                certificadoArray = pk.getCertificates();
                certificado = certificadoArray[0];
                OCSPVerifier consumirOCPS = new OCSPVerifier();
                Integer estadoOCSP = consumirOCPS.getStatus(pk.getOcsp());
                X509Certificate certificadoX509 = (X509Certificate) certificado;

                FirmaValida firmaValida;
                firmaValida = new FirmaValida(rutaEntrada.getName(), pk.getSignDate(), pk.verify(), pk.verifyTimestampImprint(), pk.getHashAlgorithm(), pk.getReason(), pk.getLocation(), estadoOCSP, fechaEstampado, certificadoX509);

                return firmaValida;
            }
        } catch (SignatureException ex) {
            Logger.getLogger(FirmaItext.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(FirmaItext.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FirmaItext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PdfPKCS7 verifySignature(String name) throws SignatureException, IOException {

        PdfReader reader = new PdfReader("my_signed_doc.pdf");
        AcroFields af = reader.getAcroFields();
        ArrayList names = af.getSignatureNames();

        for (int k = 0; k < names.size(); ++k) {

            name = (String) names.get(k);
            System.out.println("Signature name: " + name);
            System.out.println("Signature covers whole document: " + af.signatureCoversWholeDocument(name));
            PdfPKCS7 pk = af.verifySignature(name);
            Calendar cal = pk.getSignDate();
            Certificate pkc[] = pk.getCertificates();

        }
        return null;
    }

    /**
     * Se encarga de firmar sin el estampado de tiempo
     *
     * @param documento
     * @param ks
     * @param pin
     * @param alias
     * @throws Exception
     */
    public void firmaSinEstampado(String documento, KeyStore ks, String pin, String alias, InformacionParaFirmar InformacionParaFirmar) throws Exception {

        PrivateKey key = (PrivateKey) ks.getKey(alias, pin.toCharArray());

        Certificate[] chain = ks.getCertificateChain(alias);

        String document_to_sign = documento;
        String document_signed = "firmado.pdf";

        PdfReader reader = new PdfReader(document_to_sign);
        FileOutputStream fout = new FileOutputStream(document_signed);
        PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0');

        PdfSignatureAppearance sap = stp.getSignatureAppearance();

        sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);

        if (InformacionParaFirmar.isUltimaPagina()) {
            numeroPaginaFirma = reader.getNumberOfPages();
        } else {
            numeroPaginaFirma = 1;
        }

        sap.setReason(InformacionParaFirmar.getRazon());
        sap.setLocation("San josé");
        sap.setAcro6Layers(true);
//        sap.setImage(InformacionParaFirmar.getImage());

        sap.setSignatureGraphic(InformacionParaFirmar.getImage());

        //incluir imagen demo
        Image imagenDemo = Image.getInstance(getClass().getResource("/pegasus/crux/componente/utilerias/imagenes/demo.png"));
        sap.setImage(imagenDemo);

        Rectangle rect = new Rectangle(450, 30, 550, 90);
        sap.setVisibleSignature(rect, numeroPaginaFirma, null);
        stp.close();

    }
}