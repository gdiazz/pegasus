/*
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.sign.text;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;

import com.cruxconsultores.pegasus.model.SignObjectComponet;
import com.cruxconsultores.pegasus.utils.CertificateUtils;

/**
 * Esta clase se encarga de las firmas de texto
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 11/03/2014, 11:20:20 AM
 * @version 1.0
 */
public class StringSigner {

    /**
     * Aunque la semantica difiere del contexto, este objeto contiene
     * todas los elemenos necesarios para firmar un texto
     */
    private SignObjectComponet componente;
    
    /**
     * Proveedor de seguridad
     */
    private Provider provider;
    
    /**
     * Crea el objeto con todo lo necesario para firmar
     * @param comp 
     */
    public StringSigner(SignObjectComponet comp){
        this.componente = comp;
    }

    public StringSigner() {
    }
    
    
    /**
     * Firma una cadena de texto utilizando los datos del certificado del usuario
     * @param text Cadena de texto a firmar
     * @return Texto firmado
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws SignatureException
     * @throws InvalidKeyException 
     */
    public String signText(String text) throws NoSuchAlgorithmException, NoSuchProviderException, SignatureException, InvalidKeyException {
        if (text != null) {
            if (!text.isEmpty()) {
                Signature signer = Signature.getInstance("SHA1withRSA");
                signer.initSign(this.componente.getPrivateKey());
                signer.update(text.getBytes());
                return  new String(new Base64().encode(signer.sign()));
            }
        }
        return null;
    }
    /**
     * Verifica una cadena de texto firmada
     * @param original
     * @param text
     * @param cert
     * @return 
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.InvalidKeyException
     * @throws java.security.SignatureException
     * @throws java.security.cert.CertificateException
     */
    public boolean verifySignedText(String original, String text, String cert) throws  NoSuchAlgorithmException, InvalidKeyException, SignatureException, CertificateException{
        boolean isValid = false;
        X509Certificate certificate;
        if (text != null) {
            if (!text.isEmpty()) {
                Signature signature = Signature.getInstance("SHA1withRSA");
                certificate = CertificateUtils.getCertificate(cert);
                signature.initVerify(certificate.getPublicKey());
                signature.update(original.getBytes());
                isValid = signature.verify(new Base64().decode(text.getBytes()));
            }

        }
        return isValid;

    }
    
    
   
    
}
