/*
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasus.validation;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.cruxconsultores.pegasus.utils.FileUtils;

import es.mityc.javasign.utils.Base64Coder;
/**
 * Esta clase se encarga de obtener todos los certificados firmante en un docu-
 * mento xml firmado
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 14/02/2014, 11:31:56 AM
 * @version 1.0
 */
public class CertExtractor {

    /**
     * Es el nombre del tag que contiene ceritificados
     */
    private static final String TAG_CERT = "ds:X509Certificate";

    /**
     * direccion del archivo analziado
     */
    private String dirFile;

    /**
     * Lista de certificados en la firma
     */
    private List<String> certList;

    /**
     * Lista de certificados
     */
    private List<X509Certificate> X509CertList;

    /**
     * Recibe la dirección path del documento a analizar
     * @param dirFile Directorio de archivo a analizar
     */
    public CertExtractor(String dirFile) {
        this.dirFile = dirFile;
        certList = getAllCerts(TAG_CERT);
    }

    /**
     * Obtiene el directorio donde se encuentra el archivo a analizar
     * @return Dirección del archivo
     */
    public String getDirFile() {
        return dirFile;
    }

    /**
     * Obtiena la lista de certificads
     * @return Lita de certficados
     */
    public List<String> getCertList() {
        return certList;
    }

    /**
     * Obtiene la lista de de certificados del documento XML
     * @return Lista de certificados x509 del documento xml
     */
    public List<X509Certificate> getX509CertList() {
        X509CertList = new ArrayList<X509Certificate>();
        for (String string : certList) {
            X509CertList.add(getCertFromString(string));
        }
        return X509CertList;
    }

    /**
     * Obtiene una lista de certificados cifrados en texto de un xml firmado
     * @return Lista de certificados cifrados en texto
     */
    private List<String> getAllCerts(String tagName) {
        List<String> certs = new ArrayList<String>();
        Document xml = FileUtils.parseXmlTODoc(this.dirFile);
        NodeList list = xml.getElementsByTagName(tagName);

        for (int i = 0; i < list.getLength(); i++) {
            certs.add(list.item(i).getNodeValue());
        }

        return certs;

    }


    /**
     * Obtiene un certificado de una cadena de texto con 
     * el formato adecuado (El texto viene encriptado en Base64)
     * @param s
     * @return devuelve un certificado
     */
    public static X509Certificate getCertFromString(String s) {
        X509Certificate cert = null;
        InputStream is = null;
        //Se decodifica el texto de base64
        byte[] certEntryBytes = Base64Coder.decode(s);
        is = new ByteArrayInputStream(certEntryBytes);

        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509");
            cert = (X509Certificate) cf.generateCertificate(is);
        } catch (CertificateException ex) {
            Logger.getLogger(CertExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cert;

    }

}
