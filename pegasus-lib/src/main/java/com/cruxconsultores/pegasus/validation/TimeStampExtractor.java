/*
 * Copyright © 2014 by Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasus.validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampToken;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.cruxconsultores.pegasus.utils.FileUtils;

import es.mityc.javasign.utils.Base64Coder;

/**
 * Esta clase obtiene todos los sellos de tiempo
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since Creado en 14/02/2014, 01:52:51 PM
 * @version 1.0

 */
public class TimeStampExtractor {
    
    /**
     * Es el nombre del tag que contiene los sellos de tiempo
     */
    private static final String TAG_TS = "etsi:SignatureTimeStamp";

    /**
     * direccion del archivo analziado 
     */
    private String dirFile;

    /**
     * Lista de sellos de tiempo en la firma
     */
    private List<TimeStampToken> tsList;

    /**
     * Extrae todos los sellos de tiempo del archivo
     * @param dirFile  dirección absoluta del archivo xml a extraer los sellos
     */
    public TimeStampExtractor(String dirFile) {
        this.dirFile = dirFile;
        tsList = getAllTimeStampByTag(TAG_TS);
    }

    /**
     * Obtiene la lista con todos los sellos de tiempo que tenga el archivo xml
     * @return 
     */
    public List<TimeStampToken> getTsList() {
        return tsList;
    }
    
    
    

    
    /**
     * Obtiene una lista de certificados cifrados en texto de un xml firmado
     *
     * @param tagName
     * @return Lista de certificados cifrados en texto
     */
    private List< TimeStampToken > getAllTimeStampByTag(String tagName) {
        tsList = new ArrayList<TimeStampToken>();
        Document xml = FileUtils.parseXmlTODoc(this.dirFile);
        NodeList list = xml.getElementsByTagName(tagName);

        for (int i = 0; i < list.getLength(); i++) {
            tsList.add(getTimteStampTokenFromString(list.item(i).getNodeValue()));
        }
        return tsList;
    }
    
    /**
     * Obtiene el sello de tiempo de un String
     * Este metodo es usado para obtener el sello de tiempo de 
     * alguna Hilera de texto obtenida de los nodos de sellos de tiempo de una 
     * firma.
     * @param array
     * @return 
     */
    private TimeStampToken getTimteStampTokenFromString(String s){
        byte [] bytes = Base64Coder.decode(s);
        TimeStampToken timeStampToken = null;
        try {
            timeStampToken = new TimeStampToken(new CMSSignedData(bytes));
        } catch (TSPException ex) {
            Logger.getLogger(TimeStampExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TimeStampExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CMSException ex) {
            Logger.getLogger(TimeStampExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return timeStampToken;
    }

}
