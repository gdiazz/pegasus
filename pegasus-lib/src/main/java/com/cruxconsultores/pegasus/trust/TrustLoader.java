/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cruxconsultores.pegasus.trust;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cruxconsultores.pegasus.utils.CertificateUtils;
import com.cruxconsultores.pegasus.utils.ConfigProperties;
import com.cruxconsultores.pegasus.utils.FileUtils;
import com.cruxconsultores.pegasus.utils.Util;

/**
 * Es un gestor de certificados de confianza. Requiere de un archivo de
 * configuración para relacionar las CA con los certificados para firma. Entre
 * sus tareas está en identificar quién es el emisor de un certificado en
 * cuestión.
 *
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 22/09/2014
 * @version 1.0 Copyright © 2014 Crux Consultores All Rights Reserved.
 */
public final class TrustLoader {

	/**
	 * 
	 */
	private static final String TRUST_CONFIG_XML = "/config/TrustConfig.xml";
	private final Document doc;
	private final InputStream isTrustFile;
	private List<TrustedCertificate> trustedCertificates;

	public List<TrustedCertificate> getTrustedCertificates() {
		return trustedCertificates;
	}

	public void setTrustedCertificates(
			List<TrustedCertificate> trustedCertificates) {
		this.trustedCertificates = trustedCertificates;
	}

	/**
	 * El construcutor por defecto que carga los certificados configurados en el
	 * archivo TrustConfig.xml
	 */
	public TrustLoader() {
		isTrustFile = TrustLoader.class.getResourceAsStream(TRUST_CONFIG_XML);
		doc = FileUtils.inputStreamToDocument(isTrustFile);

		NodeList nodes = doc.getElementsByTagName("certificate");
		trustedCertificates = new ArrayList<TrustedCertificate>();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);

			TrustedCertificate cert = new TrustedCertificate();

			cert.setName(n.getAttributes().getNamedItem("name").getNodeValue());
			// cert.setSerialNumber(n.getAttributes().getNamedItem("serial").getNodeValue());

			NodeList childs = n.getChildNodes();

			cert.setCaName(childs.item(1).getNodeValue());
			cert.setUrlCrt(childs.item(3).getNodeValue());
			cert.setUrlOcsp(childs.item(5).getNodeValue());
			cert.setUrlCrl(childs.item(7).getNodeValue());
			trustedCertificates.add(cert);
		}
		downLoadAllCerts(trustedCertificates);
		downLoadAllCrls(trustedCertificates);

	}

	public void downLoadAllCerts(List<TrustedCertificate> trustedCerts) {
		for (TrustedCertificate trustedCertificate : trustedCerts) {

			String path = ConfigProperties.DIR_CA + "/";
			String fileName = null;
			try {
				fileName = new File(URLDecoder.decode(
						trustedCertificate.getUrlCrt(), "UTF-8")).getName();
			} catch (UnsupportedEncodingException ex) {

			}
			if (!(new File(path + fileName).exists())) {
				try {
					URL url = new URL(trustedCertificate.getUrlCrt());

					url.openConnection();
					InputStream file = url.openStream();
					byte[] data = IOUtils.toByteArray(file);
					org.apache.commons.io.FileUtils.writeByteArrayToFile(
							new File(path + fileName), data);
					System.out.println("Descargado: " + fileName);
				} catch (MalformedURLException ex) {
					ex.printStackTrace();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public void downLoadAllCrls(List<TrustedCertificate> trustedCerts) {
		System.out.println(trustedCerts.size());
		for (TrustedCertificate trustedCertificate : trustedCerts) {

			// Descarga o carga los CRL configurados
			String pathCrl = ConfigProperties.DIR_CRL + "/";
			String fileNameCrl = null;
			try {

				fileNameCrl = new File(URLDecoder.decode(
						trustedCertificate.getUrlCrl(), "UTF-8")).getName();
			} catch (UnsupportedEncodingException ex) {

			}

			boolean isDownload = false;
			try {
				X509CRL loadCRL = Util.loadCRL(new FileInputStream(pathCrl
						+ fileNameCrl));
				if (loadCRL.getNextUpdate().after(new Date())) {
					isDownload = true;

				}
			} catch (CertificateException ex) {

			} catch (CRLException ex) {

			} catch (IOException ex) {

			}

			if (new File(pathCrl + fileNameCrl).exists() && isDownload) {
				try {
					trustedCertificate.setCrl(Util.loadCRL(new FileInputStream(
							pathCrl + fileNameCrl)));

				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				} catch (CertificateException ex) {
					ex.printStackTrace();
				} catch (CRLException ex) {
					ex.printStackTrace();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			} else {
				try {

					URL url = new URL(trustedCertificate.getUrlCrl());

					url.openConnection();

					InputStream file = url.openStream();
					byte[] data = IOUtils.toByteArray(file);

					org.apache.commons.io.FileUtils.writeByteArrayToFile(
							new File(pathCrl + fileNameCrl), data);
					trustedCertificate.setCrl(Util.loadCRL(new FileInputStream(
							pathCrl + fileNameCrl)));
					System.out.println("Descargado: " + fileNameCrl);
				} catch (MalformedURLException ex) {
				} catch (IOException ex) {
				} catch (CertificateException ex) {
				} catch (CRLException ex) {

				}

			}
		}
	}

	public X509Certificate getCaFromDistributionPoint(X509Certificate cert) {
		String get = null;
		String path = ConfigProperties.DIR_CA + "/";
		try {
			get = URLDecoder.decode(
					CertificateUtils.getCaDistributionPoints(cert).get(0),
					"UTF-8");
			get = get.substring(0, get.length() - 4);
		} catch (UnsupportedEncodingException ex) {

		} catch (CertificateParsingException ex) {

		} catch (IOException ex) {

		}
		if (trustedCertificates != null) {
			for (TrustedCertificate trustedCertificate : trustedCertificates) {

				File f = new File(get);
				if (trustedCertificate.getName().equals(f.getName())) {
					try {
						X509Certificate ca = Util.loadCert(path
								+ trustedCertificate.getName() + ".crt");
						return ca;
					} catch (FileNotFoundException ex) {

					} catch (CertificateException ex) {

					}

				}

			}

		}

		return null;
	}

	public CRL getCrl(X509Certificate cert) {
		if (trustedCertificates != null) {
			for (TrustedCertificate trustedCertificate : trustedCertificates) {

			}

		}
		return null;
	}

	public X509Certificate getCertificate(URL url) {
		return null;
	}

}
