/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cruxconsultores.pegasus.trust;

import java.security.cert.CRL;
import java.security.cert.X509Certificate;

/**
 *
 * @author Guillermo
 */
public class TrustedCertificate {
    private String name;
    private String serialNumber;
    private String urlCrt;
    private String urlCaIssuer;
    private String urlCrl;
    private String urlOcsp;
    private String caName;
    
    
    private X509Certificate caIssuer;
    private CRL crl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUrlCaIssuer() {
        return urlCaIssuer;
    }

    public void setUrlCaIssuer(String urlCaIssuer) {
        this.urlCaIssuer = urlCaIssuer;
    }

    public String getUrlCrl() {
        return urlCrl;
    }

    public void setUrlCrl(String urlCrl) {
        this.urlCrl = urlCrl;
    }

    public String getUrlOcsp() {
        return urlOcsp;
    }

    public void setUrlOcsp(String urlOcsp) {
        this.urlOcsp = urlOcsp;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public X509Certificate getCaIssuer() {
        return caIssuer;
    }

    public void setCaIssuer(X509Certificate caIssuer) {
        this.caIssuer = caIssuer;
    }

    public CRL getCrl() {
        return crl;
    }

    public void setCrl(CRL crl) {
        this.crl = crl;
    }

    public String getUrlCrt() {
        return urlCrt;
    }

    public void setUrlCrt(String urlCrt) {
        this.urlCrt = urlCrt;
    }

    @Override
    public String toString() {
        return "TrustedCertificate{" + "name=" + name + ", serialNumber=" + serialNumber + ", urlCrt=" + urlCrt + ", urlCaIssuer=" + urlCaIssuer + ", urlCrl=" + urlCrl + ", urlOcsp=" + urlOcsp + ", caName=" + caName + ", caIssuer=" + caIssuer + ", crl=" + crl + '}';
    }

    

    
    
    
    
}
