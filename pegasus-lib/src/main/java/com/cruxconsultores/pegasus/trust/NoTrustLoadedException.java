/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cruxconsultores.pegasus.trust;

/**
 *
 * @author Guillermo
 */
public class NoTrustLoadedException extends Exception{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoTrustLoadedException(String msg) {
        super(msg);
    }
    
    
}
