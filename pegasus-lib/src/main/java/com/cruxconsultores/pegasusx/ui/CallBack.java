/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */

package com.cruxconsultores.pegasusx.ui;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.cert.CertificateException;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 23/09/2014
 */
public interface CallBack {
    public void run(String str) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, ProviderException;
}
