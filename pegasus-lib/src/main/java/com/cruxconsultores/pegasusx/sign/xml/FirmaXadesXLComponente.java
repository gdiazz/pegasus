/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasusx.sign.xml;

import java.io.File;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.w3c.dom.Document;

import com.cruxconsultores.pegasus.device.DeviceManager;
import com.cruxconsultores.pegasus.device.model.AthenaSmartCard;
import com.cruxconsultores.pegasus.ocsp.OCSPLiveConsultant;

import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.javasign.EnumFormatoFirma;
import es.mityc.javasign.ts.HTTPTimeStampGenerator;
import es.mityc.javasign.ts.TSPAlgoritmos;
import es.mityc.javasign.xml.refs.InternObjectToSign;
import es.mityc.javasign.xml.refs.ObjectToSign;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 23/09/2014
 */
public class FirmaXadesXLComponente extends GenericXadesSignature {

    private String fileToSign;
    private String description;
    private String signRol;
    private String nodeTosign;
    private String parentId;
    private DeviceManager manager;
    public static int USING_OCSP = 0;
    public static int USING_CRL = 1;
    private int sigantureUsing;
    @Override
    protected DeviceManager getDeviceManager() {
        return this.manager;
    }

    
    
    @Override
    protected DataToSign createDataToSign() {
        if (this.description == null) {
            this.description = "Default";
        }

        if (this.signRol == null) {
            this.signRol = "Default";
        }

        DataToSign dataToSign = new DataToSign();
        dataToSign.setXadesFormat(EnumFormatoFirma.XAdES_XL);
        dataToSign.setCertStatusManager(new OCSPLiveConsultant("http://ocsp.sinpe.fi.cr/ocsp"));
        dataToSign.setTimeStampGenerator(new HTTPTimeStampGenerator("http://tsa.sinpe.fi.cr/tsaHttp/", TSPAlgoritmos.SHA1));
        dataToSign.setXMLEncoding("UTF-8");
        dataToSign.setEnveloped(true);
        Document docToSign = getDocument(this.fileToSign);
        dataToSign.setDocument(docToSign);
        dataToSign.addObject(new ObjectToSign(new InternObjectToSign(nodeTosign), this.description, null, "text/xml", null));
        dataToSign.setParentSignNode(this.parentId);
        return dataToSign;
    }

    public String getFileToSign() {
        return fileToSign;
    }

    public void setFileToSign(String fileToSign) {
        this.fileToSign = fileToSign;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSignRol() {
        return signRol;
    }

    public void setSignRol(String signRol) {
        this.signRol = signRol;
    }

    public String getNodeTosign() {
        return nodeTosign;
    }

    public void setNodeTosign(String nodeTosign) {
        this.nodeTosign = nodeTosign;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public DeviceManager getManager() {
        return manager;
    }

    public void setManager(DeviceManager manager) {
        this.manager = manager;
    }

    public int getSigantureUsing() {
        return sigantureUsing;
    }

    public void setSigantureUsing(int sigantureUsing) {
        this.sigantureUsing = sigantureUsing;
    }
    
    public String getSignedFilePath() {
        File f = new File(this.fileToSign);
        String nombre = "";
        if (f.exists()) {
            nombre = f.getAbsolutePath().substring(0, f.getAbsolutePath().length() - 4) + "_firmado.xml";
        }
        return nombre;
    }
    
    
    //2382734
    public static void main(String args[]) throws Exception {
        FirmaXadesXLComponente signature = new FirmaXadesXLComponente();
        signature.setDescription("Ejemplo");
        signature.setSignRol("Primera Firma");
        signature.setParentId("documentoDigital");
        signature.setNodeTosign("principal");
        signature.setFileToSign("C:\\Users\\Guillermo\\Desktop\\firmado\\temp_firmado.xml");
        signature.setOutPutFile("C:\\Users\\Guillermo\\Desktop\\firmado\\temp_firmado2.xml");
        DeviceManager manager = new AthenaSmartCard();
        manager.init("1189");
        signature.setManager(manager);
        
        signature.execute();

    }

}

