/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasusx.sign.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cruxconsultores.pegasus.device.DeviceManager;
import com.cruxconsultores.pegasus.device.EnumKeyUsage;
import com.cruxconsultores.pegasus.model.PegasusCertificate;
import com.cruxconsultores.pegasus.utils.FileUtils;

import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.firmaJava.libreria.xades.FirmaXML;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 23/09/2014
 */
public abstract class GenericXadesSignature {

	private String outPutFile;
	private String urlOcsp;
	private String urlTsa;
	private EnumKeyUsage usage;

	public void execute() throws Exception {

		DeviceManager deviceManager = getDeviceManager();

		if (deviceManager == null) {
			System.err.println("El gestor de llaves no se ha logrado obtener");
			return;
		}

		PegasusCertificate pcert = deviceManager.getPegCertificatesByUsage(
				usage).get(0);
		X509Certificate certificate = pcert.getCert();
		if (certificate == null) {
			System.err.println("No existe ningún certificado para firmar");
			return;

		}

		PrivateKey privateKey;

		try {
			privateKey = deviceManager.getPrivateKey(pcert.getAlias());
		} catch (UnrecoverableKeyException ex) {
			System.err.println("Error al obtener la llave privada del gestor");
			return;
		} catch (KeyStoreException ex) {
			System.err.println("Error al obtener la llave privada del gestor");
			return;
		} catch (IOException ex) {
			System.err.println("Error al obtener la llave privada del gestor");
			return;
		} catch (CertificateException ex) {
			System.err.println("Error al obtener la llave privada del gestor");
			return;
		} catch (NoSuchAlgorithmException ex) {
			System.err.println("Error al obtener la llave privada del gestor");
			return;
		}

		Provider provider = deviceManager.getProvider();

		DataToSign dataToSign = createDataToSign();

		FirmaXML firma = new FirmaXML();

		Document docSined = null;

		Object res[] = firma.signFile(certificate, dataToSign, privateKey,
				provider);
		docSined = (Document) res[0];

		saveDocumentToFile(docSined, outPutFile);

	}

	protected abstract DeviceManager getDeviceManager();

	protected abstract DataToSign createDataToSign();

	protected Document getDocument(String source) {
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		try {
			doc = dbf.newDocumentBuilder().parse(source);
		} catch (ParserConfigurationException ex) {
			System.err.println("Error al parsear el documento");
			ex.printStackTrace();
			System.exit(-1);
		} catch (SAXException ex) {
			System.err.println("Error al parsear el documento");
			ex.printStackTrace();
		} catch (IOException ex) {
			System.err.println("Error al parsear el documento");
			ex.printStackTrace();
		} catch (IllegalArgumentException ex) {
			System.err.println("Error al parsear el documento");
			ex.printStackTrace();
		}
		return doc;
	}


	private void saveDocumentToFile(Document document, String pathfile) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(pathfile);

			FileUtils.writeXML(document, fos);
		} catch (FileNotFoundException e) {
			System.err.println("Error al salvar el documento");
			e.printStackTrace();
			System.exit(-1);
		} finally {
			try {
				fos.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public String getOutPutFile() {
		return outPutFile;
	}

	public void setOutPutFile(String outPutFile) {
		this.outPutFile = outPutFile;
	}

	/**
	 * @return the urlOcsp
	 */
	public String getUrlOcsp() {
		return urlOcsp;
	}

	/**
	 * @param urlOcsp
	 *            the urlOcsp to set
	 */
	public void setUrlOcsp(String urlOcsp) {
		this.urlOcsp = urlOcsp;
	}

	/**
	 * @return the urlTsa
	 */
	public String getUrlTsa() {
		return urlTsa;
	}

	/**
	 * @param urlTsa
	 *            the urlTsa to set
	 */
	public void setUrlTsa(String urlTsa) {
		this.urlTsa = urlTsa;
	}

	/**
	 * @return the usage
	 */
	public EnumKeyUsage getUsage() {
		return usage;
	}

	/**
	 * @param usage
	 *            the usage to set
	 */
	public void setUsage(EnumKeyUsage usage) {
		this.usage = usage;
	}

}
