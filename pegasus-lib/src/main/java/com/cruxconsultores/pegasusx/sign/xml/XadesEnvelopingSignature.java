/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasusx.sign.xml;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import com.cruxconsultores.pegasus.device.DeviceManager;
import com.cruxconsultores.pegasus.device.model.AthenaSmartCard;
import com.cruxconsultores.pegasus.ocsp.OCSPLiveConsultant;
import com.cruxconsultores.pegasusx.ui.CallBack;
import com.cruxconsultores.pegasusx.ui.PinFormJDialog;

import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.firmaJava.libreria.xades.XAdESSchemas;
import es.mityc.javasign.EnumFormatoFirma;
import es.mityc.javasign.ts.HTTPTimeStampGenerator;
import es.mityc.javasign.ts.TSPAlgoritmos;
import es.mityc.javasign.xml.refs.InternObjectSignToSign;
import es.mityc.javasign.xml.refs.ObjectToSign;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 23/09/2014
 */
public class XadesEnvelopingSignature extends GenericXadesSignature {

    private String fileToSign;
    private String description;
    private String signRol;

    @Override
    protected DeviceManager getDeviceManager() {
        final DeviceManager manaer = new AthenaSmartCard();

        PinFormJDialog form = new PinFormJDialog(null, true);
        form.setCallBak(new CallBack() {
            @Override
            public void run(String str) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
                manaer.init(str);

            }
        });
        form.setVisible(true);
        return manaer;
    }

    @Override
    protected DataToSign createDataToSign() {
        if (this.description == null) {
            this.description = "Default";
        }

        if (this.signRol == null) {
            this.signRol = "Default";
        }

        DataToSign dataToSign = new DataToSign();
        dataToSign.setXadesFormat(EnumFormatoFirma.XAdES_XL);
        dataToSign.setCertStatusManager(new OCSPLiveConsultant("http://ocsp.sinpe.fi.cr/ocsp"));
        dataToSign.setTimeStampGenerator(new HTTPTimeStampGenerator("http://tsa.sinpe.fi.cr/tsaHttp/", TSPAlgoritmos.SHA1));
        dataToSign.setEsquema(XAdESSchemas.XAdES_132);
        dataToSign.setXMLEncoding("UTF-8");
        dataToSign.setEnveloped(false);
        InternObjectSignToSign objectToSign = new InternObjectSignToSign();
        objectToSign.setData(getDocument(this.fileToSign).getDocumentElement());
        dataToSign.addObject(new ObjectToSign(objectToSign, description, null, "text/xml", null));
        return dataToSign;
    }

    public String getFileToSign() {
        return fileToSign;
    }

    public void setFileToSign(String fileToSign) {
        this.fileToSign = fileToSign;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSignRol() {
        return signRol;
    }

    public void setSignRol(String signRol) {
        this.signRol = signRol;
    }

    public static void main(String args[]) throws Exception {
        XadesEnvelopingSignature signature = new XadesEnvelopingSignature();
        signature.setDescription("Ejemplo");
        signature.setSignRol("First Sign");
        signature.setFileToSign("C:\\Users\\Guillermo\\Desktop\\Prueba\\prueba.xml");
        signature.setOutPutFile("C:\\Users\\Guillermo\\Desktop\\Prueba\\prueba_firmado.xml");
        signature.execute();

    }

}
