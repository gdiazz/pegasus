/*
 * Copyright © 2014 Crux Consultores
 * All Rights Reserved.
 */
package com.cruxconsultores.pegasusx.sign.xml;

import org.w3c.dom.Document;

import com.cruxconsultores.pegasus.crl.CRLLiveConsultant;
import com.cruxconsultores.pegasus.device.DeviceManager;
import com.cruxconsultores.pegasus.ocsp.OCSPLiveConsultant;
import com.cruxconsultores.pegasusx.ui.PinFormJDialog;

import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.javasign.EnumFormatoFirma;
import es.mityc.javasign.ts.HTTPTimeStampGenerator;
import es.mityc.javasign.ts.TSPAlgoritmos;
import es.mityc.javasign.xml.refs.InternObjectToSign;
import es.mityc.javasign.xml.refs.ObjectToSign;

/**
 * @author Guillermo B Díaz Solís <gdiaz@cruxconsultores.com>
 * @since 23/09/2014
 */
public class XadesMixedSignature extends GenericXadesSignature {

	private String fileToSign;
	private String description;
	private String signRol;
	private String nodeTosign;
	private String parentId;
	private String validationType;

	public static final String USING_CRL = "crl";
	public static final String USING_OCSP = "ocsp";

	private DeviceManager manager;

	@Override
	protected DeviceManager getDeviceManager() {

		if (manager == null) {
			PinFormJDialog form = new PinFormJDialog(null, true);
			form.setVisible(true);

			return form.getManager();
		}
		return manager;

	}

	@Override
	protected DataToSign createDataToSign() {
		if (this.description == null) {
			this.description = "Default";
		}

		if (this.signRol == null) {
			this.signRol = "Default";
		}

		DataToSign dataToSign = new DataToSign();
		dataToSign.setXadesFormat(EnumFormatoFirma.XAdES_XL);

		if (getValidationType().equals(USING_OCSP)) {
			dataToSign
					.setCertStatusManager(new OCSPLiveConsultant(getUrlOcsp()));
		} else if (getValidationType().equals(USING_CRL)) {
			dataToSign.setCertStatusManager(new CRLLiveConsultant());
		}

		dataToSign.setTimeStampGenerator(new HTTPTimeStampGenerator(
				getUrlTsa(), TSPAlgoritmos.SHA1));
		dataToSign.setXMLEncoding("UTF-8");
		dataToSign.setEnveloped(true);
		Document docToSign = null;

		docToSign = getDocument(this.fileToSign);

		dataToSign.setDocument(docToSign);
		dataToSign.addObject(new ObjectToSign(
				new InternObjectToSign(nodeTosign), this.description, null,
				"text/xml", null));
		dataToSign.setParentSignNode(this.parentId);

		return dataToSign;
	}

	public String getFileToSign() {
		return fileToSign;
	}

	public void setFileToSign(String fileToSign) {
		this.fileToSign = fileToSign;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSignRol() {
		return signRol;
	}

	public void setSignRol(String signRol) {
		this.signRol = signRol;
	}

	public String getNodeTosign() {
		return nodeTosign;
	}

	public void setNodeTosign(String nodeTosign) {
		this.nodeTosign = nodeTosign;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the manager
	 */
	public DeviceManager getManager() {
		return manager;
	}

	/**
	 * @param manager
	 *            the manager to set
	 */
	public void setManager(DeviceManager manager) {
		this.manager = manager;
	}

	/**
	 * @return the validationType
	 */
	public String getValidationType() {
		return validationType;
	}

	/**
	 * @param validationType
	 *            the validationType to set
	 */
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}

}
